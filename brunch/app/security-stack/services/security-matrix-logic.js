var m = angular.module('security-stack');

m.factory('securityMatrixLogic', function(simpleLogicCore, modulesDao) {
    return function ($scope, logic) {
        return simpleLogicCore($scope, logic, {
            retrieve: function() {
                return modulesDao.securityMatrix();
            }
        });
    }
});