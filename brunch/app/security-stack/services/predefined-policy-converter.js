var m = angular.module('security-stack');

m.factory('predefinedPolicyConverter', function($q) {
    return function(input, args) {
        var deferred = $q.defer();

        _(input.data.rows).each(function(row) {
            row.policy.context = {
                saas: row.saas,
                security: row.sec_app
            };
        });

        deferred.resolve(input);
        return deferred.promise;
    }
});