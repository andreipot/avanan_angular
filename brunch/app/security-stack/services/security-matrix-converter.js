var m = angular.module('security-stack');

m.factory('securityMatrixConverter', function($q, path) {
    var root = path('security-stack');
    return function(input, args) {
        var deferred = $q.defer();

        var policyMap = {};
        _(args.modulesMatrix.data).each(function(value, saas) {
            var saasPolicies = policyMap[saas] || {};
            _(value).each(function(policies, module) {
                saasPolicies[module] = _(policies).map(function(policy, policyId) {
                    return {
                        name: policy.name,
                        status: policy.status,
                        id: policyId
                    }
                });
            });
            policyMap[saas] = saasPolicies;
        });

        _(args.policyCatalog.data.rows).chain().filter(function(row) {
            return _(row.context).isObject() && _(row.context.security).isString();
        }).each(function(row) {
            var saasPolicies = policyMap[row.context.saas] || {};
            saasPolicies[row.context.security] = saasPolicies[row.context.security] || [];
            if(_(saasPolicies[row.context.security]).every(function(policy) {
                    return policy.id != row.policy_id
                })) {
                saasPolicies[row.context.security].push({
                    status: row.policy_status,
                    name: row.policy_name,
                    id: row.policy_id
                });
            }
            policyMap[row.context.saas] = saasPolicies;
        });

        var groups = _(input.data.groups).map(function(group) {
            if(group.family == 'security') {
                return {
                    label: group.categoryName,
                    addRoute: {
                        name: 'app-store',
                        params: {
                            filter: group.type
                        }
                    },
                    items: _(group.modules).chain().filter(function(module) {
                        return module.state == 'active';
                    }).map(function(module) {
                        return {
                            id: module.name,
                            label: module.label,
                            img: {
                                path: root.img('solutions/' + module.stack_icon)
                            }
                        };
                    }).value()
                }
            } else {
                return {
                    label: group.categoryName
                }
            }
        });

        var applications = _(_(input.data.groups).find(function(group) {
            return group.family == 'saas';
        }).modules).map(function(app) {
            return {
                label: app.label,
                img: {
                    path: root.img('applications/' + app.stack_icon)
                },
                status: app.state,
                groups: _(groups).chain().map(function(group) {
                    return _(group.items).map(function(module) {
                        if(app.state != 'active') {
                            return {
                                state: 'none'
                            };
                        }
                        var policies = (policyMap[app.name] || {})[module.id];
                        var result = {
                            saas: app.name,
                            saasLabel: app.label,
                            security: module.id,
                            securityLabel: module.label
                        };
                        if(_(policies).isUndefined() || _(policies).isEqual({})) {
                            result.state = 'off';
                            return result;
                        }
                        result.policies = policies;
                        function isRunning(policy) {
                            return policy.status == 'running';
                        }
                        if(_(policies).every(isRunning)) {
                            result.state = 'on-green';
                        } else if(_(policies).some(isRunning)) {
                            result.state = 'on-blue';
                        } else {
                            result.state = 'off';
                        }
                        return result;
                    });
                }).flatten().value()
            }
        });

        var data = {
            data: {
                applications: applications,
                groups: groups
            }
        };

        deferred.resolve($.extend({}, input, {
            data: data
        }));
        return deferred.promise;
    }
});