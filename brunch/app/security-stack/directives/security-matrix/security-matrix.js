var m = angular.module('security-stack');

m.directive('securityMatrix', function (path, routeHelper, widgetLogic, policyDao, $q) {
    return {
        restrict: 'A',
        replace: true,
        templateUrl: path('security-stack').directive('security-matrix').template(),
        scope: {
            model: '=securityMatrix'
        },
        link: function ($scope, element, attrs) {
            widgetLogic($scope, $scope.model.logic);

            function enchantGroup(group) {
                group.tooltip =  _(group.policies).map(function(p) {
                    return p.name + ' (' + p.status + ')';
                }).join('<br>');
            }

            $scope.$watch('model', function () {
                $scope.tableModel = $.extend({}, $scope.model.data, true);
                $scope.tableModel.items = [];
                _($scope.tableModel.groups).each(function (group) {
                    if (_(group.items).isUndefined() || !group.items.length) {
                        $scope.tableModel.items.push({
                            empty: true,
                            group: group
                        });
                    } else {
                        $scope.tableModel.items = $scope.tableModel.items.concat(_(group.items).map(function (item) {
                            return $.extend({
                                group: group
                            }, item);
                        }));
                    }
                });

                _($scope.tableModel.applications).each(function(application) {
                    _(application.groups).each(function(group) {
                        enchantGroup(group);
                    });
                });
            }, true);

            $scope.groupWidth = function (group) {
                var items = group.items || [];
                return Math.max(1, items.length);
            };

            $scope.getItemIndexForColumn = function (column) {
                var idx = 0;
                for (var i = 0; i < column; i++) {
                    if (!$scope.tableModel.items[i].empty) {
                        idx++;
                    }
                }
                return idx;
            };

            $scope.itemLink = function (item) {
                if (item.group.addRoute) {
                    return routeHelper.getPath(item.group.addRoute.name, item.group.addRoute.params);
                }
                return '/';
            };

            $scope.toAppStore = function (event, item) {
                if (!_(event).isUndefined()) {
                    event.preventDefault();
                }
                if (_(item).isUndefined()) {
                    routeHelper.redirectTo('cloud-apps');
                } else if (item.group.addRoute) {
                    routeHelper.redirectTo(item.group.addRoute.name, item.group.addRoute.params);
                }
            };

            function policyByIdx(application, index) {
                return application.groups[$scope.getItemIndexForColumn(index)];
            }

            var actionInProcess = false;
            $scope.switchPolicy = function (application, index) {
                var policy = policyByIdx(application, index);
                if(actionInProcess) {
                    return;
                }
                actionInProcess = true;
                if (_(policy.policies).isUndefined()) {
                    policyDao.predefinedPolicy(policy.saas, policy.security).then(function(response) {
                        var policyBody = {
                            "policy_data": {
                                "excludeCondition": {
                                    "operator": "AND",
                                    "not": false,
                                    "conditions": []
                                },
                                "description": "",
                                "entityType": "google_user",
                                "actions": [],
                                "includeCondition": {
                                    "operator": "AND",
                                    "not": false,
                                    "conditions": [{
                                        "type": "condition",
                                        "not": false,
                                        "operator": "bool",
                                        "property": "google_user.isAdmin",
                                        "data": true,
                                        "options": {}
                                    }]
                                },
                                "excludeObjects": []
                            },
                            "entity_type": "google_user",
                            "policy_status": "running",
                            "context": {
                                saas: policy.saas,
                                security: policy.security
                            }
                        };
                        if (response.data.rows.length) {
                            policyBody = _(response.data.rows).first().policy;
                        }
                        policyBody = $.extend(true, {
                            "policy_name": policy.securityLabel + ' over ' + policy.saasLabel,
                            "policy_data": {
                                "name": policy.securityLabel + ' over ' + policy.saasLabel
                            },
                            "policy_status": 'running'
                        }, policyBody);
                        if(policyBody.policy_name != policyBody.policy_data.name) {
                            policyBody.policy_data.name = policyBody.policy_name;
                        }
                        policyDao.create(policyBody)['finally'](function() {
                            actionInProcess = false;
                        }).then(function(response) {
                            policy = policyByIdx(application, index);
                            policy.policies = [{
                                id: response.data.policy_id,
                                name: response.data.policy_name,
                                status: response.data.policy_status
                            }];
                            policy.state = 'on-green';
                            enchantGroup(policy);
                        });
                    });
                } else if (policy.state != 'off') {
                    $q.all(_(policy.policies).chain().filter(function(policy) {
                        return policy.status == 'running';
                    }).map(function(policy) {
                        return policyDao.retrieve(policy.id);
                    }).value()).then(function(responses) {
                        return $q.all(_(responses).map(function(response) {
                            var policyBody = response.data;
                            policyBody.policy_status = 'pause';
                            return policyDao.update(policyBody.policy_id, policyBody);
                        }));
                    })['finally'](function() {
                        actionInProcess = false;
                    }).then(function() {
                        policy = policyByIdx(application, index);
                        policy.state = 'off';
                        _(policy.policies).each(function(p) {
                            p.status = 'pause';
                        });
                        enchantGroup(policy);
                    });
                } else {
                    $q.all(_(policy.policies).map(function(policy) {
                        return policyDao.retrieve(policy.id);
                    })).then(function(responses) {
                        return $q.all(_(responses).map(function(response) {
                            var policyBody = response.data;
                            policyBody.policy_status = 'running';
                            return policyDao.update(policyBody.policy_id, policyBody);
                        }));
                    })['finally'](function() {
                        actionInProcess = false;
                    }).then(function() {
                        policy = policyByIdx(application, index);
                        policy.state = 'on-green';
                        _(policy.policies).each(function(p) {
                            p.status = 'running';
                        });
                        enchantGroup(policy);
                    });
                }
            };
        }
    }
});