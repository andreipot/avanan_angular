var m = angular.module('security-stack');

m.controller('SecurityStackController', function($scope, $routeParams, widgetPolling, sizeClasses, path) {
    widgetPolling.updateWidgets(path('security-stack').ctrl('security-stack').json('security-stack').path);
    $scope.widgetPolling = widgetPolling;
    $scope.getSizeClasses = sizeClasses;
});