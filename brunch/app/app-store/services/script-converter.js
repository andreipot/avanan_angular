var m = angular.module('app-store');

m.factory('scriptConverter', function($q) {
    return function(input, args) {
        var deferred = $q.defer();

        function convertType(type) {
            if(type == 'INT') {
                return 'integer';
            }
            return 'string';
        }

        var data = {
            notExist: _(input.data).isEqual({}),
            title: "Wizard",
            pages: [
                {
                    lastPage: true,
                    params: _(input.data).map(function (body, id) {
                        return {
                            id: id,
                            type: convertType(body.type),
                            data: body.value,
                            label: body.label,
                            hidden: _(body.label).isUndefined()
                        };
                    })
                }
            ]
        };

        deferred.resolve($.extend({}, input, {
            data: data
        }));
        return deferred.promise;
    };
});