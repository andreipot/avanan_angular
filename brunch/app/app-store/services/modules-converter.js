var m = angular.module('app-store');

m.factory('modulesConverter', function($q) {
    return function (input, args) {
        var deferred = $q.defer();

        var data = {
            groups: {}
        };

        _(input.data.apps_categories.categories).each(function(category) {
            data.groups[category.categoryId] = $.extend(category, {
                modules: []
            });
        });

        _(input.data.apps_items.modules).each(function(module) {
            data.groups[module.category_id].modules.push(module);
        });

        var stateWeight = {
            active: 0,
            nonactive: 1,
            display_only: 2
        };
        data.groups = _(input.data.apps_categories.categories).chain().sortBy(args.categorySort).map(function(category) {
            category.modules = _(category.modules).sortBy(function(module) {
                return stateWeight[module.state] + '-' + module[args.moduleSort];
            });
            return category;
        }).value();

        deferred.resolve($.extend({}, input, {
            data: data
        }));
        return deferred.promise;
    }
});