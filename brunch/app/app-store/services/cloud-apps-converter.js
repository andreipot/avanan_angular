var m = angular.module('app-store');

m.factory('cloudAppsConverter', function($q) {
    return function(input, args) {
        var deferred = $q.defer();

        var appStore = input.data;
        appStore.groups = _(appStore.groups).filter(function(group) {
            return group.family == 'saas';
        });

        var data = {
            widgets: _(_(input.data.groups).first().modules).map(function(module) {
                return {
                    "type": "store-item",
                    "size": "col-lg-3 col-md-4 col-sm-6 col-xs-6",
                    "wrapper": {
                        "type":"none"
                    },
                    "options": {
                        "img": {
                            "path": module.app_store_icon
                        },
                        "title": module.label,
                        "description": module.description,
                        "new": module.new,
                        "moduleName": module.name,
                        "moduleStatus": module.state,
                        "script": module.configuration,
                        "auth": module.authentication,
                        "button": {
                            'class': 'btn-secure',
                            label: 'Secure this app',
                            activeLabel: 'Configure'
                        }
                    }
                }
            })
        };

        deferred.resolve($.extend({}, input, {
            data: data
        }));
        return deferred.promise;
    };
});