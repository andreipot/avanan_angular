var m = angular.module('app-store');

m.factory('appStoreConverter', function($q) {
    return function(input, args) {
        var deferred = $q.defer();

        var appStore = input.data;
        appStore.groups = _(appStore.groups).filter(function(group) {
            if(!_(args.filter).isUndefined()) {
                return group.family == 'security' && group.type == args.filter;
            }
            return group.family == 'security';
        });

        var data = {
            widgets: _(input.data.groups).map(function(group) {
                return {
                    "type": "multi-widget",
                    "size": "col-lg-12 col-md-12 col-sm-12 col-xs-12",
                    "wrapper": {
                        "type": "widget-panel",
                        "class": "store-group",
                        "title": group.categoryLongName
                    },
                    "widgets": _(group.modules).map(function(module) {
                        var result = {
                            "type": "store-item",
                            "size": "col-lg-3 col-md-4 col-sm-6 col-xs-6",
                            "wrapper": {
                                "type":"none"
                            },
                            "options": {
                                "img": {
                                    "path": module.app_store_icon
                                },
                                "title": module.label,
                                "description": module.description,
                                "new": module.new,
                                "moduleName": module.name,
                                "moduleStatus": module.state,
                                "script": module.configuration,
                                "auth": module.authentication,
                                "authMessage": module.authentication_message,
                                "button": {
                                    'class': 'btn-install',
                                    label: 'Activate',
                                    activeLabel: 'Configure'
                                }
                            }
                        };

                        if(_(module.price_text).isString()) {
                            result.options.price = module.price_text;
                        } else {
                            result.options.price = 'Free';
                            result.class = 'free';
                        }
                        return result;
                    })
                };
            })
        };


        deferred.resolve($.extend({}, input, {
            data: data
        }));
        return deferred.promise;
    };
});