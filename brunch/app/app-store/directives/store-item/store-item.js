var m = angular.module('app-store');

m.directive('storeItem', function(path, http, modals, modulesDao, scriptConverter, widgetPolling) {
    return {
        restrict: 'A',
        replace: true,
        templateUrl: path('app-store').directive('store-item').template(),
        scope: {
            model: '=storeItem'
        },
        link: function ($scope, element, attrs) {
            $scope.path = path('app-store');

            $scope.isConfigurable = function() {
                return !_(_($scope.model.options.script).find(function(property) {
                    return !_(property.label).isUndefined();
                })).isUndefined();
            };

            $scope.isAuth = function() {
                return _($scope.model.options.auth).isString();
            };

            $scope.configure = function() {
                scriptConverter({
                    data: $scope.model.options.script || {}
                }).then(function(response) {
                    if ($scope.isAuth()) {
                        _(response.data.pages).first().params.push({
                            type: 'button',
                            title: 'oauth',
                            'class': 'btn-info',
                            execute: function () {
                                oauth();
                            }
                        });
                    }
                    return modals.params(response.data.pages, response.data.title, response.data.size);
                }).then(function(params) {
                    params = params || {};
                    modulesDao.saveConfig($scope.model.options.moduleName, params).then(function() {
                        widgetPolling.reload();
                    });
                });
            };

            function oauth() {
                modals.confirm({
                    title: '',
                    message: $scope.model.options.authMessage || 'In order to secure this application you are asked to provide admin-level oAuth authentication',
                    okText: 'Perform oauth Authentication'
                }).then(function () {
                    return http.get($scope.model.options.auth);
                }).then(function(response) {
                    window.open(response.data.url, '_blank').focus();
                });
            }

            var actions = {
                'display_only': {
                    execute: function() {
                        modals.alert('Please contact support@avanan.com regarding activating this option', {
                            title: ''
                        });
                    },
                    enabled: function() {
                        return true;
                    },
                    getLabel: function() {
                        return $scope.model.options.button.label;
                    }
                },
                'active': {
                    execute: function () {
                        $scope.configure();
                    },
                    enabled: function() {
                        return $scope.isConfigurable() || $scope.isAuth();
                    },
                    getLabel: function() {
                        return $scope.model.options.button.activeLabel;
                    }
                },
                'nonactive': {
                    execute: function () {
                        if ($scope.isAuth()) {
                            oauth();
                        } else {
                            $scope.configure();
                        }
                    },
                    enabled: function() {
                        return true;
                    },
                    getLabel: function() {
                        return $scope.model.options.button.label;
                    }
                }
            };

            $scope.install = function() {
                actions[$scope.model.options.moduleStatus || 'display_only'].execute();
            };

            $scope.enabled = function() {
                return actions[$scope.model.options.moduleStatus || 'display_only'].enabled();
            };

            $scope.getButtonLabel = function() {
                return actions[$scope.model.options.moduleStatus || 'display_only'].getLabel();
            };

            $scope.getDollars = function() {
                var splitted = $scope.model.options.price.split('.');
                if(splitted.length == 2) {
                    return splitted[0] + '.';
                }
                return $scope.model.options.price;
            };

            $scope.getCents = function() {
                var splitted = $scope.model.options.price.split('.');
                if(splitted.length == 2) {
                    return splitted[1];
                }
                return '';
            };

            $scope.isHavePrice = function() {
                return _($scope.model.options.price).isString();
            };

            $scope.isFree = function() {
                return $scope.model.options.price.toUpperCase() == 'FREE';
            };
        }
    }
});