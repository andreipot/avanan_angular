var m = angular.module('app-store');

m.controller('AppStoreController', function(path, $scope, widgetPolling, sizeClasses, $routeParams, $route, modulesDao) {
    $scope.widgetPolling = widgetPolling;
    $scope.getSizeClasses = sizeClasses;
    $scope.wrapperClass = 'app-store';

    widgetPolling.reload(function() {
        modulesDao[$route.current.accessor]($routeParams.filter).then(function(response) {
            widgetPolling.setLocal(response.data);
        });
    });
});
