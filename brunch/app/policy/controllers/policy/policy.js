var m = angular.module('policy');

m.controller('PolicyController', function($scope, $routeParams, $timeout, $q, defaultHttpErrorHandler, path, modals, policyDao, policyReportDao, objectTypeDao, tablePaginationHelper, columnsHelper, $route, routeHelper, commonDao, policySaveConverter, tableRemediationActionsBuilder) {
    var id = $routeParams.id;
    var isNew = routeHelper.getParams().isNew;

    $scope.isEmpty = function(o) {
        return _.isEmpty(o);
    };
    $scope.internalModel = {
        saasList: {},
        properties: {},
        sources: {},
        propertyTypes: {},
        actionTypes: {},
        entityIdMap: []
    };

    $scope.model = {
        includeCondition: {},
        excludeCondition: {},
        excludeObjects: [],
        actions: []
    };

    if(isNew) {
        $scope.model.includeCondition = {
            "operator": "AND",
            "not": false,
            "conditions": [
                {
                    "type": "condition",
                    "not": false
                }
            ]
        };
    }

    $q.all([
        objectTypeDao.retrieve(),
        policyDao.retrieve(id)
    ]).then(function(responses) {
        var properties = responses[0].data;
        $scope.internalModel.saasList = properties.saasList;
        $scope.internalModel.sources = properties.sources;
        $scope.internalModel.allSources = properties.allSources;
        $scope.internalModel.propertyTypes = properties.propertyTypes;
        $scope.internalModel.actionTypes = properties.actionTypes;
        $scope.internalModel.dataUrls = properties.dataUrls;
        var policy = responses[1].data.policy_data;
        $scope.internalModel.oldPolicy = responses[1].data.old_policy_data;
        $.extend(true, $scope.model, policy);
        $scope.model.saas = responses[1].data.context.saas;

        return commonDao.entityIdentifier($scope.model.excludeObjects, policy.entityType);
    }).then(function(entityIdMap) {
        $scope.internalModel.entityIdMap = entityIdMap;

        $scope.policyDefinitionsPanel = {
            title: 'Policies definitions',
            'class': 'light white-body white-header no-margin',
            bodyStyle: {
                'padding': '0px 17px 10px 17px'
            }
        };

        $scope.showPolicy = isNew;
        $scope.resultsPanel = {
            title: 'Results',
            'class': 'light white-body white-header no-margin',
            leftButton: {
                label: function() {
                    if($scope.showPolicy) {
                        return 'Hide policy'
                    }
                    return 'Show policy'
                },
                'class': 'btn-blue',
                onclick: function() {
                    $scope.showPolicy = !$scope.showPolicy;
                    $scope.splitterOptions.disabled = !$scope.showPolicy;
                }
            },
            progress: {
                tooltip: '',
                'class': 'policy-progress',
                label: 'Loading',
                data: [{
                    'class': 'progress-bar-success',
                    value: 0
                }]
            }
        };

        var icons = {
            minimize: 'fa-minus',
            expand: 'fa-plus'
        };

        $scope.policyOptionsPanel = {
            title: "Policy options",
            minimize: true,
            minimized: !isNew,
            'class': 'expandable-group left-controls no-margin',
            icons: icons
        };

        $scope.includeConditionPanel = {
            title: "Condition",
            minimize: true,
            minimized: !isNew,
            'class': 'expandable-group left-controls no-margin',
            icons: icons
        };

        $scope.excludeConditionPanel = {
            title: "Excluded items",
            minimize: true,
            minimized: true,
            'class': 'expandable-group left-controls no-margin',
            icons: icons
        };

        $scope.remediationActionsPanel = {
            title: "Remediation actions",
            minimize: true,
            minimized: !isNew,
            'class': 'expandable-group left-controls no-margin',
            icons: icons
        };

        $scope.jsonPanel = {
            title: "Raw data",
            minimize: true,
            minimized: true,
            'class': 'expandable-group left-controls no-margin',
            icons: icons
        };

        $scope.splitterOptions = {
            orientation: 'vertical',
            position: '50%',
            limit: 150,
            disabled: !$scope.showPolicy
        };

        function generateBody() {
            var saas = $scope.model.saas;
            var model = $.extend({}, $scope.model);
            delete model.saas;
            return {
                "policy_status": "running",
                "policy_data": model,
                "old_policy_data": $scope.internalModel.oldPolicy,
                "context": {
                    "saas": saas
                }
            };
        }

        $scope.recheckPolicy = function() {
            policyDao.recheck(id);
        };

        $scope.updatePolicy = function() {
            if(_($scope.model.entityType).isUndefined()) {
                modals.alert('You must select object type before update policy');
                return;
            }
            if(_($scope.model.name).isUndefined()) {
                modals.alert('You must specify policy name before update policy');
                return;
            }

            policyDao.update(id, generateBody());
        };

        $scope.generateRaw = function() {
            var body = generateBody();
            policySaveConverter(body).then(function(raw) {
                $scope.raw = raw;
            });
        };

        function updateOptionsLabel() {
            if(_($scope.internalModel.sources).isUndefined()) {
                $scope.policyOptionsPanel.title = "Policy options";
            } else {
                var policyName = $scope.model.name;
                var policyTypeLabel = $scope.internalModel.sources[$scope.model.entityType].label;
                $route.current.fullTitle = $route.current.title + ' - ' + policyName + ' (' + policyTypeLabel + ')';
            }
        }

        $scope.$watch('internalModel.sources', updateOptionsLabel);
        $scope.$watch('model.entityType + model.name', updateOptionsLabel);

        $scope.$watch('model.entityType', function(newEntityType, oldEntityType) {
            if (!_(oldEntityType).isUndefined() && newEntityType != oldEntityType) {
                $scope.model.includeCondition = {};
                $scope.model.excludeCondition = {};
                $scope.model.actions = [];
                $scope.model.excludeObjects = [];
            }
        });

        $scope.$watch('model.excludeObjects', function(newExclude, oldExclude) {
            //TODO Slow but simple diff. Change it if there will be performance issue
            var added = _(newExclude).filter(function(e) {
                return oldExclude.indexOf(e) < 0;
            });
            var removed = _(oldExclude).filter(function(e) {
                return newExclude.indexOf(e) < 0;
            });
            policyDao.updateExclude(id, added, removed).then(function() {
                if($scope.tableHelper != null) {
                    $scope.tableHelper.reload(false);
                }
            });
        }, true);

        $scope.$watch('internalModel.properties', function(properties) {
            if($scope.tableHelper != null) {
                $scope.tableHelper.destroy()
            }
            $scope.tableModel = null;
            $scope.tableOptions = {
                pagesAround: 2,
                pageSize: 20,
                pagination: {
                    page: 1,
                    ordering: {},
                    filter: '',
                    disableTop: true
                },
                toggleState: 'Match',
                actions: [{
                    label: 'Exclude selected',
                    execute: function() {
                        _($scope.tableHelper.getSelectedIds()).each(function(id) {
                            if(!_($scope.model.excludeObjects).contains(id)) {
                                $scope.model.excludeObjects.push(id);
                                var variant = {
                                    id: id,
                                    label: $scope.tableHelper.getLabel(id, true)
                                };
                                var existed = _($scope.internalModel.entityIdMap).find(function(v) {
                                    return _(v).isEqual(variant);
                                });
                                if(_(existed).isUndefined()) {
                                    $scope.internalModel.entityIdMap.push(variant);
                                }
                            }
                        });
                        $scope.tableHelper.clearSelection();
                        $scope.tableHelper.reload();
                    }
                }],
                filter: function(row, columns) {
                    var idx = columnsHelper.getPrimaryIdx(columns);
                    var found = _($scope.model.excludeObjects).find(function(e) {
                        return e === row[idx].originalText;
                    });
                    if(_(found).isUndefined()) {
                        return true;
                    }
                    return 'excluded';
                }
            };

            tableRemediationActionsBuilder($scope.tableOptions, $scope.internalModel.actionTypes, $scope.internalModel.sources, 'google_drive', function() {
                return $scope.tableHelper.getSelectedIds();
            });

            var options = $.extend({
                options: {
                    autoUpdate: 5000,
                    checkboxes: true
                }
            }, $scope.internalModel.properties);

            $scope.tableHelper = tablePaginationHelper($scope, $scope.tableOptions, options, function(args) {
                var ordering;
                if(args.ordering.column) {
                    ordering = args.ordering.columnName + ' ' + args.ordering.order.toUpperCase();
                }
                return policyReportDao.retrieve(
                    id,
                    $scope.internalModel.allSources,
                    $scope.internalModel.actionTypes,
                    args.offset / args.limit + 1,
                    args.limit,
                    args.filter,
                    ordering,
                    $scope.tableOptions.toggleState,
                    function(tested, total) {
                        $scope.resultsPanel.progress.total = total;
                        $scope.resultsPanel.progress.tooltip = 'Tested ' + tested + ' of ' + total + ' entities';
                        $scope.resultsPanel.progress.label = (tested * 100 / total).toFixed(2) + '%';
                        _($scope.resultsPanel.progress.data).first().value = tested;
                    });
            }, function(tableModel) {
                $scope.tableModel = tableModel;
            });
        });

        $scope.initialized = true;
    }, defaultHttpErrorHandler);
});