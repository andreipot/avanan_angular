var m = angular.module('policy');

m.controller('PolicyListController', function($scope, $q, tablePaginationHelper, routeHelper, path, policyDao, objectTypeDao, columnsHelper, $timeout, widgetSettings) {
    function changeStatus(policyIds, status) {
        $q.all(_(policyIds).map(function(policyId) {
            return policyDao.retrieve(policyId).then(function(response) {
                var policy = response.data;
                policy.policy_status = status;
                return policyDao.update(policyId, policy);
            });
        })).then(function() {
            $scope.tableHelper.clearSelection();
            $scope.tableHelper.reload();
        });
    }

    function isActionActive(state, singleSelection) {
        return function() {
            var selected = $scope.tableHelper.getSelected();
            if (singleSelection && selected.length > 1 || selected.length == 0) {
                return false;
            }
            if(_(state).isUndefined()) {
                return true;
            }
            var idx = columnsHelper.getIdxById($scope.options.columns, 'policy_status');
            return _(selected).some(function(s) {
                return s[idx].originalText !== state;
            });
        }
    }

    var policyListWidgetUUID = '523cd709-a4b5-4857-bfa0-06b280b0686e';
    var showPausedPoliciesKey = 'show-paused-policies';
    var showHiddenPoliciesKey = 'show-hidden-policies';
    $scope.policyListPanel = {
        uuid: policyListWidgetUUID,
        wrapper: {
            title: "Policies",
            'class': 'light white-body white-header',
            filters: [{
                type: 'flag',
                local: true,
                state: false,
                text: 'Show paused policies',
                value: showPausedPoliciesKey
            }, {
                type: 'flag',
                local: true,
                state: false,
                text: 'Show hidden policies',
                value: showHiddenPoliciesKey
            }]
        }
    };

    $scope.tableOptions = {
        pagesAround: 2,
        pageSize: 20,
        pagination: {
            page: 1,
            ordering: {},
            filter: '',
            disableTop: true
        },
        actions: [{
            label: 'Pause',
            active: isActionActive('pause'),
            execute: function() {
                changeStatus($scope.tableHelper.getSelectedIds(), 'pause');
            }
        }, {
            label: 'Start',
            active: isActionActive('running'),
            execute: function() {
                changeStatus($scope.tableHelper.getSelectedIds(), 'running');
            }
        }, {
            label: 'Delete',
            active: isActionActive(),
            execute: function() {
                changeStatus($scope.tableHelper.getSelectedIds(), 'mark_for_deletion');
            }
        }, {
            label: 'Clone',
            active: isActionActive(undefined, true),
            execute: function() {
                var policyId = $scope.tableHelper.getSelectedIds()[0];
                policyDao.retrieve(policyId).then(function(response) {
                    var policy = response.data;
                    var name = 'Copy of ' + policy.policy_name;
                    policy.policy_name = name;
                    policy.policy_data.name = name;
                    delete policy['policy_id'];
                    return policyDao.create(policy);
                }).then(function(response) {
                    routeHelper.redirectTo('policy-edit', {id: response.data.policy_id});
                });
            }
        }, {
            label: 'Export',
            active: isActionActive(),
            execute: function() {
                $q.all(_($scope.tableHelper.getSelectedIds()).map(function(policyId) {
                    return policyDao.retrieve(policyId);
                })).then(function(responses) {
                    var policies = _(responses).map(function(response) {
                        var policy = response.data;
                        delete policy['policy_id'];
                        policy.policy_data.excludeObjects = [];
                        return policy;
                    });
                    var blob = new Blob([JSON.stringify(policies)], {type: "application/json;charset=utf-8"});
                    var name = 'policies_export';
                    if(policies.length == 1) {
                        name = _(policies).first().policy_name;
                    }
                    saveAs(blob, name + '.json');
                });
            }
        }, {
            label: 'Add new policy',
            display: 'button',
            execute: function() {
                routeHelper.redirectTo('policy-create');
            }
        }, {
            label: 'Import a policy',
            display: 'button',
            execute: function() {
                $timeout(function() {
                    $('#import-file').trigger('click');
                });
            }
        }],
        filter: function(row) {
            var idx = columnsHelper.getIdxById($scope.options.columns, 'policy_status');
            return row[idx].originalText !== 'mark_for_deletion';
        }
    };

    $scope.importFileReadComplete = function(content) {
        try {
            var policies = JSON.parse(content);
            if(!_(policies).isArray()) {
                policies = [policies];
            }
            $q.all(_(policies).map(function(policy) {
                return policyDao.create(policy);
            })).then(function(responses) {
                if(responses.length == 1) {
                    routeHelper.redirectTo('policy-edit', {id: _(responses).first().data.policy_id});
                } else {
                    $scope.tableHelper.reload();
                }
            });
        } catch(e) {
            alert('Incorrect file');
        }
    };

    $scope.$watchCollection(function() {
        return [
            widgetSettings.param(policyListWidgetUUID, showPausedPoliciesKey),
            widgetSettings.param(policyListWidgetUUID, showHiddenPoliciesKey)
        ];
    }, function(newVal, oldVal) {
        if(newVal != oldVal && !_($scope.tableHelper).isUndefined()) {
            $scope.tableHelper.clearSelection();
            $scope.tableHelper.reload();
        }
    });

    $q.all([
        path('policy').ctrl('policy-list').json('list-conf').retrieve(),
        objectTypeDao.retrieve()
    ]).then(function(responses) {
        $scope.options = responses[0].data;
        $scope.sources = responses[1].data.sources;

        $scope.tableHelper = tablePaginationHelper($scope, $scope.tableOptions, $scope.options, function(args) {
            var showPaused = widgetSettings.param(policyListWidgetUUID, showPausedPoliciesKey);
            var showHidden = widgetSettings.param(policyListWidgetUUID, showHiddenPoliciesKey);
            return policyDao.list($scope.options.columns, $scope.sources, !showPaused, !showHidden);
        }, function(tableModel) {
            $scope.tableModel = tableModel;
        });
    });
});