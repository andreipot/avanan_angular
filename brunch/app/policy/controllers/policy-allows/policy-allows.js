var m = angular.module('policy');

m.controller('PolicyAllowsController', function($scope, $routeParams, $q, policyDao, policyAllowsDao, path, tablePaginationHelper) {
    var id = $routeParams.id;

    $scope.policyAllowsListPanel = {
        wrapper: {
            title: "Loading...",
            'class': 'light white-body white-header'
        }
    };

    $scope.tableOptions = {
        pagesAround: 2,
        pageSize: 20,
        pagination: {
            page: 1,
            ordering: {},
            filter: '',
            disableTop: true
        },
        actions: [{
            label: 'Delete',
            display: 'dropdown-button',
            active: function() {
                return $scope.tableHelper.getSelectedIds().length;
            },
            execute: function() {
                $q.all(_($scope.tableHelper.getSelectedIds()).map(function(allowId) {
                    return policyAllowsDao.remove(allowId);
                })).then(function() {
                    $scope.tableHelper.reload();
                });
            }
        }]
    };

    $q.all([
        policyDao.retrieve(id),
        path('policy').ctrl('policy-allows').json('list-conf').retrieve()
    ]).then(function(responses) {
        var policy = responses[0].data;
        var options = responses[1].data;

        $scope.policyAllowsListPanel.wrapper.title = policy.policy_data.name + ' allows';

        $scope.tableHelper = tablePaginationHelper($scope, $scope.tableOptions, options, function(args) {
            return policyAllowsDao.retrieve(options.columns, id);
        }, function(tableModel) {
            $scope.tableModel = tableModel;
        });
    });
});