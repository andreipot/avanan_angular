var m = angular.module('policy');

m.controller('PolicyCreateController', function($scope, $routeParams, modals, routeHelper, policyDao, objectTypeDao, feature) {
    $scope.internalModel = {
        saasList: {},
        sources: {},
        propertyTypes: {},
        properties: {}
    };

    if(feature.policyTemplates) {
        $scope.policyOptionsPanel = {
            title: "Policy options",
            'class': 'light white-body white-header no-margin',
            checkbox: {
                value: false,
                text: 'Advanced',
                'class': 'advanced-checkbox'
            }
        };

        $scope.$watchCollection(function() {
            return [$scope.model.templateName, $scope.policyOptionsPanel.checkbox.value];
        }, function() {
            if($scope.policyOptionsPanel.checkbox.value) {
                $scope.policyOptionsPanel.title = "Policy options";
            } else if(_($scope.model.templateName).isUndefined()) {
                $scope.policyOptionsPanel.title = "Select template";
            } else {
                $scope.policyOptionsPanel.title = "Policy template options";
            }
        });
    } else {
        $scope.policyOptionsPanel = {
            title: "Policy options",
            'class': 'light white-body white-header no-margin',
            checkbox: {
                value: true,
                text: 'Advanced',
                'class': 'advanced-checkbox display-none'
            }
        };
    }

    $scope.model = {};
    $scope.$watch('policyOptionsPanel.checkbox.value', function() {
        $scope.model = {
            includeCondition: {},
            excludeCondition: {},
            actions: []
        };
    });

    objectTypeDao.retrieve().then(function(response) {
        $scope.internalModel.saasList = response.data.saasList;
        $scope.internalModel.sources = response.data.sources;
        $scope.internalModel.propertyTypes = response.data.propertyTypes;
        $scope.internalModel.actionTypes = response.data.actionTypes;
        $scope.internalModel.loaded = true;
    });

    $scope.isSaveVisible = function() {
        if(!$scope.policyOptionsPanel.checkbox.value) {
            return !_($scope.model.entityType).isUndefined();
        }
        return true;
    };

    $scope.save = function() {
        if(_($scope.model.entityType).isUndefined()) {
            modals.alert('You must select object type before you can continue');
            return;
        }
        if(_($scope.model.name).isUndefined()) {
            modals.alert('You must specify policy name before you can continue');
            return;
        }

        var saas = $scope.model.saas;
        var model = $.extend({}, $scope.model);
        delete model.saas;
        policyDao.create({
            "policy_status": "running",
            "policy_data": model,
            "context": {
                "saas": saas
            }
        }).then(function(response) {
            routeHelper.redirectTo('policy-edit', {
                id: response.data.policy_id,
                isNew: true
            });
        });
    };
});