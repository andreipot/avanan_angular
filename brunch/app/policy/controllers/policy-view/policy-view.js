var m = angular.module('policy');

m.controller('PolicyViewController', function ($scope, $routeParams, policyDao, path, policyReportDao, tablePaginationHelper, $q, objectTypeDao, columnsHelper, enchantDao, tableRemediationActionsBuilder, $route) {
    var id = $routeParams.id;

    $scope.internalModel = {};
    objectTypeDao.retrieve().then(function(response) {
        var properties = response.data;
        $scope.internalModel.saasList = properties.saasList;
        $scope.internalModel.sources = properties.sources;
        $scope.internalModel.allSources = properties.allSources;
        $scope.internalModel.propertyTypes = properties.propertyTypes;
        $scope.internalModel.actionTypes = properties.actionTypes;
        $scope.internalModel.dataUrls = properties.dataUrls;

        function atLeastOneSelected() {
            return $scope.tableHelper.getSelectedIds().length > 0;
        }

        $scope.tableOptions = {
            pagesAround: 2,
            pageSize: 20,
            pagination: {
                page: 1,
                ordering: {},
                filter: '',
                disableTop: true
            },
            toggleState: 'Match',
            actions: [{
                label: 'Add to exceptions',
                display: 'dropdown-button',
                active: atLeastOneSelected,
                submenus: [{
                    label: 'Allow this user to publicly share this file',
                    active: atLeastOneSelected
                }, {
                    label: 'Allow public sharing of this file',
                    active: atLeastOneSelected
                }, {
                    label: 'Allow this user to publicly share everything',
                    active: atLeastOneSelected
                }, {
                    label: 'Allow any files in this folder to be publicly shared',
                    active: atLeastOneSelected
                }, {
                    label: 'Allow users from this group to publicly share this folder',
                    active: atLeastOneSelected
                }, {
                    label: 'Except all current public-shared files in this folder',
                    active: atLeastOneSelected
                }]
            }]
        };

        $scope.resultsPanel = {
            title: 'Loading...',
            'class': 'avanan white-body',
            filters: [{
                type: 'flag',
                local: true,
                state: false,
                text: 'Show fixed',
                value: 'showFixed'
            }]
        };

        tableRemediationActionsBuilder($scope.tableOptions, $scope.internalModel.actionTypes, $scope.internalModel.sources, 'google_drive', function() {
            return $scope.tableHelper.getSelectedIds();
        }, {
            actionRepeatOptions: [{
                label: 'Notify user again after',
                labelPostfix: 'days',
                data: 7
            }, {
                label: 'Send final notice after',
                labelPostfix: 'days',
                data: 7
            }, {
                label: 'Revoke Public Share after',
                labelPostfix: 'days',
                data: 7
            }]
        });

        $scope.columnsMap = {};
        function mapOrdering(ordering) {
            if(!_(ordering).isString()) {
                return ordering;
            }
            var splitted = ordering.split(' ');
            if(splitted.length < 2) {
                return ordering;
            }
            return $scope.columnsMap[splitted[0]] + ' ' + splitted[1];
        }
        path('policy').ctrl('policy-view').json('publicly-shared-data').retrieve().then(function (response) {
            var options = response.data;
            $route.current.title = '#' + JSON.stringify({
                display: 'html',
                text: '<strong>' + options.options.title + '</strong>'
            });

            $scope.tableHelper = tablePaginationHelper($scope, $scope.tableOptions, options, {
                isExpandable: function(row) {
                    return true;
                },
                expand: function(row) {
                    var primaryIdx = columnsHelper.getPrimaryIdx(options.columns);
                    var rootCause = row[primaryIdx].text.entity_id;
                    return enchantDao.dataEntitiesByRootCause(options.columns, rootCause, $scope.internalModel.actionTypes);
                },
                retrieve: function (args) {
                    var ordering;
                    if (args.ordering.column) {
                        ordering = args.ordering.columnName + ' ' + args.ordering.order.toUpperCase();
                    }
                    return policyReportDao.retrieve(
                        id,
                        $scope.internalModel.allSources,
                        $scope.internalModel.actionTypes,
                        args.offset / args.limit + 1,
                        args.limit,
                        args.filter,
                        mapOrdering(ordering),
                        $scope.tableOptions.toggleState).converter(function(input) {
                            var primaryIdx = columnsHelper.getPrimaryIdx(input.data.columns);

                            $scope.columnsMap = _(input.data.columns).chain().map(function(value) {
                                return [value.propName, value.id];
                            }).object().value();

                            var ids = _(input.data.data).map(function(row) {
                                return row[primaryIdx].originalText;
                            });

                            return enchantDao.dataEntitiesByIds(options.columns, ids, $scope.internalModel.actionTypes).converter(function(cInput) {
                                cInput.data.pagination = input.data.pagination;
                                return $q.when(cInput);
                            });
                        });
                }
            }, function (tableModel) {
                $scope.tableModel = tableModel;
                $scope.resultsPanel.title = $scope.tableHelper.getTotal() + ' ' + options.options.title;
            });

            $scope.initialized = true;
        });
    });
});