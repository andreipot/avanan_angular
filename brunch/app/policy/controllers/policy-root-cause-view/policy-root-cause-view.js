var m = angular.module('policy');

m.controller('PolicyRootCauseViewController', function ($scope, $routeParams, policyDao, path, policyReportDao, tablePaginationHelper, $q, objectTypeDao, columnsHelper, enchantDao, tableRemediationActionsBuilder, $route, policyAllowsDao, tableAllowsActionsBuilder) {
    var internalName = $routeParams.internalName;

    $scope.internalModel = {};
    $q.all([
        policyAllowsDao.types('public_shared_files_allow'),
        objectTypeDao.retrieve(),
        path('policy').ctrl('policy-root-cause-view').json('publicly-shared-data').retrieve(),
        policyDao.retrieveByInternalName(internalName)
    ]).then(function(responses) {
        var allowsTypes = responses[0].data;
        var properties = responses[1].data;
        var options = responses[2].data;
        var id = responses[3].data.policy_id;
        $scope.internalModel.saasList = properties.saasList;
        $scope.internalModel.sources = properties.sources;
        $scope.internalModel.allSources = properties.allSources;
        $scope.internalModel.propertyTypes = properties.propertyTypes;
        $scope.internalModel.actionTypes = properties.actionTypes;
        $scope.internalModel.dataUrls = properties.dataUrls;

        $scope.tableOptions = {
            pagesAround: 2,
            pageSize: 20,
            pagination: {
                page: 1,
                ordering: {},
                filter: '',
                disableTop: true
            },
            toggleState: 'Match',
            actions: []
        };

        tableAllowsActionsBuilder(id, $scope.tableOptions, $scope.internalModel.actionTypes, allowsTypes, {
            getSelectedIdsCount: function() {
                return $scope.tableHelper.getSelectedIds().length;
            },
            getSelectedIds: function() {
                var selectedIds = $scope.tableHelper.getSelectedIds();
                return $q.all(_(selectedIds).map(function(id) {
                    if(id.entity_type == 'file') {
                        return $q.when([id.entity_id]);
                    }
                    return enchantDao.dataEntitiesByRootCause(options.columns, id.entity_id, $scope.internalModel.actionTypes).converter(function(input) {
                        var primaryIdx = columnsHelper.getPrimaryIdx(options.columns);
                        return $q.when(_(input.data.data).map(function(row) {
                            return row[primaryIdx].text.entity_id
                        }));
                    });
                })).then(function(responses) {
                    return $q.when(_(responses).chain().flatten().uniq().value());
                });
            },
            reload: function() {
                $scope.tableHelper.reload();
            }
        });

        tableRemediationActionsBuilder($scope.tableOptions, $scope.internalModel.actionTypes, $scope.internalModel.sources, 'google_drive', function() {
            return $scope.tableHelper.getSelectedIds();
        }, options.remediationActionBuilderArgs);

        $scope.resultsPanel = {
            title: 'Loading...',
            'class': 'avanan white-body single-widget',
            filters: [{
                type: 'flag',
                local: true,
                state: false,
                text: 'Show fixed',
                value: 'showFixed'
            }]
        };

        $route.current.title = '#' + JSON.stringify({
            display: 'html',
            text: '<strong>' + options.options.title + '</strong>'
        });

        $scope.tableHelper = tablePaginationHelper($scope, $scope.tableOptions, options, {
            isExpandable: function(row) {
                var idx = columnsHelper.getIdxById(options.columns, 'entity_type');
                return row[idx].originalText == 'folder';
            },
            expand: function(row) {
                var primaryIdx = columnsHelper.getPrimaryIdx(options.columns);
                var rootCause = row[primaryIdx].text.entity_id;
                return enchantDao.dataEntitiesByRootCause(options.columns, rootCause, $scope.internalModel.actionTypes);
            },
            retrieve: function (args) {
                return policyReportDao.filePolicyRootCauses(id)
                    .pagination(args.offset, args.limit)
                    .filter(args.filter)
                    .order(args.ordering.columnName, args.ordering.order)
                    .converter(function(input) {
                        var filter = function(row) {
                            return _(row.google_file_id || row.google_folder_id).isString();
                        };
                        var ids = _(input.data.rows).chain().filter(filter).map(function(row) {
                            return row.google_file_id || row.google_folder_id;
                        }).value();
                        var idsCounts = _(input.data.rows).chain().filter(filter).map(function(row) {
                            return [row.google_file_id || row.google_folder_id, row.count];
                        }).object().value();
                        return enchantDao.dataEntitiesByIds(options.columns, ids, $scope.internalModel.actionTypes, idsCounts).converter(function(cInput) {
                            cInput.data.pagination.current = args.offset / $scope.tableOptions.pageSize + 1;
                            cInput.data.pagination.total = input.data.total_rows;
                            return $q.when(cInput);
                        });
                    });
            }
        }, function (tableModel) {
            $scope.tableModel = tableModel;
            $scope.resultsPanel.title = $scope.tableHelper.getTotal() + ' ' + options.options.title;
        });

        $scope.initialized = true;
    });
});