var m = angular.module('policy');

m.directive('subSelector', function(path) {
    return {
        restrict: 'A',
        replace: false,
        templateUrl: path('policy').directive('sub-selector').template(),
        scope: {
            menus: '=subSelector',
            model: '=ngModel'
        },
        link: function($scope, element, attrs) {
            var menus = $.extend(true, {}, $scope.menus);
            $scope.root = {
                id: 0,
                nodeId: 0,
                fullId: menus[0].id
            };
            $scope.$watch('model', function(model) {
                if(!_($scope.model).isUndefined() && !_($scope.model.nodeId).isUndefined()) {
                    $scope.value = menus[$scope.model.nodeId].label;
                }
            });

            $scope.menuClicked = function(event, menuId) {
                event.preventDefault();
                if(!_(menuId).isUndefined() && $scope.filteredMenus[menuId.nodeId].selectable) {
                    $scope.value = menus[menuId.nodeId].label;
                    $scope.model = $.extend({}, menuId, true);
                    $scope.searchDropdown.open = false;
                } else {
                    if($scope.searchDropdown.open) {
                        element.find('input').focus();
                    }
                    event.stopPropagation();
                }
            };

            $scope.searchDropdown = {
                accessor: function(val) {
                    if(_(val).isBoolean()) {
                        if(val || !this.hover) {
                            this.open = val;
                        }
                        if(!val && !this.hover) {
                            $scope.value = $scope.oldValue;
                        }
                    }
                    return this.open;
                },
                open: false,
                hover: false
            };

            $scope.oldValue = '';
            $scope.onFilterFocus = function(state) {
                if(state == $scope.searchDropdown.accessor()) {
                    return;
                }
                if(state) {
                    $scope.oldValue = $scope.value;
                    $scope.value = '';
                    $scope.filteredMenus = menus;
                }
                $scope.searchDropdown.accessor(state);
            };

            $scope.onFilterChangeOld = function() {
                if($scope.value === '') {
                    $scope.filteredMenus = menus;
                } else {
                    var lowerCasedFilter = $scope.value.toLowerCase();
                    $scope.filteredMenus = {};
                    var keys = [];
                    _(menus).each(function(val, key) {
                        if(key != 0) {
                            $scope.filteredMenus[key] = val;
                            if(~val.label.toLowerCase().indexOf(lowerCasedFilter)) {
                                keys.push(key);
                            }
                        }
                    });
                    if(!keys.length) {
                        $scope.filteredMenus[-1] = {
                            label: 'Not found'
                        };
                        keys = [-1];
                    }
                    $scope.filteredMenus[0] = {
                        sub: keys
                    };
                }
            };

            $scope.onFilterChange = function() {
                function getMenuKeys(id, addAll) {
                    var keys = [];
                    _(menus[id].sub).each(function (id) {
                        var menu = menus[id];
                        var labelMatched = addAll || ~(menu.label || '').toLowerCase().indexOf(lowerCasedFilter);
                        if (!processed[id] && (labelMatched || _(menu.sub).isArray() && menu.sub.length && _($scope.filteredMenus[id]).isUndefined())) {
                            processed[id] = true;
                            var subKeys = getMenuKeys(id, labelMatched);
                            if(labelMatched || subKeys.length) {
                                keys.push(id);
                                $scope.filteredMenus[id] = $.extend(true, {}, menu);
                                if(subKeys.length) {
                                    $scope.filteredMenus[id].sub = subKeys;
                                }
                            }
                        } else if(labelMatched) {
                            keys.push(id);
                        }
                    });
                    return keys;
                }

                if($scope.value === '') {
                    $scope.filteredMenus = menus;
                } else {
                    var lowerCasedFilter = $scope.value.toLowerCase();
                    $scope.filteredMenus = {};
                    var processed = {};
                    var keys = getMenuKeys(0);
                    if(!keys.length) {
                        $scope.filteredMenus['not_found_menu'] = {
                            label: 'Not found'
                        };
                        keys = ['not_found_menu'];
                    }
                    $scope.filteredMenus[0] = {
                        sub: keys
                    };
                }
            };
        }
    }
});