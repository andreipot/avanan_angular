var m = angular.module('policy');

m.directive('selector', function(path, $timeout) {
    return {
        restrict: 'A',
        replace: true,
        templateUrl: path('policy').directive('selector').template(),
        scope: {
            model: '=selector',
            variants: '=',
            change: '&?',
            noVariantsCaption: '@?'
        },
        link: function($scope, element, attrs) {
            $scope.change = $scope.change || _.noop;

            $scope.setVariant = function($event, variant) {
                $event.preventDefault();
                var prev = $scope.model;
                if(_(variant).isObject()) {
                    $scope.model = variant.id;
                } else {
                    $scope.model = variant;
                }
                if(prev != $scope.model) {
                    $timeout(function () {
                        $scope.change({
                            newValue: $scope.model,
                            oldValue: prev
                        });
                    });
                }
            };

            $scope.getLabel = function(id) {
                var variant = _($scope.variants).find(function(v) {
                    if(_(v).isObject()) {
                        return v.id == id;
                    }
                    return v == id;
                });
                if(_(variant).isUndefined()) {
                    return '';
                }
                if(_(variant).isObject()) {
                    return variant.label;
                }
                return variant;
            };

            $scope.getVariantLabel = function(variant) {
                if(_(variant).isObject()) {
                    return variant.label;
                }
                return variant;
            };
        }
    }
});