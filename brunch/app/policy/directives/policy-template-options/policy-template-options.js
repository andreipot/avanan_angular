var m = angular.module('policy');

m.directive('policyTemplateOptions', function(path, policyDao, modals, columnsHelper, tablePaginationHelper, uuid, remediationProperties) {
    var root = path('policy').directive('policy-template-options');
    return {
        restrict: 'A',
        replace: false,
        templateUrl: root.template(),
        scope: {
            model: '=policyTemplateOptions',
            sources: '=',
            propertyTypes: '=',
            properties: '=',
            actionTypes: '='
        },
        link: function($scope, element, attrs) {
            $scope.properties = $scope.sources[$scope.model.entityType];

            $scope.internalModel = {
                originalModel: $.extend(true, {}, $scope.model),
                editableTemplate: true
            };

            $scope.getActionType = function(action) {
                return _($scope.actionTypes[$scope.model.entityType]).find(function(type) {
                    return type.id == action.name;
                });
            };

            if($scope.model.templateName) {
                $scope.internalModel.editableTemplate = false;
                selectTemplate();
            } else {
                root.json('template-list-conf').retrieve().then(function(response) {
                    $scope.options = response.data;
                    $scope.tableHelper = tablePaginationHelper($scope, $scope.tableOptions, $scope.options, function (args) {
                        return policyDao.getTemplatesList(response.data.columns, $scope.sources);
                    }, function (tableModel) {
                        $scope.tableModel = tableModel;
                    });

                    $scope.$watchCollection('tableHelper.getSelected()', function(selected) {
                        if(_(selected).isUndefined() || selected.length == 0) {
                            return;
                        }
                        var nameIdx = columnsHelper.getIdxById(response.data.columns, 'template_name');
                        var descIdx = columnsHelper.getIdxById(response.data.columns, 'template_desc');
                        var policyIdx = columnsHelper.getIdxById(response.data.columns, 'policy');
                        var template = _(selected).first();
                        var policy = template[policyIdx].originalText;

                        $.extend(true, $scope.model, policy.policy_desc, {
                            templateName: template[nameIdx].originalText,
                            templateDesc: template[descIdx].originalText,
                            variables: policy.variables
                        });

                        selectTemplate();
                        $scope.tableHelper.clearSelection();
                    });
                });
            }

            $scope.tableOptions = {
                pagesAround: 2,
                pageSize: 20,
                pagination: {
                    page: 1,
                    ordering: {},
                    filter: '',
                    disableBottom: true
                }
            };

            $scope.reset = function() {
                if($scope.internalModel.editableTemplate) {
                    for(var key in $scope.model) if($scope.model.hasOwnProperty(key)) {
                        delete $scope.model[key];
                    }
                    $.extend($scope.model, $scope.internalModel.originalModel);
                    delete $scope.internalModel.questions;
                    $scope.internalModel.templateSelected = false;
                }
            };

            $scope.selectTemplate = function() {
                if(!$scope.internalModel.editableTemplate) {
                    return;
                }
                root.json('template-list-conf').retrieve().then(function(response) {
                    return modals.table({
                        title: 'Select template',
                        tableOptions: $scope.tableOptions,
                        options: response.data,
                        'class': 'avanan-table line-selection small-icons',
                        retrieve: function(args) {
                            return policyDao.getTemplatesList(response.data.columns, $scope.sources);
                        }
                    }, 'lg').then(function(selected) {
                        var nameIdx = columnsHelper.getIdxById(response.data.columns, 'template_name');
                        var descIdx = columnsHelper.getIdxById(response.data.columns, 'template_desc');
                        var policyIdx = columnsHelper.getIdxById(response.data.columns, 'policy');
                        var template = _(selected).first();
                        var policy = template[policyIdx].originalText;

                        $.extend(true, $scope.model, policy.policy_desc, {
                            templateName: template[nameIdx].originalText,
                            templateDesc: template[descIdx].originalText,
                            variables: policy.variables
                        });

                        selectTemplate();
                    });
                })
            };

            function selectTemplate() {
                var conditions = {};
                function parseConditions(group) {
                    _(group.conditions).each(function(condition) {
                        if(condition.type == 'condition') {
                            if(_(condition.dataVar).isString()) {
                                conditions[condition.dataVar] = condition;
                            }
                        } else {
                            parseConditions(condition);
                        }
                    })
                }
                parseConditions($scope.model.includeCondition || {});
                parseConditions($scope.model.excludeCondition || {});

                $scope.internalModel.questions = _($scope.model.variables).map(function(value, key) {
                    return $.extend({
                        id: key,
                        condition: conditions[key]
                    }, value);
                });

                $scope.properties = $scope.sources[$scope.model.entityType];

                $scope.internalModel.optionalActions = _($scope.model.optionalActions).map(function(optional) {
                    var actionType = $scope.getActionType(optional);
                    var properties = remediationProperties($scope.properties.properties);
                    var self = {
                        label: optional.label,
                        action: _($scope.model.actions).find(function(action) {
                            return !_(optional.id).isUndefined() && action.id == optional.id;
                        }),
                        enabled: !_(optional.id).isUndefined(),
                        questions: _(optional.variables).map(function(value, key) {
                            return $.extend({
                                id: key,
                                param: $.extend(true, {}, _(actionType.attributes).find(function(attr) {
                                    return attr.id == key;
                                }))
                            }, value);
                        }),
                        properties: properties,
                        onChange: function() {
                            if(self.enabled) {
                                $scope.model.actions = _($scope.model.actions).filter(function(action) {
                                    return action != self.action;
                                });
                                delete optional.id;
                            } else {
                                var action = $.extend(true, {}, _(optional).omit('variables'));
                                optional.id = action.id = uuid.random();
                                $scope.model.actions.push(action);

                                self.action = action;
                            }
                        }
                    };
                    return self;
                });

                $scope.internalModel.templateSelected = true;
            }
        }
    }
});