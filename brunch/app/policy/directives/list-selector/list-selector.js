var m = angular.module('policy');

m.directive('listSelector', function($timeout) {
    return {
        restrict: 'A',
        replace: false,
        scope: {
            model: '=listSelector',
            variants: '=?'
        },
        link: function ($scope, element, attrs) {
            element.on('change', function(data) {
                $timeout(function() {
                    $scope.model = data.val;
                });
            });

            function reinit() {
                var selectSettings = {
                    dropdownCssClass : 'hidden-list-selector-dropdown',
                    placeholder: 'Type and press enter',
                    createSearchChoice: function(term, data) {
                        if ($(data).filter(function() {
                                return this.text.localeCompare(term)===0;
                            }).length===0) {
                            return {id:term, text:term};
                        }
                    },
                    multiple: true,
                    data: []
                };
                if(!_($scope.variants).isUndefined()) {
                    selectSettings = {
                        dropdownCssClass: 'list-selector-dropdown',
                        placeholder: 'Click to select',
                        multiple: true,
                        data: _($scope.variants).map(function (variant) {
                            return {id: variant.id, text: variant.label};
                        })
                    };
                }


                element.select2('destroy');
                element.addClass('list-selector');
                element.css('display', 'inline-block');
                element.css('width','100%');
                element.find('.select2-choices').addClass('form-control');
                element.select2(selectSettings);
                element.select2('data', _($scope.model).chain().map(function(term) {
                    var text = term;
                    if(!_($scope.variants).isUndefined()) {
                        var found = false;
                        _($scope.variants).each(function(variant) {
                            if(variant.id == term) {
                                found = true;
                                text = variant.label;
                            }
                        });
                        if(!found) {
                            return null;
                        }
                    }
                    return {id:term, text:text};
                }).filter(function(d) {
                    return _(d).isObject();
                }).value());
            }

            $scope.$watch('model', function(newVal, oldVal) {
                if(newVal != oldVal) {
                    reinit();
                }
            }, true);
            $scope.$watch('variants', function(newVal, oldVal) {
                if(newVal != oldVal) {
                    reinit();
                }
            }, true);

            reinit();

            $scope.$on('$destroy', function() {
                element.select2('destroy');
            });
        }
    };
});