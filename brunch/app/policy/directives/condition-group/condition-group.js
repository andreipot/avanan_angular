var m = angular.module('policy');

m.directive('conditionGroup', function(path, recursionHelper) {
    return {
        restrict: 'A',
        replace: true,
        templateUrl: path('policy').directive('condition-group').template(),
        scope: {
            model: '=conditionGroup',
            options: '=',
            propertyTypes: '=',
            remove: '&?',
            root: '@?'
        },
        compile: function(element) {
            return recursionHelper.compile(element, this.link);
        },
        link: function ($scope, element, attrs) {
            $scope.internalModel = {
                simpleMode: false
            };

            $scope.operatorVariants = ["AND", "OR"];

            $scope.notVariants = [{
                id: false,
                label: ''
            }, {
                id: true,
                label: 'NOT'
            }];

            $scope.$watch('model', function() {
                $scope.model.conditions = $scope.model.conditions || [];
                $scope.model.operator = $scope.model.operator || 'AND';
                $scope.model.check = check;
                _($scope.model.conditions).each(function(condition) {
                    condition.remove = removeCondition;
                });
                if(_($scope.model.not).isUndefined()) {
                    $scope.model.not = false;
                }
            });

            $scope.setNot = function($event, value) {
                $event.preventDefault();
                $scope.model.not = value;
            };

            $scope.setOperator = function($event, value) {
                $event.preventDefault();
                $scope.model.operator = value;
            };

            $scope.addCondition = function() {
                if($scope.model.check()) {
                    $scope.model.conditions.push({
                        type: 'condition',
                        remove: removeCondition,
                        check: checkCondition
                    });
                }
            };
            $scope.addGroup = function() {
                if($scope.model.check()) {
                    $scope.model.conditions.push({
                        type: 'group',
                        remove: removeCondition,
                        check: checkCondition
                    });
                }
            };

            function removeCondition() {
                var self = this;
                $scope.model.conditions = _($scope.model.conditions).filter(function(condition) {
                    return condition !== self;
                });
            }

            function checkCondition() {
                //Condition always incorrect until it will be overwritten in it
                return false;
            }

            function check() {
                if(!_(_($scope.model.conditions).find(function(condition) {
                    return !condition.check();
                })).isUndefined()) {
                    return false;
                }
                return true;
            }
        }
    }
});