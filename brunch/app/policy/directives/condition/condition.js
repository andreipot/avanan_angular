var m = angular.module('policy');

m.directive('condition', function(path, policyPropertyParser) {
    return {
        restrict: 'A',
        replace: true,
        templateUrl: path('policy').directive('condition').template(),
        scope: {
            model: '=condition',
            options: '=',
            propertyTypes: '=',
            remove: '&'
        },
        link: function ($scope, element, attrs) {
            $scope.invalid = {};
            $scope.internal = {
                operatorLabel: '',
                operator: {},
                options: []
            };

            $scope.notVariants = [{
                id: false,
                label: ''
            }, {
                id: true,
                label: 'NOT'
            }];

            $scope.$watch('model', function(model) {
                model.check = function() {
                    $scope.invalid = {};
                    if(_(model.property).isUndefined()) {
                        $scope.invalid.property = true;
                        return false;
                    }
                    return true;
                };
            }, true);

            $scope.getPropertyTypes = function(property) {
                if(_(property).isUndefined() || _(property.nodeId).isUndefined()) {
                    return [];
                }
                var propertyTypeId = $scope.options.properties[property.nodeId].type;
                return $scope.propertyTypes[propertyTypeId];
            };

            var cachedOperatorVariants = null;
            $scope.getOperatorVariants = function(property) {
                var variants = _($scope.getPropertyTypes(property)).map(function(o) {
                    return {
                        id: o.label,
                        label: o.label
                    };
                });
                if(_(variants).isEqual(angular.copy(cachedOperatorVariants))) {
                    return cachedOperatorVariants;
                }
                return cachedOperatorVariants = variants;
            };

            $scope.isOperatorHidden = function(property) {
                var types = $scope.getPropertyTypes(property);
                return types.length == 1 && _(types).first().hidden;
            };

            $scope.getVariants = function(property) {
                if(_(property).isUndefined() || _(property.nodeId).isUndefined()) {
                    return [];
                }
                return $scope.options.properties[property.nodeId].variants;
            };

            $scope.getOperatorByLabel = function(label) {
                var operators = $scope.getPropertyTypes($scope.internal.property);
                return _(operators).find(function(operator) {
                    return operator.label === label;
                });
            };

            $scope.getOperatorById = function(id) {
                var result = _($scope.getPropertyTypes($scope.internal.property)).find(function(property) {
                    return property.id == id;
                });
                if(_(result).isUndefined()) {
                    result = _($scope.getPropertyTypes($scope.internal.property)).first();
                }
                return result;
            };

            $scope.$watch('internal.property', function(property) {
                $scope.invalid.property = false;
                if(!_(property).isUndefined()) {
                    var types = $scope.getPropertyTypes(property);
                    if(_(_(types).find(function(type) {
                        return type.label == $scope.internal.operatorLabel;
                    })).isUndefined()) {
                        $scope.internal.operatorLabel = $scope.getPropertyTypes(property)[0].label;
                    }
                    $scope.model.property = $scope.internal.property.fullId;
                }
            }, true);

            function updateOptions(prevOptions) {
                if (_($scope.internal.operator.options).isArray()) {
                    _($scope.internal.operator.options).each(function (option) {
                        if (option.type === 'flag') {
                            var prevOption = _(prevOptions).find(function(o) {
                                return o.id == option.id;
                            });
                            var state = option.defaultState;
                            if(!_(prevOption).isUndefined()) {
                                state = prevOption.state;
                            }
                            $scope.internal.options.push({
                                id: option.id,
                                text: option.label,
                                state: state
                            });
                        }
                    });
                }
                var state = false;
                if(!_(prevOptions).isUndefined()) {
                    var prevOption = _(prevOptions).find(function(o) {
                        return o.id == 'data_is_property';
                    });
                    if(!_(prevOption).isUndefined()) {
                        state = prevOption.state;
                    }
                }
                $scope.internal.options.push({
                    id: 'data_is_property',
                    text: 'Data is property',
                    state: state
                });
            }

            $scope.setNot = function($event, value) {
                $event.preventDefault();
                $scope.model.not = value;
            };

            $scope.setOperator = function($event, value) {
                $event.preventDefault();
                $scope.internal.operatorLabel = value;
            };

            $scope.$watch('internal.operatorLabel', function(label) {
                var previousType;
                if(!_($scope.internal.operator).isUndefined()) {
                    previousType = $scope.internal.operator.type;
                }
                $scope.internal.operator = $scope.getOperatorByLabel(label);
                if(_($scope.internal.operator).isUndefined()) {
                    return;
                }
                $scope.model.operator = $scope.internal.operator.id;
                var prevOptions = $scope.internal.options;
                $scope.internal.options = [];
                if(!_($scope.internal.operator).isUndefined()) {
                    var type = $scope.internal.operator.type;
                    if(type != previousType) {
                        if (type === 'none') {
                            if (!_($scope.model.data).isUndefined()) {
                                delete $scope.model.data;
                            }
                        } else if (type === 'single' || type === 'single-date') {
                            $scope.model.data = ''
                        } else if (type === 'double' || type === 'double-date') {
                            $scope.model.data = {
                                first: '',
                                second: ''
                            }
                        } else if (type === 'list') {
                            $scope.model.data = [];
                        }
                    }
                    updateOptions(prevOptions);
                }
            });

            $scope.$watch('internal.options', function(options) {
                $scope.model.options = {};
                _(options).each(function(option) {
                    $scope.model.options[option.id] = option.state;
                });
            }, true);

            $scope.dateOptions = {
                formatYear: 'yy',
                startingDay: 1
            };

            if(!_($scope.model.operator).isUndefined()) {
                $scope.internal.property = policyPropertyParser($scope.model.property);
                $scope.internal.operator = $scope.getOperatorById($scope.model.operator);
                $scope.internal.operatorLabel = $scope.internal.operator.label;
                updateOptions(_($scope.model.options).map(function(val, key) {
                    return {
                        id: key,
                        state: val
                    }
                }));
            }

            if(_($scope.model.not).isUndefined()) {
                $scope.model.not = false;
            }
        }
    }
});