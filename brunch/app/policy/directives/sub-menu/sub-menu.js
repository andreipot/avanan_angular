var m = angular.module('policy');

m.directive('subMenu', function(path, recursionHelper) {
    return {
        restrict: 'A',
        replace: true,
        templateUrl: path('policy').directive('sub-menu').template(),
        scope: {
            menus: '=subMenu',
            menuId: '=',
            selectedId: '=',
            depth: '@',
            menuClicked: '&menuClicked'
        },
        compile: function(element) {
            return recursionHelper.compile(element, this.link);
        },
        link: function($scope, element, attrs) {
            $scope.internal = {};
            $scope.$watch('menus', function() {
                $scope.hoveredSubMenu = null;
            });

            $scope.menuClickedWrapper = function(event, menuId) {
                $scope.menuClicked({event: event, menuId: menuId});
            };

            $scope.getSubMenus = function() {
                if(!_($scope.menus).isUndefined()) {
                    var scroll = $scope.internal.scroll;
                    var maxLines = $scope.internal.maxLines;
                    var count = 0;
                    var idx = 0;
                    var menus = _($scope.allSubMenus).reduce(function(memo, menu){
                        if(idx >= scroll && count < maxLines) {
                            memo.push(menu);
                            if(_(menu).isString()) {
                                count++;
                            }
                        }
                        if(_(menu).isString()) {
                            idx++;
                        }
                        return memo;
                    }, []);
                    while(_(_(menus).last()).isObject()) {
                        menus = _(menus).initial();
                    }
                    return _(menus).map(function(nodeId) {
                        if(_(nodeId).isObject()) {
                            return nodeId;
                        }
                        var id = _(nodeId.split('.')).last();
                        return {
                            id: id,
                            nodeId: nodeId,
                            fullId: $scope.menuId.fullId + '.' + id
                        };
                    });
                }
            };

            $scope.isScrollUpEnabled = function() {
                if(!_($scope.menus).isUndefined()) {
                    return $scope.internal.scroll > 0;
                }
            };

            function filterMenus(menu) {
                return _(menu).isString();
            }

            $scope.isScrollDownEnabled = function() {
                if(!_($scope.menus).isUndefined()) {
                    return _($scope.allSubMenus).filter(filterMenus).length > $scope.internal.scroll + $scope.internal.maxLines;
                }
            };

            $scope.onWheel = function(event, delta) {
                event.preventDefault();
                event.stopPropagation();
                $scope.processScroll(delta);
            };

            $scope.onScrollClick = function(event, delta) {
                $scope.processScroll(delta);
                $scope.menuClicked({event: event, menuId: undefined});
            };

            $scope.processScroll = function(delta) {
                $scope.hoveredSubMenu = null;
                $scope.internal.scroll -= delta;
                $scope.internal.scroll = Math.min(_($scope.allSubMenus).filter(filterMenus).length - $scope.internal.maxLines, $scope.internal.scroll);
                $scope.internal.scroll = Math.max(0, $scope.internal.scroll);
                $scope.subMenus = $scope.getSubMenus();
            };

            $scope.getTopForMenu = function(index) {
                var submenu = $scope.menus[$scope.subMenus[index].nodeId];
                if(_(submenu.sub).isUndefined()) {
                    return 0;
                }
                var submenuHeight = Math.min(_(submenu.sub).chain().filter(function(subId) {
                    return !($scope.depth == 1 && $scope.menus[subId].sub);
                }).filter(filterMenus).value().length - 1, $scope.internal.maxLines);
                var realIndex = 0;
                for(var i = 0; i < index; i++) {
                    if(_($scope.subMenus[i].id).isString()) {
                        realIndex++;
                    }
                }
                return -(Math.min(submenuHeight, realIndex) * 100) + '%';
            };

            $scope.getSubSelected = function() {
                if(_($scope.selectedId).isUndefined()) {
                    return undefined;
                }
                var dot = $scope.selectedId.indexOf('.');
                if(~dot) {
                    return undefined;
                }
                return $scope.selectedId.slice(dot + 1);
            };

            function init(menus) {
                $scope.internal = {
                    maxLines: 17,
                    scroll: 0
                };

                if(_(menus).isUndefined()) {
                    return;
                }
                
                $scope.allSubMenus = _($scope.menus[$scope.menuId.nodeId].sub).filter(function(subId) {
                    if($scope.depth == 0 && $scope.menus[subId].sub) {
                        return false;
                    }
                    if($scope.menuId.nodeId == 0) {
                        return true;
                    }
                    if(_(subId).isObject()) {
                        return !subId.onlyOnRoot;
                    }
                    return !$scope.menus[subId].onlyOnRoot;
                });

                $scope.subMenus = $scope.getSubMenus();
            }

            $scope.$watch('menus', init);
        }
    }
});