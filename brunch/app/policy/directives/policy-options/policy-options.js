var m = angular.module('policy');

m.directive('policyOptions', function(path, routeHelper, modals, equalCache) {
    return {
        restrict: 'A',
        replace: false,
        templateUrl: path('policy').directive('policy-options').template(),
        scope: {
            model: '=policyOptions',
            saasList: '=',
            sources: '=',
            properties: '=',
            existed: '@?'
        },
        link: function ($scope, element, attrs) {
            $scope.internal = {};

            function watch() {
                if(_($scope.sources).isUndefined() || _($scope.sources[$scope.model.entityType]).isUndefined()) {
                    return;
                }
                $scope.properties = $scope.sources[$scope.model.entityType];
                $scope.internal = {
                    saas: $scope.properties.saas,
                    entityType: $scope.model.entityType
                };
            }
            $scope.$watch('model.entityType', watch);
            $scope.$watch('sources', watch);

            $scope.getSourceVariants = equalCache(function() {
                return _($scope.sources).chain().map(function(source, sourceId) {
                    if(source.saas == $scope.internal.saas) {
                        return {
                            id: sourceId,
                            label: source.label
                        };
                    }
                    return null;
                }).filter(function(source) {
                    return source != null;
                }).value();
            });

            $scope.getSaasVariants = equalCache(function() {
                return _($scope.saasList).map(function(saas, saasId) {
                    return {
                        id: saasId,
                        label: saas.label
                    };
                });
            });

            var previousSaas;
            var previousEntityType;
            $scope.$watch('internal.entityType', function(newEntityType, oldEntityType) {
                previousEntityType = oldEntityType;
            });

            $scope.$watch('internal.saas', function(newSaas, oldSaas) {
                previousSaas = oldSaas;
            });

            $scope.onSaasChange = function() {
                previousEntityType = $scope.internal.entityType;
                $scope.internal.entityType = null;
                $scope.onEntityTypeChange();
            };

            $scope.onEntityTypeChange = function() {
                if($scope.existed == 'true') {
                    modals.confirm('If you change object type all conditions will be lost. Are you sure to continue?').then(function () {
                        $scope.model.entityType = $scope.internal.entityType;
                    }, function () {
                        $scope.internal.saas = previousSaas;
                        $scope.internal.entityType = previousEntityType;
                    });
                } else {
                    $scope.model.saas = $scope.internal.saas;
                    $scope.model.entityType = $scope.internal.entityType;
                }
            };

            $scope.model.description = $scope.model.description || '';
        }
    }
});