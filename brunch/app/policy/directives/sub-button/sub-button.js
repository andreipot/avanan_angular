var m = angular.module('policy');

m.directive('subButton', function(path) {
    return {
        restrict: 'A',
        replace: true,
        templateUrl: path('policy').directive('sub-button').template(),
        scope: {
            menus: '=subButton',
            onClick: '&'
        },
        link: function ($scope, element, attrs) {
            $scope.root = {
                id: 0,
                nodeId: 0,
                fullId: $scope.menus[0].id
            };

            $scope.menuClicked = function(event, menuId) {
                event.preventDefault();
                if(!_(menuId).isUndefined() && $scope.menus[menuId.nodeId].selectable) {
                    $scope.onClick({menuId: menuId});
                } else {
                    event.stopPropagation();
                }
            };
        }
    }
});