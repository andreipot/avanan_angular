var m = angular.module('policy');

m.directive('stateSwitch', function(path) {
    var root = path('policy').directive('state-switch');
    return {
        restrict: 'A',
        replace: true,
        template: '<img class="state-switch" ng-src="{{states[model]}}"/>',
        scope: {
            model: '=stateSwitch',
            states: '=?'
        },
        link: function($scope, element, attrs) {
            $scope.states = $scope.states || {
                'off': root.img('off.png'),
                'on-blue': root.img('on-blue.png'),
                'on-green': root.img('on-green.png')
            }
        }
    }
});