var m = angular.module('policy');

m.directive('showSubMenu', function($parse) {
    return {
        restrict: 'A',
        replace: false,
        link: function($scope, element, attrs) {
            var subMenuId = $parse(attrs.showSubMenu)($scope);
            element.hover(function() {
                $scope.$parent.hoveredSubMenu = subMenuId;
                $scope.$parent.$digest();
            }, function() {
            });
        }
    }
});