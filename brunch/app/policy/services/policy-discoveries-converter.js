var m = angular.module('policy');

m.factory('policyDiscoveriesConverter', function($q) {
    return function (input, args) {
        var deferred = $q.defer();

        var policies = {};

        _(args.policyCatalog.data.rows).each(function(row) {
            var policy = policies[row.policy_id] = {
                id: row.policy_id,
                name: row.policy_name,
                data: {}
            };

            policy.data[null] = args.findingsStatus.data[policy.id] || null;
        });

        _(input.data.rows).each(function(row) {
            var policy = policies[row.policy_id];
            if(_(policy).isObject()) {
                var policyData = policy.data[row.insert_date] || {};
                policyData[row.status] = row.count;
                policy.data[row.insert_date] = policyData;
                if(_(row.insert_date).isNull()) {
                    policy.last_match_time = row.last_match_time;
                }
            }
        });

        var data = _(policies).map(function(policy) {
            var discovers = [];
            var matches = (policy.data[null] || {})['Match'] || 0;
            var current = moment().hours(12).minutes(0).seconds(0).milliseconds(0);
            discovers.push([parseInt(current.format('x')), matches]);
            for(var i = 0; i < args.after; i++) {
                var date = current.format('YYYY-MM-DD');

                matches -= (policy.data[date] || {})['Match'] || 0;
                matches += (policy.data[date] || {})['Unmatch'] || 0;

                current.add(-1, 'day');
                discovers.push([parseInt(current.format('x')), matches]);
            }
            discovers.reverse();

            return {
                id: policy.id,
                label: policy.name,
                last_match_time: policy.last_match_time,
                data: discovers
            };
        });

        deferred.resolve($.extend({}, input, {
            data: data
        }));
        return deferred.promise;
    };
});