var m = angular.module('policy');

m.factory('remediationProperties', function() {
    return function(properties) {
        return function (propertyType) {
            properties = $.extend(true, {}, properties);
            if (_(propertyType).isUndefined()) {
                return properties;
            }

            properties[0].pointer = properties[0].id;
            properties['.' + properties[0].id] = properties[0];
            properties[0] = {
                id: '',
                sub: ['.' + properties[0].id]
            };

            _(properties).chain().keys().each(function (key) {
                if (properties[key].selectable) {
                    delete properties[key];
                } else if (properties[key].pointer == propertyType) {
                    properties[key].selectable = true;
                }
            });
            var removed = true;
            while (removed) {
                removed = false;
                _(properties).chain().keys().each(function (key) {
                    properties[key].sub = _(properties[key].sub).filter(function (subId) {
                        return _(properties[subId]).isObject();
                    });
                    if (properties[key].sub.length == 0) {
                        delete properties[key].sub;
                        if (properties[key].pointer != propertyType) {
                            delete properties[key];
                            removed = true;
                        }
                    }
                });
            }

            function fixTree(id, depth) {
                if(depth == 0) {
                    return false;
                }
                properties[id].sub = _(properties[id].sub).filter(function(subId) {
                    return fixTree(subId, depth - 1);
                });
                if(properties[id].sub && properties[id].sub.length == 0) {
                    delete properties[id].sub;
                }
                return properties[id].selectable || properties[id].sub;
            }

            fixTree(0, 5);

            if (_(properties[0]).isUndefined()) {
                properties['not_found_menu'] = {
                    label: 'Not found'
                };
                properties[0] = {
                    sub: ['not_found_menu']
                };
            }

            return properties;
        }
    }
});