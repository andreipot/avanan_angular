var m = angular.module('policy');

m.factory('policySaveConverter', function($q, actionTagsProcessor, uuid) {
    return function (input, args) {
        var deferred = $q.defer();

        var result = $.extend(true, {}, input);

        _(result.policy_data.actions).each(function(action, idx) {
            if(!_(result.old_policy_data).isUndefined()) {
                var oldAction = _(result.old_policy_data.actions).find(function(oldAction) {
                    return action.id == oldAction.id;
                });
                if(_(oldAction).isObject()) {
                    var untagOldAction = $.extend(true, {}, oldAction);
                    actionTagsProcessor.replaceTags(untagOldAction);
                    delete untagOldAction.tags;
                    if (_(untagOldAction).isEqual(action)) {
                        result.policy_data.actions[idx] = oldAction;
                        return;
                    }
                }
            }
            actionTagsProcessor.generateTags(action, action.attributes);
            var oldId = action.id;
            action.id = uuid.random();
            var optional = _(result.policy_data.optionalActions).find(function(optional) {
                return optional.id == oldId;
            });
            if(!_(optional).isUndefined()) {
                optional.id = action.id;
            }
        });

        if(!_(result.old_policy_data).isUndefined()) {
            delete result.old_policy_data;
        }

        var fields = ['templateName', 'templateDesc', 'variables', 'optionalActions'];
        result.context.template = _(result.policy_data).pick(fields);
        result.policy_data = _(result.policy_data).omit(fields);

        result.entity_type = result.policy_data.entityType;
        result.policy_name = result.policy_data.name;
        result.policy_description = result.policy_data.description;

        //delete result.policy_data.entityType;
        //delete result.policy_data.name;
        //delete result.policy_data.description;

        function processCondition(condition) {
            if(_(condition.conditions).isUndefined()) {
                if(_(condition.options).isObject() && !_(condition.options.data_is_property).isUndefined()) {
                    if(condition.options.data_is_property) {
                        condition.data = {
                            property: condition.data.fullId
                        };
                    }
                    condition.data_is_property = condition.options.data_is_property;
                    delete condition.options.data_is_property;
                    if (_(condition.options).isEqual({})) {
                        delete condition.options;
                    }
                }
            } else {
                _(condition.conditions).each(function(subCond) {
                    processCondition(subCond);
                });
            }
        }

        processCondition(result.policy_data.includeCondition);
        processCondition(result.policy_data.excludeCondition);

        deferred.resolve(result);
        return deferred.promise;
    };
});