var m = angular.module('policy');

m.factory('policyConverter', function($q, actionTagsProcessor, policyPropertyParser) {
    return function (input, args) {
        var deferred = $q.defer();

        if(!_(input.data.context.template).isUndefined()) {
            $.extend(input.data.policy_data, input.data.context.template);
        }

        input.data.old_policy_data = $.extend(true, {}, input.data.policy_data);

        _(input.data.policy_data.actions).each(function(action) {
            actionTagsProcessor.replaceTags(action);
            delete action.tags;
        });

        input.data.policy_data.entityType = input.data.entity_type;
        input.data.policy_data.name = input.data.policy_name;
        input.data.policy_data.description = input.data.policy_description;

        delete input.data.entity_type;
        delete input.data.policy_name;
        delete input.data.policy_description;

        function processCondition(condition) {
            if(_(condition.conditions).isUndefined()) {
                if(condition.data_is_property) {
                    condition.data = policyPropertyParser(condition.data.property);
                    condition.options = condition.options || {};
                    condition.options.data_is_property = true;
                }
                if(!_(condition.data_is_property).isUndefined()) {
                    delete condition.data_is_property;
                }
            } else {
                _(condition.conditions).each(function(subCond) {
                    processCondition(subCond);
                });
            }
        }

        processCondition(input.data.policy_data.includeCondition);
        processCondition(input.data.policy_data.excludeCondition);

        deferred.resolve(input);
        return deferred.promise;
    };
});