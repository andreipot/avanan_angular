var m = angular.module('policy');

m.factory('policyPropertyParser', function () {
    return function(property) {
        if(!_(property).isString() || !property.length) {
            return {};
        }
        var dot = property.lastIndexOf('.');
        var propertyId = property;
        if(~dot) {
            propertyId = propertyId.slice(dot + 1);
        }
        var nodeId = property;
        var splitted = nodeId.split('.');
        if(splitted.length > 1) {
            nodeId = splitted[splitted.length - 2] + '.' + splitted[splitted.length - 1];
        }
        return {
            fullId: property,
            id: propertyId,
            nodeId: nodeId
        };
    }
});