var m = angular.module('policy');

m.factory('tableRemediationActionsBuilder', function(entityTypes, modals, manualActionDao, remediationProperties) {
    return function(tableOptions, actionTypes, sources, saas, getSelectedIds, args) {
        args = args || {};
        var actionTypesMap = _(actionTypes).chain().map(function(value, key) {
            return [key, _(value).chain().map(function(action) {
                return [action.id, action];
            }).object().value()]
        }).object().value();

        var uniqueActions = {};
        _(actionTypesMap).each(function(actions, entityType) {
            _(actions).each(function(action, actionKey) {
                var uniq = uniqueActions[actionKey];
                if(_(uniq).isUndefined()) {
                    uniq = {
                        entityTypes: [],
                        action: action
                    };
                }
                uniq.entityTypes.push(entityType);
                uniqueActions[actionKey] = uniq;
            });
        });
        _(uniqueActions).each(function(uniq, actionKey) {
            tableOptions.actions.push({
                label: uniq.action.label,
                display: 'dropdown-button',
                entityTypes: uniq.entityTypes,
                active: function() {
                    var self = this;
                    var ids = getSelectedIds();
                    var types = _(ids).chain().map(function(id) {
                        return id.entity_type;
                    }).uniq().value();
                    return types.length == 1 && _(self.entityTypes).contains(entityTypes.toBackend(saas, _(types).first()));
                },
                execute: function() {
                    var self = this;

                    var ids = getSelectedIds();
                    var type = _(ids).first().entity_type;
                    var entities = _(ids).map(function(id) {
                        return id.entity_id;
                    });

                    modals.params([{
                        params: _(uniq.action.attributes).map(function(param) {
                            return $.extend(true, {}, param, {
                                data: ((args.defaultData || {})[actionKey] || {})[param.id]
                            });
                        }),
                        lastPage: true,
                        getProperties: _.memoize(remediationProperties(sources[entityTypes.toBackend(saas, type)].properties)),
                        actionRepeatOptions: args.actionRepeatOptions[actionKey]
                    }], uniq.action.label, uniq.action.size).then(function(data) {
                        var body = {
                            name: actionKey,
                            attributes: data
                        };
                        manualActionDao.create(entityTypes.toBackend(saas, type), entities, body);
                    });
                }
            });
        });
    }
});