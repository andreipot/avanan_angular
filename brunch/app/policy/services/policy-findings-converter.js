var m = angular.module('policy');

m.factory('policyFindingsConverter', function($q) {
    return function (input, args) {
        var deferred = $q.defer();

        var data = {};

        var rows = input.data.rows || [input.data];

        _(rows).each(function(row) {
            data[row.policy_id || args.policyId] = {
                Match: row.policy_match,
                Unmatch: row.policy_unmatch,
                Pending: row.policy_pending
            };
        });

        deferred.resolve($.extend({}, input, {
            data: data
        }));
        return deferred.promise;
    };
});