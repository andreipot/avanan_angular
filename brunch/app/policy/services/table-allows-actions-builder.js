var m = angular.module('policy');

m.factory('tableAllowsActionsBuilder', function(policyAllowsDao, routeHelper) {
    return function(policyId, tableOptions, actionTypes, allowsTypes, args) {
        function atLeastOneSelected() {
            return args.getSelectedIdsCount() > 0;
        }

        tableOptions.actions.push({
            label: 'Add to Allows',
            display: 'dropdown-button',
            active: atLeastOneSelected,
            submenus: _(allowsTypes).map(function(value, key) {
                return {
                    label: value.label,
                    active: atLeastOneSelected,
                    execute: function() {
                        args.getSelectedIds().then(function(entityIds) {
                            policyAllowsDao.create(policyId, key, 'google_file', entityIds).then(function() {
                                args.reload();
                            });
                        });
                    }
                };
            })
        });

        tableOptions.actions.push({
            label: 'Manage Allows...',
            display: 'dropdown-button',
            execute: function() {
                routeHelper.redirectTo('policy-allows', {
                    id: policyId
                });
            }
        });
    };
});