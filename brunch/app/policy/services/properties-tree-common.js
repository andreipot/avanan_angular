var m = angular.module('policy');

m.factory('propertiesTreeCommon', function() {
    return {
        removeSameEntityTypesSubMenus: function (source) {
            _(source.properties).chain().filter(function (property) {
                return _(property.sub).isArray();
            }).each(function (property) {
                property.sub = _(property.sub).filter(function (subId) {
                    return source.properties[0].id != source.properties[subId].pointer;
                });
            });
        }
    }
});