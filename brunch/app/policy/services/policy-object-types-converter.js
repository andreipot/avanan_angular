var m = angular.module('policy');

m.factory('policyObjectTypesConverter', function($q) {
    return function (input, args) {
        var deferred = $q.defer();

        var actions = input.data.actions;

        _(input.data.entities).each(function(entity) {
            if(_(entity.actions).isArray()) {
                entity.actions = _(entity.actions).map(function(action) {
                    if(_(action).isObject()) {
                        return action;
                    }
                    return actions[action];
                });
            }
        });
        delete input.data.actions;

        deferred.resolve(input);
        return deferred.promise;
    }
});