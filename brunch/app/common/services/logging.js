var m = angular.module('common');

m.factory('logging', function() {
    return {
        log: function(data, asText) {
            if(_(console).isObject() && _(console.log).isFunction()) {
                console.log(asText ? JSON.stringify(data, null, '\t') : data);
            }
        }
    }
});