var m = angular.module('common');

m.factory('singleValueConverter', function ($q) {
    return function (input, args) {
        var deferred = $q.defer();

        var rows = input.data.rows || [{}];

        var result = rows[0][args.name] || 0;

        deferred.resolve($.extend({}, input, {
            data: result
        }));

        return deferred.promise;
    }
});