var m = angular.module('common');

m.factory('rowsToObjectConverter', function ($q) {
    return function (input, args) {
        var deferred = $q.defer();

        var rows = input.data.rows || [];

        var result = _(rows).reduce(function(memo, row) {
            memo[row[args.key]] = row[args.value];
            return memo;
        }, {});

        deferred.resolve($.extend({}, input, {
            data: result
        }));

        return deferred.promise;
    }
});