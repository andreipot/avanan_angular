var m = angular.module('common');

m.factory('eventTypeListSelectConverter', function ($q) {
    return function (input, args) {
        var deferred = $q.defer();

        var rows = input.data.rows || [{}];

        var result = _(rows).map(function(row) {
            return {
                "id":row[args.name],
                "label":row[args.name]
            };
        });

        deferred.resolve($.extend({}, input, {
            data: result
        }));

        return deferred.promise;
    }
});