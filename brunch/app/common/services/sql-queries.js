var m = angular.module('common');

m.factory('sqlQueries', function(modulesManager) {
    var prefix = '/api/v1/sql_queries';
    var commonQueries = [
        'policy_catalog',
        'policy_discovers',
        'entity/geo',
        'scan_details/comodo',
        'scan_details/opswat',
        'scan_details/wildfire',
        'opswat_total',
        'opswat_infected'
    ];
    return function(name) {
        if(name.indexOf('/') == 0) {
            return prefix + name;
        }
        if(_(commonQueries).contains(name)) {
            return prefix + '/common/' + name;
        }
        return prefix + '/' + modulesManager.currentModule() + '/' + name;
    }
});