var m = angular.module('common');

m.factory('linkedText', function(routeHelper) {
    function findEnd(text) {
        var count = 1;
        var pos = 1;
        while(count > 0) {
            pos++;
            if(text[pos] == '}') {
                count--;
            } else if(text[pos] == '{') {
                count++;
            }
            if(pos == text.length) {
                return -1;
            }
        }
        return pos;
    }
    var linkedText = {
        toParts: function(text) {
            if(_(text).isNull() || _(text).isUndefined()) {
                text = '';
            }
            text = '' + text;
            var parts = [];
            while (text.length) {
                var index = text.indexOf('#{');
                if (index === -1) {
                    parts.push({
                        display: 'text',
                        data: {
                            text: text
                        }
                    });
                    text = '';
                } else if (index > 0) {
                    parts.push({
                        display: 'text',
                        data: {
                            text: text.slice(0, index)
                        }
                    });
                    text = text.slice(index);
                } else {
                    var end = findEnd(text);
                    var parsingData = text.slice(1, end + 1);
                    if(parsingData.indexOf("{'") == 0) {
                        //There bug if data contains single quote
                        parsingData = parsingData.replace(/'/g, '"');
                    }
                    var data = JSON.parse(parsingData);
                    var part = {
                        display: data.display || 'link',
                        'class': data.class,
                        data: data
                    };
                    if(part.display == 'link') {
                        part.data.path = '/#!' + routeHelper.getPath(part.data.type, part.data, part.data.qs);
                    }
                    parts.push(part);
                    text = text.slice(end + 1);
                }
            }
            return parts;
        },
        toText: function(text) {
            var parts = this.toParts(text);
            return _(parts).reduce(function(memo, part) {
                var partText = part.data.text;
                if(part.display == 'inline-list') {
                    partText = part.data.countText;
                }
                partText = partText || '';
                if(partText.indexOf('#{') == 0) {
                    return memo + linkedText.toText(partText);
                }
                return memo + partText;
            }, '');
        }
    };
    return linkedText;
});