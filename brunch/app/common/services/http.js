var m = angular.module('common');

m.factory('http', function ($http, $q, routeHelper, $route) {
    var HttpRequest = function(method, url) {
        this.data = {
            method: method,
            url: url,
            params: {},
            body: undefined,
            preFetches: [],
            converters: [],
            bodyConverters: [],
            pagination: undefined,
            filter: undefined,
            order: undefined,
            timeoutPromise: undefined,
            cache: undefined
        }
    };

    HttpRequest.prototype.preFetch = function(promise, name) {
        this.data.preFetches.push({
            promise: promise,
            name: name
        });
        return this;
    };

    HttpRequest.prototype.params = function (params) {
        $.extend(true, this.data.params, _(params).chain().map(function(param, key) {
            if(_(param).isBoolean()) {
                return [key, param ? 1 : 0];
            }
            return [key, param];
        }).object().value());
        return this;
    };

    HttpRequest.prototype.body = function (body) {
        this.data.body = body;
        return this;
    };

    HttpRequest.prototype.converter = function (converter, args) {
        if(_(converter).isUndefined()) {
            return this;
        }
        this.data.converters.push({
            converter: converter,
            args: args
        });
        return this;
    };

    HttpRequest.prototype.bodyConverter = function (converter, args) {
        if(_(converter).isUndefined()) {
            return this;
        }
        this.data.bodyConverters.push({
            converter: converter,
            args: args
        });
        return this;
    };

    HttpRequest.prototype.dynamicConverter = function(converter, args) {
        this.data.converters.push({
            dynamicConverter: converter,
            args: args
        });
        return this;
    };

    HttpRequest.prototype.count = function () {
        this.data.count = true;
        return this;
    };

    HttpRequest.prototype.pagination = function (offset, limit) {
        this.data.pagination = {
            offset: offset,
            limit: limit
        };
        return this;
    };

    HttpRequest.prototype.filter = function (filter) {
        this.data.filter = filter;
        return this;
    };

    HttpRequest.prototype.order = function (column, direction, nulls) {
        this.data.order = {
            column: column,
            direction: direction,
            nulls: nulls
        };
        return this;
    };

    HttpRequest.prototype.timeout = function (timeoutPromise) {
        if (_(timeoutPromise).isUndefined()) {
            var deferred = $q.defer();
            this.data.timeoutPromise = deferred.promise;
            return deferred;
        }
        this.data.timeoutPromise = timeoutPromise;
        return this;
    };

    HttpRequest.prototype.cache = function (timeout, name) {
        this.data.cache = {
            timeout: timeout,
            name: name
        };
        return this;
    };

    HttpRequest.prototype.run = function() {
        var deferred = $q.defer();
        this.then(function(response) {
            deferred.resolve(response);
        }, function(error) {
            deferred.reject(error);
        });
        return deferred.promise;
    };

    HttpRequest.prototype.then = function (callback, errorCallback) {
        callback = callback || _.noop;
        var originCallback = errorCallback || _.noop;
        errorCallback = function(error) {
            originCallback(error);
            if(error.status == 403 && $route.current.name != 'auth') {
                routeHelper.redirectTo('auth', {
                    page: 'login'
                });
            }
        };

        var self = this;

        function process(callback, errorCallback) {
            var deferred = $q.defer();

            function buildHttpPromise(url) {
                var methodParams = self.data.params;
                if (!_(self.data.pagination).isUndefined()) {
                    methodParams.offset = self.data.pagination.offset;
                    methodParams.limit = self.data.pagination.limit;
                }
                if (!_(self.data.filter).isUndefined()) {
                    methodParams.filterBy = self.data.filter;
                }
                if (!_(self.data.order).isUndefined()) {
                    methodParams.sortColumn = self.data.order.column;
                    methodParams.sortDirection = self.data.order.direction;
                    methodParams.orderNulls = self.data.order.nulls;
                }

                if (self.data.method == 'get') {
                    return $http[self.data.method](url, {
                        timeout: self.data.timeoutPromise,
                        params: methodParams
                    });
                }

                return processConverter(self.data.bodyConverters, self.data.body).then(function(body) {
                    return $http[self.data.method](url, body, {
                        timeout: self.data.timeoutPromise,
                        params: methodParams
                    });
                });
            }

            function processConverter(converters, input, preFetches, deferred, callback) {
                var result = {
                    promise: undefined
                };
                if(_(deferred).isUndefined()) {
                    result = deferred = $q.defer();
                }
                if (converters.length == 0) {
                    if(_(callback).isFunction()) {
                        deferred.resolve(callback(input));
                    } else {
                        deferred.resolve(input);
                    }
                    return result.promise;
                }
                var c = _(converters).first();
                var args = c.args;
                if (_(args).isFunction()) {
                    args = args(preFetches);
                }
                args = $.extend(true, {}, args, {
                    http: self.data
                });
                var converter = c.converter;
                if (_(c.dynamicConverter).isFunction()) {
                    converter = c.dynamicConverter(preFetches);
                }
                if (_(converter).isUndefined()) {
                    processConverter(_(converters).rest(), input, preFetches, deferred, callback);
                } else {
                    converter(input, args).then(function (result) {
                        processConverter(_(converters).rest(), result, preFetches, deferred, callback);
                    }, function(error) {
                        errorCallback(error);
                        deferred.reject(error);
                    });
                }
                return result.promise;
            }

            var requests = _(self.data.preFetches).map(function (preFetch) {
                return preFetch.promise;
            });

            function buildPreFetches(preFetchResponses) {
                var preFetches = {};
                _(preFetchResponses).each(function (preFetch, idx) {
                    preFetches[self.data.preFetches[idx].name] = preFetch;
                });
                return preFetches;
            }

            if (_(self.data.url).isFunction()) {
                $q.all(requests).then(function (preFetchResponses) {
                    var preFetches = buildPreFetches(preFetchResponses);
                    buildHttpPromise(self.data.url(preFetches)).then(function (httpResponse) {
                        processConverter(self.data.converters, httpResponse, preFetches, deferred, callback);
                    }, function(error) {
                        errorCallback(error);
                        deferred.reject(error);
                    });
                }, function(error) {
                    errorCallback(error);
                    deferred.reject(error);
                });
            } else {
                requests.splice(0, 0, buildHttpPromise(self.data.url));
                $q.all(requests).then(function (responses) {
                    processConverter(self.data.converters, _(responses).first(), buildPreFetches(_(responses).rest()), deferred, callback);
                }, function(error) {
                    errorCallback(error);
                    deferred.reject(error);
                });
            }

            return deferred.promise;
        }

        return process(callback, errorCallback);
    };

    function method(methodName) {
        return function (url) {
            return new HttpRequest(methodName, url);
        }
    }

    return {
        'get': method('get'),
        'put': method('put'),
        'post': method('post'),
        'delete': method('delete')
    }
});