var m = angular.module('common');

m.factory('defaultListConverter', function ($q, $filter, $injector) {
    return function(input, args) {
        var deferred = $q.defer();

        var defaultTableConverters = $injector.get('defaultTableConverters');
        var converters = $.extend({}, args.converters, defaultTableConverters(args));

        var rows = input.data.rows || [];

        function getText(data, id) {
            if(_(data).isUndefined()) {
                return undefined;
            }
            if(_(id).isNull()) {
                return data;
            }
            var dot = id.indexOf('.');
            if(~dot) {
                return getText(data[id.slice(0, dot)], id.slice(dot + 1));
            }
            return data[id];
        }

        function buildColumnsMap(row) {
            var result = {};
            _(args.columns).each(function(column) {
                result[column.id] = getText(row, column.id);
            });
            return result;
        }

        var data = _(rows).map(function(row) {
            var columnsMap = buildColumnsMap(row);
            return _(args.columns).map(function(column) {
                var original = columnsMap[column.id];
                var tags;
                if(column.type === 'tag') {
                    tags = original;
                    if(!_(tags).isArray()) {
                        tags = [tags];
                    }
                    if (_(tags).isNull() || _(tags).isUndefined()) {
                        tags = [column.def];
                    } else {
                        if (!_(column.converter).isUndefined()) {
                            tags = _(tags).map(function (tag) {
                                var converter = converters[column.converter] || column.converter;
                                return converter(tag, column.id, columnsMap, column.converterArgs || {});
                            });
                        }
                    }
                    return {
                        tags: tags,
                        originalTags: tags
                    };
                } else {
                    var text = original;
                    if (!_(column.def).isUndefined() && (_(text).isNull() || _(text).isUndefined())) {
                        text = column.def;
                    } else {
                        if (column.format == 'time') {
                            if(_(text).isString() && text.length) {
                                text = moment(text).format(column.timeFormat || 'YYYY-MM-DD HH:mm:ss');
                            } else {
                                text = '';
                            }
                        } else if (column.format == 'bytes') {
                            text = $filter('bytes')(text)
                        }
                        if (!_(column.converter).isUndefined()) {
                            var converter = converters[column.converter] || column.converter;
                            if(_(converter).isString()) {
                                alert('converter ' + converter + ' not defined');
                            }
                            text = converter(text, column.id, columnsMap, column.converterArgs || {});
                            if (_(text).isObject() && _(text.then).isUndefined()) {
                                tags = text.tags;
                                text = text.text;
                            }
                        }
                    }
                    return {
                        tags: tags,
                        text: text,
                        originalText: original
                    };
                }
            });
        });

        var currentPage;
        if(_(args.http).isObject() && _(args.http.pagination).isObject()) {
            currentPage = args.http.pagination.offset / args.http.pagination.limit + 1;
        }

        var result = {
            pagination: {
                total: input.data.total_rows || input.data.rows.length,
                current: input.data.page || currentPage
            },
            info: args.info,
            toggle: args.toggle,
            options: args.options,
            data: data
        };

        deferred.resolve($.extend({}, input, {
            data: result
        }));

        return deferred.promise;
    };
});