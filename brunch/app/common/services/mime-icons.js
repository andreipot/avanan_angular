var m = angular.module('common');

m.factory('mimeIcons', function (path, $q) {
    var root = path('common');
    var iconsPromise = root.staticFile('mime-icons.txt').retrieve().then(function(response) {
        var result = {};
        _(response.data.split(/\n/)).chain().filter(function(fileName) {
            return _(fileName).isString() && fileName.length;
        }).each(function(fileName) {
            var mimeType = fileName
                .replace('.svg', '')
                .replace('gnome-mime-', '')
                .replace('application-', 'application/')
                .replace('image-', 'image/')
                .replace('text-', 'text/')
                .replace('video-', 'video/');
            result[mimeType] = root.img('mime/' + fileName);
        }).value();
        return result;
    });

    return function () {
        return iconsPromise.then(function(icons) {
            return $q.when(function(mimeType) {
                mimeType = mimeType || '';
                return icons[mimeType] || _(icons).find(function (icon, name) {
                        return name.indexOf('/') == -1 && mimeType.indexOf(name) >= 0;
                    }) || icons.unknown;
            });
        });
    };
});