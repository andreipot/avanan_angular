var m = angular.module('common');

m.factory('defaultMapConverter', function ($q) {
    return function (input, args) {
        var deferred = $q.defer();

        var rows = input.data.rows || [];

        var fields = args.fields;

        function getText(row, fieldId) {
            var field = fields[fieldId];
            var text = row[field.id];
            if(_(text).isUndefined() || _(text).isNull()) {
                text = field.def;
            }
            if(!_(field.converter).isUndefined()) {
                args.converters[field.converter](text);
            }
            return text;
        }

        var result = {
            data: _(rows).map(function(row) {
                return {
                    latLng: [getText(row, 'lat'), getText(row, 'lon')],
                    metadata: {
                        username: {
                            label: "Username",
                            value: getText(row, 'username')
                        },
                        time: {
                            label: "Date/time",
                            value: getText(row, 'time')
                        },
                        ip: {
                            label: "IP address",
                            value: getText(row, 'ip')
                        },
                        event: {
                            label: "Event",
                            value: getText(row, 'event')
                        },
                        description: {
                            label: "Description",
                            value: getText(row, 'description')
                        },
                        country: {
                            label: "Country",
                            value: getText(row, 'country')
                        },
                        city: {
                            label: "City",
                            value: getText(row, 'city')
                        }
                    }
                }
            })
        };

        deferred.resolve($.extend({}, input, {
            data: result
        }));

        return deferred.promise;
    }
});