var m = angular.module('common');

m.factory('modals', function(confirmModal, alertModal, paramsModal, tableModal, treeModal) {
    return {
        confirm: confirmModal.show,
        alert: alertModal.show,
        params: paramsModal.show,
        table: tableModal.show,
        tree: treeModal.show
    };
});