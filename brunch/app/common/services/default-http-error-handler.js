var m = angular.module('common');

m.factory('defaultHttpErrorHandler', function(routeHelper) {
    return function(error) {
        if(error.status == 404) {
            routeHelper.redirectTo('404', {}, {replace: true});
        } else {
            routeHelper.redirectTo('500', {}, {replace: true});
        }
    };
});