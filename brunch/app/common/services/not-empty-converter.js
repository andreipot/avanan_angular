var m = angular.module('common');

m.factory('notEmptyConverter', function ($q) {
    return function (input, args) {
        var deferred = $q.defer();

        if(input.data.rows.length === 0) {
            deferred.reject(input);
        } else {
            deferred.resolve(input);
        }

        return deferred.promise;
    }
});