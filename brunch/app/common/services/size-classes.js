var m = angular.module('common');

m.factory('sizeClasses', function() {
    return function(size) {
        if(size === 'smaller') {
            return 'col-lg-2 col-md-3 col-sm-4 col-xs-4';
        }
        if(size === 'small') {
            return 'col-lg-3 col-md-4 col-sm-6 col-xs-6';
        }
        if(size === 'large') {
            return 'col-lg-12 col-md-12 col-sm-12 col-xs-12';
        }
        if(size === 'medium') {
            return 'col-lg-6 col-md-8 col-sm-12 col-xs-12';
        }
        return size;
    };
});