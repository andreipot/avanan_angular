var m = angular.module('common');

m.factory('defaultTableConverters', function(path, modulesManager) {
    return function(globalArgs) {
        return {
            imageConverter: function (text) {
                return '#' + JSON.stringify({
                        display: 'img',
                        path: text
                    });
            },
            userImageConverter: function (text, columnId, columnsMap, args) {
                var defaultPath = path('profiles').img('user.png');
                if(args.type) {
                    defaultPath = path('profiles').img(columnsMap[args.type] + '.png');
                }
                if(columnsMap[args.type] == 'share') {
                    text = path('profiles').img(args.saas + '/permissions/' + text + '.svg');
                }
                return '#' + JSON.stringify({
                        display: 'img',
                        'class': 'row-icon',
                        path: text,
                        defaultPath: defaultPath
                    });
            },
            groupImageConverter: function(text) {
                return '#' + JSON.stringify({
                        display: 'img',
                        'class': 'row-icon',
                        path: path('profiles').img('group.png')
                    });
            },
            applicationImageConverter: function (text) {
                return '#' + JSON.stringify({
                        display: 'img',
                        path: text,
                        'class': 'row-icon',
                        defaultPath: path('profiles').img('application.png')
                    });
            },
            userConverter: function (text, columnId, columnsMap, args) {
                var userId = columnsMap[args.id];
                if (_(userId).isString() && userId.length) {
                    return '#' + JSON.stringify({
                            display: 'link',
                            type: 'user',
                            id: userId,
                            text: text
                        });
                }
                return 'Anonymous';
            },
            mimeIconConverter: function (text, columnId, columnsMap, args) {
                return '#' + JSON.stringify({
                        display: 'img',
                        path: globalArgs.mimeIcons(text),
                        'class': 'row-icon',
                        tooltip: args.tooltip || columnsMap[args.niceMimeColumn]
                    });
            },
            remoteActivityExplanationConverter: function (text, columnId, columnsMap, args) {
                var firstEventName = columnsMap[args.event1];
                if (_(firstEventName).isString() && firstEventName.length) {
                    var explanation ='User ' +
                            columnsMap[args.event1] +
                                ' from ' +
                            columnsMap[args.location1] +
                                ' at ' +
                            columnsMap[args.time1] +
                                ' and ' +
                            columnsMap[args.event2] +
                                ' at ' +
                            columnsMap[args.location2] +
                                ', at ' +
                            columnsMap[args.time2] +
                                ', located ' +
                            columnsMap[args.distance] + ' KM apart';
                    return '#' + JSON.stringify({
                        display: 'text',
                        text: explanation
                    });
                }
                return 'Anonymous';
            },
            mimeTypeConverter: function(text, columnId, columnsMap) {
                return text || columnsMap['mimeType'];
            },
            fileOrFolderTitleConverter: function(text, columnId, columnMap) {
                if(columnMap.type == 'google_file') {
                    return '#' + JSON.stringify({
                            display: 'link',
                            text: text,
                            type: 'file',
                            id: columnMap.entity_id
                        });
                } else {
                    return '#' + JSON.stringify({
                            display: 'link',
                            text: text,
                            type: 'folder',
                            id: columnMap.entity_id
                        });
                }
            },
            saasIcon: function (text) {
                return '#' + JSON.stringify({
                        display: 'img',
                        'class': 'entity-type-image',
                        path: modulesManager.cloudApp(text).img
                    });
            },
            listConverter: function(text, columnId, columnsMap, args) {
                if(text.length == 0) {
                    return '';
                }
                return '#' + JSON.stringify({
                        display: 'inline-list',
                        'class': 'medium-width',
                        data: _(text).map(function(label, idx) {
                            var result = '';
                            if(args.isExternal && columnsMap[args.isExternal][idx]) {
                                result += '#' + JSON.stringify({
                                    display: 'img',
                                    path: path('common').img('external-user.png'),
                                    tooltip: 'External user',
                                    'class': 'icon-16px display-inline-block'
                                });
                            }
                            return result + '#' + JSON.stringify({
                                    display: 'link',
                                    type: args.type,
                                    id: columnsMap[args.ids][idx],
                                    text: label
                                });
                        }),
                        countText: text.length + ' ' + args.countPostfix
                    });
            },
            userWithEmail: function(text, columnId, columnsMap, args) {
                if(columnsMap['type'] == 'share') {
                    return '#' + JSON.stringify({
                            display: 'text',
                            text: text,
                            'class': 'display-block'
                        }) + '#' + JSON.stringify({
                            display: 'external-link',
                            href: globalArgs.entity.data[args.entityLinkColumn],
                            text: 'Change'
                        });
                }
                var result = '';
                if(!_(args.isExternalColumn).isUndefined() && columnsMap[args.isExternalColumn]) {
                    result += '#' + JSON.stringify({
                        display: 'img',
                        path: path('common').img('external-user.png'),
                        tooltip: 'External user',
                        'class': 'icon-16px display-inline-block'
                    });
                }
                result += '#' + JSON.stringify({
                        display: 'link',
                        'class': 'user-name display-block',
                        type: columnsMap['type'] || 'user',
                        id: columnsMap[args.userIdColumn],
                        text: text || 'Anonymous'
                    }) + '#' + JSON.stringify({
                        display: 'text',
                        'class': 'user-email',
                        text: columnsMap[args.emailColumn]
                    });
                return result;
            },
            composition: function(text, columnId, columnsMap, args) {
                return {
                    text: _(args).chain().map(function (value, key) {
                        return [key, columnsMap[value]];
                    }).object().value()
                };
            },
            fileParentsListConverter: function(text, columnId, columnsMap) {
                if(text.length == 0) {
                    return '/';
                }
                return '#' + JSON.stringify({
                        display: 'inline-list',
                        'class': 'medium-width',
                        data: _(text).map(function(label, idx) {
                            return '#' + JSON.stringify({
                                    display: 'link',
                                    type: 'folder',
                                    id: columnsMap['parent_ids'][idx],
                                    text: label
                                });
                        }),
                        countText: text.length + ' parents'
                    });
            },
            rowPendingLink: function (text, columnId, columnsMap, args) {
                if (!columnsMap[args.typeColumnName]) {
                    return text || '';
                }
                return '#' + JSON.stringify({
                        display: 'link',
                        type: columnsMap[args.typeColumnName],
                        id: columnsMap[args.idColumnName],
                        text: text
                    });
            },
            actionsIconsConverter: function(text, columnId, columnsMap, args) {
                text = text || [];
                return _(text).map(function(action) {
                    var status = action.error_code == 0 ? 'Success' : 'Failure';
                    if(action.status == 'pending') {
                        status = 'Pending';
                    }
                    var actionObj = _(globalArgs.actions[args.entity_type]).find(function(a) {
                            return a.id == action.name;
                        }) || {};
                    var actionLabel = actionObj.label || action.name;

                    var result = '#' + JSON.stringify({
                            display: 'img',
                            path: path('policy').ctrl('policy').img(action.name + '.png'),
                            'class': 'action-img ' + action.status,
                            tooltip: {
                                content: '<div>Action: ' + actionLabel + '</div>' +
                                '<div>Time: ' + moment(action.time_start).format('YYYY-MM-DD HH:mm:ss') + '</div>' +
                                '<div>Status: ' + status + '</div>',
                                position: {
                                    my: 'bottom right',
                                    at: 'top center'
                                }
                            }
                        });
                    if(action.status == 'done') {
                        var image = action.error_code == 0 ? 'success' : 'failure';
                        result += '#' + JSON.stringify({
                            display: 'img',
                            path: path('policy').ctrl('policy').img(image + '.png'),
                            'class': 'action-status'
                        });
                    }
                    result = '#' + JSON.stringify({
                        display: 'linked-text',
                        text: result,
                        'class': 'action-img-with-status'
                    });
                    return result;
                }).join('');
            },
            arrayLengthConverter: function(text) {
                return text.length;
            }
        }
    };
});