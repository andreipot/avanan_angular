var m = angular.module('common');

m.factory('rowsNameConverter', function($q) {
    return function(input, args) {
        var deferred = $q.defer();

        var data = {
            rows: input.data[args.name]
        };

        deferred.resolve($.extend({}, input, {
            data: data
        }));
        return deferred.promise;
    };
});