var m = angular.module('common');

m.factory('eventTypesList', function (eventsDao) {
    return function () {
        return  eventsDao.retrieveTypesList({
                drive: true,
                login: true,
                token: true
            });
    }
});