var m = angular.module('common');

m.factory('entityTypes', function() {
    var data = {
        google_drive: {
            application: 'google_app',
            file: 'google_file',
            folder: 'google_folder',
            user: 'google_user'
        }
    };

    var reverse = {};
    var saasReverse = {};
    _(data).each(function(saas, saasName) {
        _(saas).each(function(value, key) {
            reverse[value] = key;
            saasReverse[value] = saasName;
        });
    });

    return {
        toBackend: function (saas, type) {
            return (data[saas] || {})[type];
        },
        toLocal: function(type) {
            return reverse[type];
        },
        toSaas: function(type) {
            return saasReverse[type];
        }
    };
});