var m = angular.module('common');

m.factory('markedDom', function() {
    var elements = {};
    return {
        mark: function(name, element) {
            elements[name] = element;
        },
        unmark: function(name) {
            delete elements[name];
        },
        lookup: function(name, css) {
            var element = elements[name];
            if(_(css.isUndefined())) {
                return element;
            }
            return element.css(css);
        }
    };
});