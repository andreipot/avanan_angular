var m = angular.module('common');

m.factory('singleRowConverter', function ($q) {
    return function (input, args) {
        var deferred = $q.defer();

        var result = _(input.data.rows).first() || {};

        deferred.resolve($.extend({}, input, {
            data: result
        }));

        return deferred.promise;
    }
});