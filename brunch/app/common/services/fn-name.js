var m = angular.module('common');

m.factory('fnName', function() {
    function testFunc() {
    }
    if(testFunc.name == 'testFunc') {
        //In modern browsers with ES6 this work
        return function(fn) {
            return fn.name;
        }
    }
    //In other this
    return function(fn) {
        var split = /^function\s+([\w\$]+)\s*\(/.exec(fn.toString());
        return split == null ? '' : split[1];
    }
});