var m = angular.module('common');

m.factory('feature', function($q, http) {
    var initialized = false;
    var result = {
        init: function() {
            if(initialized) {
                return $q.when({});
            }
            var deferred = $q.defer();
            http.get('/api/v1/json_files/conf/ui_features.json').then(function (response) {
                $.extend(true, result, response.data);
                initialized = true;
                deferred.resolve(response);
            }, function(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        }
    };

    return result;
});