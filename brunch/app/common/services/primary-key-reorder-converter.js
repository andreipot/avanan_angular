var m = angular.module('common');

m.factory('primaryKeyReorderConverter', function ($q, columnsHelper) {
    return function (input, args) {
        var deferred = $q.defer();

        var primaryIdx = columnsHelper.getPrimaryIdx(args.columns);
        if(args.primaryKeyColumn) {
            primaryIdx = columnsHelper.getIdxById(args.columns, args.primaryKeyColumn);
        }


        input.data.data = _(args.ids).map(function(id) {
            return _(input.data.data).find(function(row) {
                return row[primaryIdx].originalText == id;
            });
        });

        deferred.resolve(input);

        return deferred.promise;
    }
});