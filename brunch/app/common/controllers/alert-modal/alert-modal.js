var m = angular.module('common');

m.controller('AlertModalController', function($scope, $modalInstance, message, options) {
    $scope.message = message;
    $scope.options = $.extend({
        display: 'text'
    }, options, true);

    $scope.close = function() {
        $modalInstance.close();
    };
});

m.factory('alertModal', function($modal, path) {
    return {
        show: function(message, options, size) {
            var modalInstance = $modal.open({
                templateUrl: path('common').ctrl('alert-modal').template(),
                controller: 'AlertModalController',
                size: size,
                resolve: {
                    message: _.constant(message),
                    options: _.constant(options)
                }
            });
            return modalInstance.result;
        }
    }
});