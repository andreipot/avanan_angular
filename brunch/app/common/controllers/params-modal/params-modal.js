var m = angular.module('common');

m.controller('ParamsModalController', function($scope, $modalInstance, pages, title, policyPropertyParser) {
    var paramTypes = {
        'string': {
            def: ''
        },
        'integer': {
            outConverter: function(param) {
                param.data = parseInt(param.data);
            }
        },
        'password': {
            def: ''
        },
        'email-body': {
            def: ''
        },
        'email': {
            def: {
                to: '',
                cc: '',
                bcc: '',
                subject: '',
                body: ''
            }
        },
        'cef-items': {
            def: [],
            inConverter: function(param) {
                param.data = _(param.data).map(function(e) {
                    if(e.type == 'property') {
                        e.data = policyPropertyParser(e.data);
                    }
                    return e;
                });
            },
            outConverter: function(param) {
                param.data = _(param.data).map(function(e) {
                    if(e.type == 'property') {
                        e.data = e.data.fullId;
                    }
                    return e;
                });
            }
        },
        'property': {
            inConverter: function(param, page) {
                var input = param.data;

                var properties = page.getProperties(param.propertyType);
                if(properties[0].sub.length == 1) {
                    var firstProperty = properties[_(properties[0].sub).first()];
                    if(_(firstProperty.sub).isUndefined() && firstProperty.selectable){
                        input = firstProperty.id;
                        param.hidden = true;
                    }
                }

                if(_(input).isString()) {
                    input = input.replace(/^\{/, '').replace(/}$/, '');
                    if(input.length) {
                        input = '.' + input;
                    }
                }
                param.data = policyPropertyParser(input);
            },
            outConverter: function(param) {
                var input = param.data;
                if(_(input.fullId).isString() && input.fullId[0] == '.') {
                    param.data = '{' + input.fullId.slice(1) + '}';
                } else {
                    param.data = undefined;
                }
            }
        },
        'boolean': {
            def: false
        }
    };

    $scope.title = title;
    $scope.pages = _(pages).map(function(page) {
        return $.extend({}, page, {
            params: _(page.params).map(function(param) {
                param = $.extend({}, param, true);
                if(!_(paramTypes[param.type]).isUndefined()) {
                    param.data = param.data || paramTypes[param.type].def;
                    var inConverter = paramTypes[param.type].inConverter || _.identity;
                    inConverter(param, page);
                }
                return param;
            })
        });
    });
    var pageSeq = [0, $scope.pages[0].nextPage];
    var pageSeqNumber = 0;

    $scope.radioClick = function(button) {
        if(!_(button.nextPage).isUndefined()) {
            if(pageSeq[pageSeqNumber + 1] != button.nextPage) {
                pageSeq = pageSeq.slice(0, pageSeqNumber + 1);
                pageSeq.push(button.nextPage);
            }
        }
    };

    $scope.addToList = function(param, type) {
        param.data = param.data || [];
        param.data.push({
            type: type
        });
    };

    $scope.removeFromList = function(param, index) {
        param.data.splice(index, 1);
    };

    $scope.cancel = function() {
        $modalInstance.dismiss();
    };

    function validate() {
        var result = _($scope.pages[$scope.page()].params).find(function(param) {
            var data = param.data;
            var error = _(param.validation || {}).find(function(value, key) {
                var message = undefined;
                if(_(value).isObject()) {
                    message = value.message;
                    value = value.value;
                }
                var error = undefined;
                if(key === 'required' && _(data).isUndefined() || _(data).isNull()) {
                    error =  message || ('Field required');
                }
                if(key === 'minLength' && (!_(data).isString() || data.length < value)) {
                    error =  message || ('Minimum ' + value + ' symbols');
                }
                if(key === 'maxLength' && (!_(data).isString() || data.length > value)) {
                    error =  message || ('Maximum ' + value + ' symbols');
                }
                if(key === 'regexp' && _(data).isString() && !data.match(value)) {
                    error = message || ('Regexp ' + value + ' is not pass');
                }
                param.error = error;
                return !_(error).isUndefined();
            });
            return !_(error).isUndefined();
        });
        return _(result).isUndefined();
    }

    $scope.ok = function() {
        if(!validate()) {
            return;
        }
        $modalInstance.close(_(pageSeq).reduce(function(memo, page) {
            if(_(page).isUndefined() || _(page).isNull()) {
                return memo;
            }
            return _($scope.pages[page].params).reduce(function(memo, param){
                if(!_(param.id).isUndefined()) {
                    if(!_(paramTypes[param.type]).isUndefined()) {
                        var outConverter = paramTypes[param.type].outConverter || _.identity;
                        outConverter(param);
                    }
                    memo[param.id] = param.data;
                }
                return memo;
            }, memo);
        }, {}));
    };

    $scope.page = function() {
        return pageSeq[pageSeqNumber];
    };

    $scope.prev = function() {
        if(!$scope.isFirstPage()) {
            pageSeqNumber--;
        }
    };

    $scope.next = function() {
        if(!validate()) {
            return;
        }
        if($scope.isNextPageEnable()) {
            pageSeqNumber++;
            if(pageSeqNumber == pageSeq.length - 1) {
                pageSeq.push($scope.pages[$scope.page()].nextPage);
            }
        }
    };

    $scope.isNextPageEnable = function() {
        return !_(pageSeq[pageSeqNumber + 1]).isNull();
    };

    $scope.isFirstPage = function() {
        return pageSeqNumber === 0;
    };

    $scope.isLastPage = function() {
        return $scope.pages[$scope.page()].lastPage;
    };
});

m.factory('paramsModal', function($modal, path) {
    return {
        show: function(pages, title, size) {
            var modalInstance = $modal.open({
                templateUrl: path('common').ctrl('params-modal').template(),
                controller: 'ParamsModalController',
                size: size,
                resolve: {
                    pages: _.constant(pages),
                    title: _.constant(title)
                }
            });
            return modalInstance.result;
        }
    }
});
