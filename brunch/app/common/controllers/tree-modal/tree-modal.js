var m = angular.module('common');

m.controller('TreeModalController', function($scope, $modalInstance, args, options) {
    $scope.args = $.extend({
        title: 'Tree dialog',
        okText: 'Ok'
    }, args);

    $scope.options = options;
    $scope.model = {
        selected: undefined
    };

    $scope.cancel = function() {
        $modalInstance.dismiss();
    };

    $scope.ok = function() {
        $modalInstance.close($scope.model.selected);
    };
});

m.factory('treeModal', function($modal, path) {
    return {
        show: function(args, options, size) {
            var modalInstance = $modal.open({
                templateUrl: path('common').ctrl('tree-modal').template(),
                controller: 'TreeModalController',
                size: size,
                resolve: {
                    args: _.constant(args),
                    options: _.constant(options)
                }
            });
            return modalInstance.result;
        }
    };
});