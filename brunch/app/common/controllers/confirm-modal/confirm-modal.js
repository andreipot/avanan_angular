var m = angular.module('common');

m.controller('ConfirmModalController', function($scope, $modalInstance, args) {
    $scope.args = $.extend({
        title: 'Confirmation dialog',
        okText: 'Ok'
    }, args);

    $scope.cancel = function() {
        $modalInstance.dismiss();
    };

    $scope.ok = function() {
        $modalInstance.close();
    };
});

m.factory('confirmModal', function($modal, path) {
    return {
        show: function(args, size) {
            if(!_(args).isObject()) {
                args = {
                    message: args
                };
            }
            var modalInstance = $modal.open({
                templateUrl: path('common').ctrl('confirm-modal').template(),
                controller: 'ConfirmModalController',
                size: size,
                resolve: {
                    args: _.constant(args)
                }
            });
            return modalInstance.result;
        }
    }
});