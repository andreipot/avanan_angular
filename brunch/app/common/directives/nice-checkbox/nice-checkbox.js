var m = angular.module('common');

m.directive('niceCheckbox', function() {
    return {
        restrict: 'A',
        replace: true,
        template: '<span class="nice-checkbox" ng-click="onClick()">' +
        '<span class="nice-checkbox-control" ng-class="getStatus()"></span>' +
        '<span class="nice-checkbox-text">{{text}}</span>' +
        '</span>',
        scope: {
            model: '=niceCheckbox',
            onChange: '&?',
            text: '='
        },
        link: function($scope, element, attrs) {
            $scope.model = $scope.model || false;
            $scope.onChange = $scope.onChange || _.noop;

            $scope.getStatus = function() {
                return $scope.model ? 'checked' : 'unchecked';
            };

            $scope.onClick = function() {
                $scope.model = !$scope.model;
                $scope.onChange();
            };
        }
    }
});