var m = angular.module('common');

m.directive('datetimePicker', function() {
    return {
        restrict: 'A',
        replace: false,
        scope: {
            options: '=?datetimePicker'
        },
        link: function($scope, element, attrs) {
            var options = $.extend({
                icons: {
                    time: "fa fa-clock-o",
                    date: "fa fa-calendar",
                    up: "fa fa-arrow-up",
                    down: "fa fa-arrow-down"
                }
            }, $scope.options);
            element.datetimepicker(options);
        }
    }
});