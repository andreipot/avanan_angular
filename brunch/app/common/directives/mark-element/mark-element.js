var m = angular.module('common');

m.directive('markElement', function(markedDom) {
    return {
        restrict: 'A',
        replace: false,
        scope: false,
        link: function($scope, element, attrs) {
            var name = attrs.watchElement;
            markedDom.mark(name, element);

            $scope.$on('$destroy', function() {
                markedDom.unmark(name);
            });
        }
    }
});