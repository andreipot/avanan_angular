var m = angular.module('common');

m.directive('qtip2', function($parse) {
    return {
        restrict: 'A',
        replace: false,
        scope: false,
        link: function($scope, element, attrs) {
            var api = null;
            function destroy() {
                if(api) {
                    api.destroy();
                    //element.qtip('destroy', true);
                    api = null;
                }
            }
            $scope.$watch(function() {
                return $parse(attrs.qtip2)($scope);
            }, function(params) {
                if(api) {
                    api.set('content', params);
                } else if (_(params).isString()) {
                    params = {
                        content: params,
                        position: {
                            my: 'bottom right',
                            at: 'top center'
                        }
                    };
                    var tooltip = element.qtip(params);
                    api = tooltip.qtip('api');
                }
            }, true);

            $scope.$on('$destroy', destroy);
        }
    }
});