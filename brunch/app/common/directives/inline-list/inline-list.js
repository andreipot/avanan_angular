var m = angular.module('common');

m.directive('inlineList', function(path) {
    return {
        restrict: 'A',
        replace: true,
        templateUrl: path('common').directive('inline-list').template(),
        scope: {
            model: '=inlineList'
        },
        link: function($scope, element, attrs) {

        }
    }
});