var m = angular.module('common');

m.directive('splitter', function($parse, $timeout) {
    return {
        restrict: 'A',
        replace: false,
        link: function($scope, element, attrs) {
            var leftPanel = element.children(":first");
            var rightPanel = leftPanel.next();
            var timeout = null;
            $scope.$watch(function() {
                return $parse(attrs.splitter)($scope).disabled;
            }, function(disabled) {
                if(disabled) {
                    if(_(element.destroy).isFunction()) {
                        element.destroy();
                    }
                } else {
                    element.split($parse(attrs.splitter)($scope));
                    $timeout.cancel(timeout);
                    updateHeight();
                }
            });

            function updateHeight() {
                var leftHeight = 500;
                leftPanel.children().each(function() {
                    leftHeight += $(this).outerHeight();
                });
                var rightHeight = 500;
                rightPanel.children().each(function() {
                    rightHeight += $(this).outerHeight();
                });
                element.height(Math.max(leftHeight, rightHeight) + 20 + 'px');
                if(leftHeight > rightHeight) {
                    rightPanel.height(leftHeight);
                } else {
                    leftPanel.height(rightHeight);
                }
                if(_(element.refresh).isFunction()) {
                    element.refresh();
                }
                timeout = $timeout(updateHeight, 500);
            }
            timeout = $timeout(updateHeight, 100);
        }
    }
});