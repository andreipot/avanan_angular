var m = angular.module('common');

m.directive('formParam', function(path, modals) {
    return {
        restrict: 'A',
        replace: true,
        templateUrl: path('common').directive('form-param').template(),
        scope: {
            param: '=formParam',
            model: '=ngModel',
            promiseresult:'=promiseResult',
            properties: '&?'
        },
        link: function($scope, element, attrs) {
            $scope.properties = $scope.properties || _.noop;

            $scope.promiseresult = $scope.promiseresult || _.noop;


            $scope.internal = {
                model: $scope.model
            };
            $scope.resolvedProperties = $scope.properties({propertyType: $scope.param.propertyType});

            $scope.emailOptions = {
                script_url : '/assets/javascript/tinymce/tinymce.min.js',
                plugins: [
                    "advlist autolink lists link image charmap print preview anchor",
                    "searchreplace visualblocks code",
                    "insertdatetime media table contextmenu paste textcolor"
                ],
                menubar: "format",
                toolbar: "undo redo | styleselect | bold italic forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image tags",
                statusbar: false
            };

            $scope.addProperty = function(menuId, withTag) {
                $scope.internal.model = $scope.internal.model || '';
                if(withTag) {
                    $scope.internal.model += '<p>';
                }
                $scope.internal.model += '{' + menuId.fullId + '}';
                if(withTag) {
                    $scope.internal.model += '</p>';
                }
            };

            $scope.openTreeModal = function() {
                var popup = $scope.param.popup || {};
                modals.tree({
                    title: popup.title
                }, $scope.param.options, popup.size).then(function(selected) {
                    $scope.internal.model = selected;
                });
            };

            $scope.$watch('internal.model', function(m) {
                $scope.model = $scope.internal.model;
            }, true);
        }
    }
});