var m = angular.module('common');

m.directive('tinymce', function($timeout) {
    return {
        restrict: 'A',
        replace: false,
        scope: {
            options: '=tinymce',
            model: '=tinymceModel'
        },
        link: function($scope, element, attrs) {
            var lastContent;
            var options = $.extend({}, $scope.options, {
                setup: function(editor) {
                    element.html($scope.model);
                    editor.on('change undo redo', function(e) {
                        $timeout(function() {
                            lastContent = $scope.model = editor.getContent();
                        });
                    });
                    if(_($scope.options.setup).isFunction()) {
                        $scope.options.setup(editor);
                    }
                }
            });
            element.tinymce(options);

            $scope.$watch('model', function(model) {
                if(model != lastContent) {
                    lastContent = model;
                    element.html($scope.model);
                }
            });
        }
    }
});