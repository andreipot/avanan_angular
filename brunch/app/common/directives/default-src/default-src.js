var m = angular.module('common');

m.directive('defaultSrc', function($parse) {
    return {
        restrict: 'A',
        replace: false,
        link: function($scope, element, attrs) {
            var defaultSrc = $parse(attrs.defaultSrc)($scope);
            if(_(defaultSrc).isString() && defaultSrc.length) {
                element.attr('src', defaultSrc);
                element.error(function () {
                    element.attr('src', defaultSrc);
                });
            }
        }
    }
});