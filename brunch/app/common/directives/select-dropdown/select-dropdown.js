var m = angular.module('common');

m.directive('selectDropdown', function(path) {
    return {
        restrict: 'A',
        replace: true,
        templateUrl: path('common').directive('select-dropdown').template(),
        scope: {
            options: '=selectDropdown',
            onChange: '&?'
        },
        link: function ($scope, element, attrs) {
            $scope.onChange = $scope.onChange || _.noop;
            $scope.selectionChanged = function(event, option) {
                event.stopPropagation();
                $scope.onChange({option: option});
            };
        }
    }
});