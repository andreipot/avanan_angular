var m = angular.module('common');

m.directive('stackedProgress', function(path) {
    return {
        restrict: 'A',
        replace: true,
        templateUrl: path('common').directive('stacked-progress').template(),
        scope: {
            model: '=stackedProgress'
        },
        link: function($scope, element, attrs) {
        }
    }
});