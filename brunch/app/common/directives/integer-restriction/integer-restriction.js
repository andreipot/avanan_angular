var m = angular.module('common');

m.directive('integerRestriction', function($timeout, $parse) {
    return {
        restrict: 'A',
        replace: false,
        scope: false,
        link: function($scope, element, attrs) {
            element.bind('keypress', function(e) {
                var key;
                var keychar;

                if (window.event) {
                    key = window.event.keyCode;
                } else if (e) {
                    key = e.which;
                } else {
                    return true;
                }
                keychar = String.fromCharCode(key);

                if (_([null, 0, 8, 9, 13, 27]).contains(key) || ("0123456789").indexOf(keychar) > -1) {
                    return true;
                }
                return false;
            });

            element.bind('blur', function() {
                $timeout(function() {
                    var model = $parse(attrs.ngModel);
                    if(model($scope) == '') {
                        model.assign($scope, '0');
                    }
                });
            })
        }
    };
});