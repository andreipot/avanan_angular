var m = angular.module('common');

m.directive('textCheckBox', function(path) {
    return {
        restrict: 'A',
        replace: true,
        template: '<div class="text-check-box" ng-class="checkBoxClass()" ng-click="onClick()"><span>{{getText() || "H"}}</span></div>',
        scope: {
            model: '=textCheckBox',
            checkLabel: '@?',
            uncheckLabel: '@?'
        },
        link: function($scope, element, attrs) {
            $scope.getText = function() {
                if($scope.model) {
                    return $scope.checkLabel;
                }
                return $scope.uncheckLabel;
            };

            $scope.onClick = function() {
                $scope.model = !$scope.model;
            };

            $scope.checkBoxClass = function() {
                var result = $scope.model ? 'checked' : 'unchecked';
                if(_($scope.getText()).isUndefined() || $scope.getText().length == 0) {
                    result += ' hidden-span';
                }
                return result;
            };
        }
    }
});