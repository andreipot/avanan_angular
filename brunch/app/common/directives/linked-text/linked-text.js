var m = angular.module('common');

m.directive('linkedText', function($location, path, routeHelper, linkedText, recursionHelper, $parse, fnName) {
    return {
        restrict: 'A',
        replace: false,
        templateUrl: path('common').directive('linked-text').template(),
        scope: false,
        //scope: {
        //    text: '=linkedText'
        //},
        compile: function(element) {
            return recursionHelper.compile(element, this.link);
        },
        link: function($scope, element, attrs) {
            $scope.routeHelper = routeHelper;
            $scope.textParts = [];
            var cached = null;
            var maxCount = 10;
            var count = maxCount;
            function resetCount() {
                count = maxCount;
                cached = null;
            }
            $scope.$watch(function() {
                var parsed = $parse(attrs.linkedText);
                if(fnName(parsed) != '$parseFunctionCall') {
                    return parsed($scope);
                }
                if(count == 0) {
                    resetCount();
                }
                if(cached != null) {
                    count--;
                    return cached;
                }
                return cached = parsed($scope);
            }, function(text) {
                text = text || '';
                if(_(text.then).isFunction()) {
                    if(_($scope.textParts).isUndefined()) {
                        $scope.textParts = linkedText.toParts('Loading...');
                    }
                    text.then(function(resolvedText) {
                        $scope.textParts = linkedText.toParts(resolvedText);
                        resetCount();
                    });
                } else {
                    var parts = linkedText.toParts(text);
                    if(parts.length == $scope.textParts.length) {
                        $.extend(true, $scope.textParts, linkedText.toParts(text));
                    } else {
                        $scope.textParts = parts;
                    }
                }
            }, true);
        }
    }
});