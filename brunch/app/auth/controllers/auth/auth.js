var m = angular.module('auth');

m.controller('AuthController', function($scope, http, oauthPopup, path, avananUserDao, routeHelper, $routeParams, $location) {
    $scope.path = path;
    var root = path('auth').ctrl('auth');
    $scope.error = {};
    $scope.page = $routeParams.page;
    if($scope.page == 'password-reset') {
        var qs = $location.search();
        $scope.token = qs.token;
        if(_($scope.token).isUndefined()) {
            changePage('login');
        }
    }
    $scope.loginModel = {
        email: '',
        password: '',
        repeatPassword: ''
    };
    $scope.loginButtons = [{
        'class': 'google-button',
        image: root.img('google.jpg'),
        text: 'Login with Google',
        execute: function() {
            oauthPopup('/auth/login_google');
        }
    }, {
        'class': 'dropbox-button',
        image: root.img('dropbox.jpg'),
        text: 'Login with Dropbox',
        execute: function() {
            oauthPopup('/auth/login_dropbox');
        }
    }, {
        'class': 'box-button',
        image: root.img('box.jpg'),
        text: 'Login with Box',
        execute: function() {
            oauthPopup('/auth/login_box');
        }
    }, {
        'class': 'password-button',
        icon: 'fa-unlock-alt',
        text: 'Login with password',
        execute: function() {
            changePage('password-login');
        }
    }];

    $scope.login = function() {
        $scope.error = {};
        avananUserDao.login($scope.loginModel.email, $scope.loginModel.password).then(function() {
            routeHelper.redirectTo('dashboard');
        }, function(error) {
            $scope.error.email = error.data.message;
            $scope.error.password = error.data.message;
        });
    };

    $scope.restore = function() {
        $scope.error = {};
        avananUserDao.forgot($scope.loginModel.email).then(function() {
            changePage('email-sent');
        }, function(error) {
            $scope.error.email = error.data.message;
        });
    };

    $scope.back = function() {
        changePage('login');
    };

    $scope.restorePassword = function(event) {
        event.preventDefault();
        changePage('password-restore');
    };

    $scope.changePassword = function() {
        $scope.error = {};
        if($scope.loginModel.password.length == 0) {
            $scope.error.password = 'Password should not be empty';
            return;
        }
        if($scope.loginModel.repeatPassword != $scope.loginModel.password) {
            $scope.error.repeatPassword = 'Passwords are not same';
            return;
        }

        avananUserDao.reset($scope.token, $scope.loginModel.password).then(function() {
            changePage('password-login');
        }, function(error) {
            $scope.error.password = error.data.message;
        })
    };

    function changePage(newPage) {
        $scope.error = {};
        routeHelper.redirectTo('auth', {
            page: newPage
        }, {replace: true, reload: false});
        $scope.page = newPage;
    }

    $scope.getTitle = function() {
        return {
            'login': 'Log In to Avanan',
            'password-login': 'Log In to Avanan',
            'password-restore': 'Password restore',
            'email-sent': 'Password restore',
            'password-reset': 'Password reset'
        }[$scope.page];
    };
});