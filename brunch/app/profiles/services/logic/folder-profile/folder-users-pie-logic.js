var m = angular.module('profiles');

m.factory('folderUsersPieLogic', function(widgetPolling, folderDao) {
    return function($scope) {
        var id = widgetPolling.scope().id;

        folderDao.usersRatio(id).then(function(response) {
            $scope.model.flotOptions.series.pie.label.formatter = function(label,point){
                return (point.percent.toFixed(1) + '%');
            };
            $scope.model.data = [{
                label: "External users",
                data: response.data[false]
            }, {
                label: "Internal users",
                data: response.data[true]
            }];
        });
    }
});