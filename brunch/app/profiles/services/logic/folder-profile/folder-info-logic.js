var m = angular.module('profiles');

m.factory('folderInfoLogic', function(widgetPolling, mimeIcons, $filter, $q, folderDao) {
    return function($scope, logic) {
        var id = widgetPolling.scope().id;
        $q.all([
            folderDao.retrieve(id),
            mimeIcons()
        ]).then(function(responses) {
            var folderInfo = responses[0];
            var icons = responses[1];

            var parentIds = _([folderInfo.data[logic.parentIdsColumn]]).flatten();
            var parentTitles = _([folderInfo.data[logic.parentTitlesColumn]]).flatten();

            $scope.model.lines = [
                {
                    "img": icons(folderInfo.data[logic.mimeTypeColumn] || 'folder'),
                    "class": "profile-image"
                }, {
                    "text": folderInfo.data[logic.titleColumn],
                    "class": "folder-name"
                }, {
                    "text": '#' + JSON.stringify({
                        display: 'inline-list',
                        data: parentIds.length == 0 ? '/' : _(parentIds).chain().map(function(id, idx) {
                            var title = parentTitles[idx];
                            if(_(title).isString() && title.length) {
                                return '#' + JSON.stringify({
                                        display: 'link',
                                        text: title,
                                        type: 'folder',
                                        id: id
                                    });
                            }
                            return null;
                        }).filter(function(link) {
                            return !_(link).isNull();
                        }).value(),
                        countText: 'Parents'
                    }),
                    'class': 'folder-parents'
                }
            ];

            if(!_(folderInfo.data[logic.sizeColumn]).isUndefined()) {
                $scope.model.lines.push({
                    "text": $filter('bytes')(folderInfo.data[logic.sizeColumn]),
                    "class": "folder-size"
                });
            }

            if(!_(folderInfo.data[logic.linkColumn]).isUndefined()) {
                $scope.model.lines.push({
                    "text": '#' + JSON.stringify({
                        display: 'external-link',
                        text: 'View folder',
                        href: folderInfo.data[logic.linkColumn]
                    }),
                    "class": "folder-link"
                })
            }
        });
    }
});