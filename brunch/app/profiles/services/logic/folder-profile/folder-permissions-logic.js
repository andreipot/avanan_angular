var m = angular.module('profiles');

m.factory('folderPermissionLogic', function(widgetPolling, folderDao, simpleLogicCore) {
    return function($scope, logic) {
        var id = widgetPolling.scope().id;
        return simpleLogicCore($scope, logic, {
            retrieve: function() {
                return folderDao.permissions($scope.model.columns, id);
            }
        });
    }
});