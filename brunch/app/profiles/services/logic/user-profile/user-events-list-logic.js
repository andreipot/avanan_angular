var m = angular.module('profiles');

m.factory('userEventsListLogic', function(eventsDao, eventsListLogicCore, widgetPolling) {
    return function ($scope, logic) {
        var userId = widgetPolling.scope().id;

        return eventsListLogicCore($scope, logic, {
            retrieveTypes: function() {
                return eventsDao.retrieveTypes({
                    drive: true,
                    login: true
                });
            },
            retrieve: function(after) {
                return eventsDao.retrieve($scope.model.columns, after, userId);
            },
            timeColumnName: logic.timeColumnName
        });
    }
});