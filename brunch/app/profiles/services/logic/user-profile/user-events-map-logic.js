var m = angular.module('profiles');

m.factory('userEventsMapLogic', function(eventsDao, widgetPolling, eventsMapLogicCore) {
    return function($scope, logic) {
        var userId = widgetPolling.scope().id;
        return eventsMapLogicCore($scope, logic, {
            retrieve: function(after) {
                return eventsDao.retrieveMap($scope.model.fields, after, userId);
            },
            timeColumnName: logic.timeColumnName
        });
    }
});