var m = angular.module('profiles');

m.factory('userFileShareLogic', function(userDao, widgetPolling, simpleLogicCore) {
    return function ($scope, logic) {
        var id = widgetPolling.scope().id;
        return simpleLogicCore($scope, logic, {
            retrieve: function() {
                return userDao.fileShare(logic.days, id);
            }
        })
    }
});