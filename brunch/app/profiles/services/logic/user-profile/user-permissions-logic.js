var m = angular.module('profiles');

m.factory('userPermissionsLogic', function(userDao, widgetPolling, paginatedListLogicCore) {
    return function ($scope, logic) {
        var id = widgetPolling.scope().id;
        return paginatedListLogicCore($scope, logic, {
            retrieve: function() {
                return userDao.permissions($scope.model.columns, id);
            }
        });
    }
});