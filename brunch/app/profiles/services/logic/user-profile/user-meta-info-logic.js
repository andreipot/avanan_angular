var m = angular.module('profiles');

m.factory('userMetaInfoLogic', function(metaInfoCoreLogic, widgetPolling, userDao) {
    return function($scope, logic) {
        var id = widgetPolling.scope().id;

        return metaInfoCoreLogic($scope, logic, {
            blackList: ['installed_apps'],
            retrieve: function() {
                return userDao.retrieve(id);
            }
        });
    }
});