var m = angular.module('profiles');

m.factory('userFileShareTypesLogicCore', function() {
    return function ($scope, logic, coreOptions) {
        coreOptions.retrieve().then(function(response) {
            var rows = response.data.rows || [];
            rows = _(rows).chain().sortBy(function(row) {
                return row.count;
            }).first(10).value();
            var idx = 0;
            $scope.model.flotOptions.xaxis.tickFormatter = function(text) {
                return parseInt(text);
            };
            var ticks = $scope.model.flotOptions.yaxis.ticks = [];
            for(var i = 0; i < 10 - rows.length; i++) {
                ticks.push([idx++, '']);
            }
            var mimeToIdx = {};
            _(rows).each(function(row) {
                mimeToIdx[row.mimeType] = idx++;
                ticks.push([mimeToIdx[row.mimeType], row.nice_string || row.mimeType.replace('application', 'app')]);
            });
            $scope.model.data = _(rows).map(function(row) {
                return {
                    label: row.mimeType,
                    data: [[row.count, mimeToIdx[row.mimeType]]]
                };
            });
            $scope.model.style = $scope.model.style || {};
            $.extend($scope.model.style, {
                height: idx * 30 + 'px'
            });
        });

        return _.noop;
    }
});