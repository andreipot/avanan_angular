var m = angular.module('profiles');

m.factory('userInfoLogic', function(userDao, widgetPolling, path) {
    return function ($scope, logic) {
        var id = widgetPolling.scope().id;
        userDao.retrieve(id).then(function(response) {
            var photoUrl = path('profiles').img('user.png');
            if(_(response.data[logic.imgColumn]).isString()) {
                photoUrl = response.data[logic.imgColumn];
            }
            $scope.model.lines = [
                {
                    "img": photoUrl,
                    "defaultImg": path('profiles').img('user.png'),
                    "class": "profile-image"
                }, {
                    "text": response.data[logic.nameColumn],
                    "class": "user-name"
                }, {
                    "text": response.data[logic.emailColumn],
                    "class": "user-email"
                }
            ]
        });
    }
});