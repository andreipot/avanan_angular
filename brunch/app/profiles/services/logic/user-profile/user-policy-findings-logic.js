var m = angular.module('widgets');

m.factory('userPolicyFindingsLogic', function(userDao, eventsListLogicCore, widgetPolling) {
    return function ($scope, logic) {
        var userId = widgetPolling.scope().id;
        return eventsListLogicCore($scope, logic, {
            retrieve: function(after) {
                return userDao.policyFindings($scope.model.columns, after, userId);
            },
            timeColumnName: 'insert_date',
            filter: false
        });
    }
});