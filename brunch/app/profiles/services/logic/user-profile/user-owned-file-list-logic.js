var m = angular.module('profiles');

m.factory('userOwnedFileListLogic', function(userDao, widgetPolling, paginatedListLogicCore) {
    return function ($scope, logic) {
        var id = widgetPolling.scope().id;
        return paginatedListLogicCore($scope, logic, {
            retrieve: function() {
                return userDao.filesOwnedList($scope.model.columns, id);
            }
        });
    }
});