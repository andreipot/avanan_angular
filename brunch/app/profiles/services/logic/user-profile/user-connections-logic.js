var m = angular.module('profiles');

m.factory('userConnectionsLogic', function(userDao, widgetPolling, paginatedListLogicCore) {
    return function ($scope, logic) {
        var id = widgetPolling.scope().id;
        return paginatedListLogicCore($scope, logic, {
            retrieve: function() {
                return userDao.connections($scope.model.columns, id);
            }
        });
    }
});