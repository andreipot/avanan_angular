var m = angular.module('profiles');

m.factory('siteInfoLogic', function($q, siteDao, mimeIcons, widgetPolling) {
    return function ($scope, logic) {
        var id = widgetPolling.scope().id;
        $q.all([
            siteDao.retrieve(id),
            mimeIcons()
        ]).then(function(responses) {
            var siteInfo = responses[0];
            var icons = responses[1];
            $scope.model.lines = [
                {
                    "img": icons('folder'),
                    "class": "site-image"
                }, {
                    "text": siteInfo.data[logic.nameColumn],
                    "class": "site-name"
                }
            ]
        });
    }
});