var m = angular.module('profiles');

m.factory('fileMetaInfoLogic', function(widgetPolling, fileDao, metaInfoCoreLogic) {
    return function($scope, logic) {
        var id = widgetPolling.scope().id;

        return metaInfoCoreLogic($scope, logic, {
            advanced: logic.advanced,
            whiteList: logic.whiteList,
            blackList: logic.blackList,
            retrieve: function() {
                return fileDao.retrieve(id);
            }
        });
    }
});