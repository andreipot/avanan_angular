var m = angular.module('profiles');

m.factory('opswatScanDetailsLogic', function(fileScanDetailsLogicCore, fileDao, widgetPolling) {
    return function($scope, logic) {
        var id = widgetPolling.scope().id;
        return fileScanDetailsLogicCore($scope, logic, {
            retrieve: function() {
                return fileDao.opswatScanDetails($scope.model.columns, id);
            }
        });
    };
});