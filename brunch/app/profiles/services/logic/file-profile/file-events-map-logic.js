var m = angular.module('profiles');

m.factory('fileEventsMapLogic', function(fileDao, widgetPolling, eventsMapLogicCore) {
    return function($scope, logic) {
        var fileId = widgetPolling.scope().id;

        return eventsMapLogicCore($scope, logic, {
            retrieve: function(after) {
                return fileDao.eventsMap($scope.model.fields, after, fileId);
            },
            timeColumnName: logic.timeColumnName
        });
    }
});