var m = angular.module('profiles');

m.factory('fileScanDetailsLogicCore', function(fileDao, simpleLogicCore, modulesManager, $q) {
    return function ($scope, logic, coreOptions) {
        var simpleFree = _.noop;
        $scope.originalSize = $scope.originalSize || $scope.model.size;
        $scope.originalType = $scope.originalType || $scope.model.type;
        $scope.originalClasses = $scope.originalClasses || $scope.model.wrapper.class;
        var isActiveWatch = $scope.$watch(function() {
            return modulesManager.isActive(logic.module);
        }, function(isActive) {
            if(isActive) {
                simpleFree();
                simpleFree = simpleLogicCore($scope, logic, {
                    retrieve: function() {
                        var deferred = $q.defer();
                        coreOptions.retrieve().then(function(response) {
                            $scope.model.type = $scope.originalType;
                            deferred.resolve(response);
                        }, function() {
                            $scope.model.type = 'info';
                            $scope.model.lines = [{
                                text: 'File was not scanned'
                            }];
                            deferred.reject();
                        });
                        return deferred.promise;
                    }
                });
                $scope.model.size = $scope.originalSize;
            } else {
                $scope.model.size = 'display-none';
            }
        });

        return function() {
            simpleFree();
            isActiveWatch();
        };
    };
});