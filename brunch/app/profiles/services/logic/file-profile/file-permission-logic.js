var m = angular.module('profiles');

m.factory('filePermissionLogic', function(widgetPolling, fileDao, simpleLogicCore) {
    return function($scope, logic) {
        var id = widgetPolling.scope().id;
        return simpleLogicCore($scope, logic, {
            retrieve: function() {
                return fileDao.permissions($scope.model.columns, id);
            }
        });
    }
});