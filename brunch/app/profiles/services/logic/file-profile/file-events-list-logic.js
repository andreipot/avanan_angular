var m = angular.module('profiles');

m.factory('fileEventsListLogic', function(widgetPolling, fileDao, eventsListLogicCore, eventsDao) {
    return function($scope, logic) {
        var fileId = widgetPolling.scope().id;

        return eventsListLogicCore($scope, logic, {
            retrieveTypes: function() {
                return eventsDao.retrieveTypes({
                    drive: true
                });
            },
            retrieve: function(after) {
                return fileDao.events($scope.model.columns, after, fileId);
            },
            timeColumnName: logic.timeColumnName
        });
    }
});