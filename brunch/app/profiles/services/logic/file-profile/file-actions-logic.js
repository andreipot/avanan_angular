var m = angular.module('profiles');

m.factory('fileActionsLogic', function($routeParams, widgetPolling, objectTypeDao, modals, manualActionDao, remediationProperties) {
    return function($scope, logic) {
        var fileId = widgetPolling.scope().id;

        objectTypeDao.retrieve().then(function(response) {
            var actionTypes = _(response.data.actionTypes[logic.entityType]).chain().map(function(action) {
                return [action.id, action];
            }).object().value();
            _($scope.model.actions).each(function(action) {
                action.onClick = function() {
                    var actionType = actionTypes[action.actionType];
                    modals.params([{
                        params: _(actionType.attributes).map(function(param) {
                            return $.extend(true, {}, param);
                        }),
                        lastPage: true,
                        getProperties: _.memoize(remediationProperties(response.data.sources[logic.entityType].properties))
                    }], actionType.label, actionType.size).then(function(data) {
                        var body = {
                            name: action.actionType,
                            attributes: data
                        };
                        manualActionDao.create(logic.entityType, fileId, body);
                    });
                };
            });
        });
    };
});