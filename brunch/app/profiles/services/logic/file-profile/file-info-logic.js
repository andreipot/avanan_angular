var m = angular.module('profiles');

m.factory('fileInfoLogic', function(widgetPolling, fileDao, mimeIcons, $filter, $q) {
    return function($scope, logic) {
        var id = widgetPolling.scope().id;
        $q.all([
            fileDao.retrieve(id),
            mimeIcons()
        ]).then(function(responses) {
            var fileInfo = responses[0];
            var icons = responses[1];

            var parentIds = _([fileInfo.data[logic.parentIdsColumn]]).flatten();
            var parentTitles = _([fileInfo.data[logic.parentTitlesColumn]]).flatten();

            $scope.model.lines = [
                {
                    "img": icons(fileInfo.data[logic.mimeTypeColumn]),
                    "class": "profile-image"
                }, {
                    "text": fileInfo.data[logic.titleColumn],
                    "class": "file-name"
                }, {
                    "text": $filter('bytes')(fileInfo.data[logic.sizeColumn]),
                    "class": "file-size"
                }, {
                    "text": parentIds.length == 0 ? '/' : '#' + JSON.stringify({
                        display: 'inline-list',
                        data: _(parentIds).chain().map(function(id, idx) {
                            var title = parentTitles[idx];
                            if(_(title).isString() && title.length) {
                                return '#' + JSON.stringify({
                                        display: 'link',
                                        text: title,
                                        type: 'folder',
                                        id: id
                                    });
                            }
                            return null;
                        }).filter(function(link) {
                            return !_(link).isNull();
                        }).value(),
                        countText: 'Parents'
                    }),
                    'class': 'file-parents'
                }
            ];

            if(_(fileInfo.data[logic.linkColumn]).isString()) {
                $scope.model.lines.push({
                    "text": '#' + JSON.stringify({
                        display: 'external-link',
                        text: 'View file',
                        href: fileInfo.data[logic.linkColumn]
                    }),
                    "class": "file-link"
                })
            }
        });
    }
});