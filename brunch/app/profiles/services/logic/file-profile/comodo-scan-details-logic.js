var m = angular.module('profiles');

m.factory('comodoScanDetailsLogic', function(fileScanDetailsLogicCore, fileDao, widgetPolling) {
    return function($scope, logic) {
        var id = widgetPolling.scope().id;
        return fileScanDetailsLogicCore($scope, logic, {
            retrieve: function() {
                return fileDao.comodoScanDetails($scope.model.columns, id);
            }
        });
    };
});