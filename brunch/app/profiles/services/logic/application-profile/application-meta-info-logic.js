var m = angular.module('profiles');

m.factory('applicationMetaInfoLogic', function(metaInfoCoreLogic, widgetPolling, applicationDao) {
    return function($scope, logic) {
        var id = widgetPolling.scope().id;

        return metaInfoCoreLogic($scope, logic, {
            advanced: true,
            whiteList: ['name', 'installed'],
            retrieve: function() {
                return applicationDao.retrieve(id);
            }
        });
    }
});