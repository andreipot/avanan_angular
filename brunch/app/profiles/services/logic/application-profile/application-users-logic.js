var m = angular.module('profiles');

m.factory('applicationUsersLogic', function(applicationDao, widgetPolling, simpleLogicCore) {
    return function ($scope, logic) {
        var id = widgetPolling.scope().id;

        return simpleLogicCore($scope, logic, {
            retrieve: function() {
                return applicationDao.userList($scope.model.columns, id);
            }
        });
    }
});