var m = angular.module('profiles');

m.factory('applicationInfoLogic', function(applicationDao, widgetPolling, path) {
    return function ($scope, logic) {
        var id = widgetPolling.scope().id;
        applicationDao.retrieve(id).then(function(response) {
            var imageLink = path('profiles').img('application.png');
            if(_(response.data[logic.imageColumn]).isString()) {
                imageLink = response.data[logic.imageColumn];
            }
            $scope.model.lines = [
                {
                    "img": imageLink,
                    "defaultImg": path('profiles').img('application.png'),
                    "class": "application-image"
                }, {
                    "text": response.data[logic.nameColumn],
                    "class": "application-name"
                }
            ]
        });
    }
});