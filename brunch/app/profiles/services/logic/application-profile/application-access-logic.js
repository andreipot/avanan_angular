var m = angular.module('profiles');

m.factory('applicationAccessLogic', function(applicationDao, widgetPolling, simpleLogicCore) {
    return function ($scope, logic) {
        var id = widgetPolling.scope().id;

        return simpleLogicCore($scope, logic, {
            retrieve: function() {
                return applicationDao.access($scope.model.columns, id);
            }
        });
    }
});