var m = angular.module('widgets');

m.factory('applicationPolicyFindingsLogic', function(applicationDao, eventsListLogicCore, widgetPolling) {
    return function ($scope, logic) {
        var appId = widgetPolling.scope().id;
        return eventsListLogicCore($scope, logic, {
            retrieve: function(after) {
                return applicationDao.policyFindings($scope.model.columns, after, appId);
            },
            timeColumnName: 'insert_date',
            filter: false
        });
    }
});