var m = angular.module('profiles');

m.factory('ipInfoLogic', function(widgetPolling, ipDao) {
    return function ($scope, logic) {
        var id = widgetPolling.scope().id;
        $scope.model.lines = [
            {
                "text": id,
                "class": "ip-address"
            }
        ];

        ipDao.retrieve(id).then(function(response) {
            var data = response.data;
            if(!_(data).isEqual({})) {
                if(_(data.geo_country).isString()) {
                    $scope.model.lines.push({
                        "text": data.geo_country,
                        "class": "ip-country"
                    });
                }
                if(_(data.geo_city).isString()) {
                    $scope.model.lines.push({
                        "text": data.geo_city,
                        "class": "ip-city"
                    });
                }
            }
        });
    }
});