var m = angular.module('profiles');

m.factory('ipUserListLogic', function(ipDao, widgetPolling, paginatedListLogicCore) {
    return function ($scope, logic) {
        var id = widgetPolling.scope().id;
        return paginatedListLogicCore($scope, logic, {
            retrieve: function() {
                return ipDao.usersWithIp($scope.model.columns, id);
            }
        });
    }
});