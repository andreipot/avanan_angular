var m = angular.module('profiles');

m.factory('groupInfoLogic', function(groupDao, widgetPolling, path) {
    return function ($scope, logic) {
        var id = widgetPolling.scope().id;
        groupDao.retrieve(id).then(function(response) {
            $scope.model.lines = [
                {
                    "img": path('profiles').img('group.png'),
                    "class": "group-image"
                }, {
                    "text": response.data[logic.nameColumn],
                    "class": "group-name"
                }, {
                    "text": response.data[logic.emailColumn],
                    "class": "group-email"
                }
            ]
        });
    }
});