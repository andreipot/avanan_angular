var m = angular.module('profiles');

m.factory('groupMetaInfoLogic', function(metaInfoCoreLogic, widgetPolling, groupDao) {
    return function($scope, logic) {
        var id = widgetPolling.scope().id;

        return metaInfoCoreLogic($scope, logic, {
            retrieve: function() {
                return groupDao.retrieve(id);
            }
        });
    }
});