var m = angular.module('profiles');

m.factory('userFileShareConverter', function($q) {
    return function (input, args) {
        var deferred = $q.defer();

        function fillGaps(dateCountMap) {
            var result = [];
            var current = moment().hours(12).minutes(0).seconds(0).milliseconds(0).add(-args.after + 1, 'days');
            for(var i = 0; i < args.after; i++) {
                var date = current.format('YYYY-MM-DD');
                result.push([parseInt(current.format('x')), dateCountMap[date] || 0]);
                current.add(1, 'days');
            }
            return result;
        }

        var rows = input.data.rows || [];

        var dateCountMap = {};
        _(rows).each(function(row) {
            dateCountMap[row.time] = row.count;
        });

        var result = {};

        result.data = [{
            label: 'User sharing activity',
            data: fillGaps(dateCountMap)
        }];

        deferred.resolve($.extend({}, input, {
            data: result
        }));

        return deferred.promise;
    }
});