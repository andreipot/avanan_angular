var m = angular.module('profiles');

m.controller('IpProfileController', function($scope, $routeParams, widgetPolling, sizeClasses, path) {
    widgetPolling.scope({
        id: $routeParams.id
    });
    widgetPolling.updateWidgets(path('profiles').ctrl('ip-profile').json('ip-profile').path);
    $scope.widgetPolling = widgetPolling;
    $scope.getSizeClasses = sizeClasses;
});