var m = angular.module('profiles');

m.controller('GroupProfileController', function($scope, $routeParams, widgetPolling, sizeClasses, path) {
    var module = $routeParams.module;
    widgetPolling.scope({
        id: $routeParams.id
    });
    widgetPolling.updateWidgets(path('profiles').ctrl('group-profile').json(module + '/group-profile').path);
    $scope.widgetPolling = widgetPolling;
    $scope.getSizeClasses = sizeClasses;
});