var m = angular.module('profiles');

m.controller('SiteProfileController', function($scope, $routeParams, widgetPolling, sizeClasses, path) {
    var module = $routeParams.module;
    widgetPolling.scope({
        id: $routeParams.id
    });
    widgetPolling.updateWidgets(path('profiles').ctrl('site-profile').json(module + '/site-profile').path);
    $scope.widgetPolling = widgetPolling;
    $scope.getSizeClasses = sizeClasses;
});