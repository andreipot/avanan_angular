var m = angular.module('profiles');

m.controller('UserProfileController', function($scope, $routeParams, widgetPolling, sizeClasses, path, userDao) {
    var module = $routeParams.module;
    var userId = $routeParams.id;
    widgetPolling.scope({
        id: userId
    });

    $scope.widgetPolling = widgetPolling;
    $scope.getSizeClasses = sizeClasses;

    var root = path('profiles').ctrl('user-profile');
    if(module == 'google_drive') {
        userDao.retrieve(userId).then(function (response) {
            if (!response.data.is_external) {
                widgetPolling.updateWidgets(root.json(module + '/user-profile').path);
            } else {
                widgetPolling.updateWidgets(root.json(module + '/external-user-profile').path);
            }
        });
    } else {
        widgetPolling.updateWidgets(root.json(module + '/user-profile').path);
    }
});