var m = angular.module('profiles');

m.controller('ApplicationProfileController', function($scope, $routeParams, widgetPolling, sizeClasses, path) {
    var module = $routeParams.module;
    widgetPolling.scope({
        id: $routeParams.id
    });
    widgetPolling.updateWidgets(path('profiles').ctrl('application-profile').json(module + '/application-profile').path);
    $scope.widgetPolling = widgetPolling;
    $scope.getSizeClasses = sizeClasses;
});