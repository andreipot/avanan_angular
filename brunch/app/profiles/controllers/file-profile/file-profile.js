var m = angular.module('profiles');

m.controller('FileProfileController', function($scope, $routeParams, widgetPolling, sizeClasses, path) {
    var module = $routeParams.module;
    widgetPolling.scope({
        id: $routeParams.id
    });
    widgetPolling.updateWidgets(
        path('profiles').ctrl('file-profile').json(module + '/file-profile').path,
        path('profiles').ctrl('file-profile').json('common-widgets').path
    );
    $scope.widgetPolling = widgetPolling;
    $scope.getSizeClasses = sizeClasses;
});