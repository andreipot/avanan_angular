var m = angular.module('widgets');

m.factory('tablePaginationHelper', function($timeout, $filter, linkedText, columnsHelper, $q) {
    return function($scope, tableOptions, options, args, setTableModel) {
        if(_(args).isFunction()) {
            args = {
                retrieve: args
            };
        }
        options = $.extend(true, {
            options: {
                highLightNew: true
            }
        }, options);
        var destroyed = false;
        tableOptions.pageSize = tableOptions.pageSize || Math.pow(2, 53) - 1;
        var inMemory = false;
        var ignoreNewColumnsColoring = true;
        var inMemoryFilter = '';
        var results;

        var memory = {
            totalRows: undefined,
            data: undefined,
            rowClasses: [],
            selected: [],
            ordering: {
                column: undefined,
                order: undefined
            },
            prevTableModel: {
                data: undefined,
                pagination: undefined
            }
        };

        function resolvePromises(data) {
            var promises = [];
            var idxToColumn = {};
            _(data).each(function(row) {
                _(row).chain().filter(function(column) {
                    return _(column.text).isObject();
                }).each(function(column) {
                    promises.push($q.when(column.text));
                    idxToColumn[promises.length - 1] = column;
                });
            });

            return $q.all(promises).then(function(results) {
                _(results).each(function(result, idx) {
                    idxToColumn[idx].text = result;
                });

                return $q.when();
            });
        }

        function setTableModelCached(tableModel) {
            tableModel.selected = memory.selected;

            var rowClasses = [];
            if(_(tableOptions.filter).isFunction()) {
                tableModel.data = _(tableModel.data).filter(function(row, idx) {
                    rowClasses[idx] = {};
                    var result = tableOptions.filter(row, tableModel.columns);
                    if(_(result).isBoolean()) {
                        return result;
                    }
                    rowClasses[idx][result] = true;
                    return true;
                });
            }
            $.extend(tableOptions.rowClasses, rowClasses);

            resolvePromises(tableModel.data).then(function() {
                function expandEnchant(row) {
                    if(!_(row.expanded).isUndefined()) {
                        return;
                    }
                    row.expanded = false;
                    row.isExpandable = function() {
                        return args.isExpandable(row);
                    };

                    row.toggleExpand = function() {
                        if(row.expanded == false) {
                            row.expanded = null;
                            args.expand(row).then(function(response) {
                                row.expanded = true;
                                row.subrows = _(response.data.data).map(function(subrow) {
                                    subrow.loaded = true;
                                    return subrow;
                                });
                            });
                        } else if(row.expanded == true) {
                            row.expanded = false;
                            delete row.subrows;
                        }
                    };
                }

                function isDataEqual(newData, oldData, checkSubRows) {
                    if(!_(angular.copy(oldData)).isEqual(newData)) {
                        return false;
                    }

                    return !checkSubRows || !_(newData).some(function(row, idx) {
                        return !_(row.subrows).isEqual(angular.copy(oldData[idx].subrows));
                    });
                }

                function finishProcessing() {
                    if(options.options.expandable) {
                        _(tableModel.data).each(function(row) {
                            expandEnchant(row);
                        });
                    }
                    tableModel.pagination = $.extend(true, {}, memory.prevTableModel.pagination, tableModel.pagination);
                    if(_(tableModel.pagination).isEqual(memory.prevTableModel.pagination)) {
                        tableModel.pagination = memory.prevTableModel.pagination;
                    }
                    setTableModel(tableModel);
                    memory.prevTableModel = tableModel;
                }

                if(!isDataEqual(tableModel.data, memory.prevTableModel.data, false)) {
                    finishProcessing();
                } else {
                    $q.all(_(memory.prevTableModel.data).map(function (row) {
                        if (!_(row.subrows).isUndefined()) {
                            return args.expand(row);
                        }
                        return $q.when(null);
                    })).then(function (responses) {
                        _(responses).each(function (response, idx) {
                            var row = tableModel.data[idx];
                            expandEnchant(row);
                            if (_(response).isNull()) {
                                return;
                            }
                            if(_(memory.prevTableModel.data[idx].subrows).isUndefined()) {
                                return;
                            }
                            row.expanded = true;
                            row.subrows = _(response.data.data).map(function (subrow) {
                                subrow.loaded = true;
                                return subrow;
                            });
                        });

                        if (isDataEqual(tableModel.data, memory.prevTableModel.data, true)) {
                            tableModel.data = memory.prevTableModel.data;
                        }

                        finishProcessing();
                    });
                }
            });
        }

        function sortingNumber(text) {
            var prefixLength = 32;
            var postfixLength = 8;
            var result = '' + text;
            if (result.indexOf('.') == -1) {
                result += '.';
            }
            while(result.indexOf('.') < prefixLength) {
                result = '0' + result;
            }
            while (result.length < prefixLength + 1 + postfixLength) {
                result += '0';
            }
            return result;
        }

        function updateBackendOrdering(ordering) {
            if(!_(options.columns).isUndefined() && !_(ordering.column).isUndefined()) {
                var orderingColumn = options.columns[ordering.column];
                if (!_(orderingColumn).isUndefined()) {
                    var sortable = orderingColumn.sortable || {};
                    if (sortable.id) {
                        ordering.column = columnsHelper.getIdxById(options.columns, sortable.id);
                        ordering.columnName = sortable.id;
                    }
                }
            }
        }

        function setFromMemory(page, ordering, filter) {
            var primaryColumn = columnsHelper.getPrimaryIdx(options.columns);
            if(_(memory.data).isUndefined() || !_(memory.ordering).isEqual(ordering)) {
                memory.data = results.data;

                if(!_(filter).isUndefined() && filter.length) {
                    memory.data = $filter('filter')(memory.data , filter, function(val, expected) {
                        expected = expected.toLowerCase();
                        if(_(val).isObject() && !_(val.text).isUndefined()) {
                            var text = '' + val.text;
                            return ~text.toLowerCase().indexOf(expected);
                        }
                        return false;
                    });
                }

                if(!_(ordering).isUndefined()) {
                    var orderingColumn = ordering.column;
                    if(!_(orderingColumn).isUndefined()) {
                        var sortable = options.columns[orderingColumn].sortable;
                        if (!_(sortable).isObject()) {
                            sortable = {}
                        }
                        if (sortable.id) {
                            orderingColumn = columnsHelper.getIdxById(options.columns, sortable.id);
                        }
                        if (!_(orderingColumn).isUndefined()) {
                            memory.data = $filter('orderBy')(memory.data, function (val) {
                                var orderPrefix = val[orderingColumn].text;
                                if (sortable.original) {
                                    orderPrefix = val[orderingColumn].originalText;
                                }
                                if (sortable.number) {
                                    orderPrefix = sortingNumber(orderPrefix);
                                }
                                if(_(orderPrefix).isNull() || _(orderPrefix).isUndefined()) {
                                    orderPrefix = '~';
                                }
                                if(_(orderPrefix).isArray()) {
                                    orderPrefix = orderPrefix.join('/');
                                }
                                orderPrefix = orderPrefix + '-';

                                return orderPrefix + sortingNumber(val[primaryColumn].text);
                            }, ordering.order == 'desc');
                        }
                    }
                }

                var rowClasses = memory.rowClasses = [];

                if(_(tableOptions.filter).isFunction()) {
                    memory.data = _(memory.data).filter(function(row, idx) {
                        rowClasses[idx] = {};
                        var result = tableOptions.filter(row, options.columns);
                        if(_(result).isBoolean()) {
                            return result;
                        }
                        rowClasses[idx][result] = true;
                        return true;
                    });
                }

                if(ignoreNewColumnsColoring || !options.options.highLightNew) {
                    ignoreNewColumnsColoring = false;
                } else {
                    var newRows = [];
                    _(memory.data).each(function (row, idx) {
                        var id = row[primaryColumn].text;
                        if (_(_(memory.prevTableModel.data).find(function (d) {
                                return d[primaryColumn].text == id;
                            })).isUndefined()) {
                            rowClasses[idx] = rowClasses[idx] || {};
                            rowClasses[idx]['new'] = true;
                            newRows.push(idx);
                        }
                    });
                    if (newRows.length) {
                        $timeout(function () {
                            _(newRows).each(function (row) {
                                rowClasses[row]['new'] = false;
                            });
                        }, 500);
                    }
                }
            }
            memory.totalRows = memory.data.length;
            var total = Math.ceil(memory.data.length / tableOptions.pageSize);
            page = Math.min(page, total);
            var tableModel = $.extend({}, results, options);
            tableModel.data = memory.data.slice((page - 1) * tableOptions.pageSize, page * tableOptions.pageSize);
            tableModel.rowClasses = memory.rowClasses.slice((page - 1) * tableOptions.pageSize, page * tableOptions.pageSize);
            tableModel.pagination = {
                current: page,
                total: total
            };
            setTableModelCached(tableModel);
        }

        var retrieveDelay = 500;
        var retrieveTimeout = null;
        var autoReloadTimeout = null;
        var requestTimeout = $q.defer();

        function reload(pagination, force) {
            var page = pagination.page;
            var ordering = $.extend(true, {}, pagination.ordering);
            var filter = pagination.filter;

            function performRetrieve() {
                updateBackendOrdering(ordering);

                if(autoReloadTimeout !== null) {
                    $timeout.cancel(autoReloadTimeout);
                    autoReloadTimeout = null;
                }
                requestTimeout.resolve();
                requestTimeout = $q.defer();
                args.retrieve({
                    offset: (page - 1) * tableOptions.pageSize,
                    limit: tableOptions.pageSize,
                    filter: filter,
                    ordering: _(ordering).isEqual({}) ? (options.options.defaultOrdering || {}) : ordering
                }).timeout(requestTimeout.promise).then(function (response) {
                    if(destroyed) {
                        return;
                    }
                    if(!_(response.data.columns).isUndefined()) {
                        options.columns = response.data.columns;
                    }
                    var page = pagination.page;
                    var ordering = pagination.ordering;
                    var filter = pagination.filter;

                    retrieveTimeout = $timeout(function() {
                        retrieveTimeout = null;
                    }, retrieveDelay);
                    results = response.data;
                    if (results.pagination.total == results.data.length && !options.options.forceServerProcessing) {
                        inMemory = true;
                        inMemoryFilter = filter;
                        setFromMemory(page, ordering, filter);
                    } else {
                        inMemory = false;
                        var tableModel = $.extend(true, {}, results, options, {
                            pagination: {
                                total: Math.ceil(results.pagination.total / tableOptions.pageSize)
                            }
                        });
                        memory.totalRows = results.pagination.total;
                        setTableModelCached(tableModel);
                    }

                    if(!_(options.options.autoUpdate).isUndefined()) {
                        autoReloadTimeout = $timeout(function () {
                            reload(tableOptions.pagination, true);
                        }, options.options.autoUpdate);
                    }
                }, function(error) {
                    if(error.status != 0) { //Zero status if we canceled request on our side
                        autoReloadTimeout = $timeout(function () {
                            reload(tableOptions.pagination, true);
                        }, 5000);
                    }
                });
            }

            if(inMemory && !_(filter).isUndefined() && ~filter.indexOf(inMemoryFilter) && !force && !options.options.forceServerProcessing) {
                setFromMemory(page, ordering, filter);
            } else {
                if(retrieveTimeout !== null) {
                    $timeout.cancel(retrieveTimeout);
                    retrieveTimeout = $timeout(performRetrieve, retrieveDelay);
                } else {
                    $timeout(performRetrieve);
                }
            }
        }

        var reloadWatch = $scope.$watch(function() {
            return tableOptions.reload;
        }, function() {
            if(tableOptions.reload) {
                var forceReload = tableOptions.forceReload;
                tableOptions.reload = false;
                tableOptions.forceReload = false;
                reload(tableOptions.pagination, forceReload);
            }
        });

        var pageWatch = $scope.$watch(function() {
            return tableOptions.pagination.page;
        }, function() {
            tableOptions.reload = true;
        });

        var paginationWatch = $scope.$watch(function() {
            return tableOptions.pagination.ordering;
        }, function() {
            tableOptions.reload = true;
        }, true);

        var filterReloadTimeout = null;
        var filterWatch = $scope.$watch(function() {
            return tableOptions.pagination.filter;
        }, function(newValue, oldValue) {
            if(newValue == oldValue) {
                return;
            }
            if(filterReloadTimeout != null) {
                $timeout.cancel(filterReloadTimeout);
            }
            filterReloadTimeout = $timeout(function() {
                filterReloadTimeout = null;
                tableOptions.pagination.page = 1;
                tableOptions.reload = true;
            }, 700);
        });

        var toggleWatch = $scope.$watch(function() {
            return tableOptions.toggleState;
        }, function(newState, oldState) {
            if(newState !== oldState) {
                tableOptions.pagination.page = 1;
                tableOptions.reload = true;
                tableOptions.forceReload = true;
            }
        });

        $scope.$on('$destroy', function() {
            api.destroy();
        });

        var api = {
            getTotal: function() {
                return memory.totalRows;
            },
            getSelected: function() {
                return memory.selected;
            },
            getSelectedIds: function() {
                var primaryIdx = columnsHelper.getPrimaryIdx(options.columns);
                return _(memory.selected).map(function(line) {
                    return line[primaryIdx].text;
                });
            },
            getLabel: function(id, onlySelected) {
                var data = memory.data;
                if(onlySelected) {
                    data = memory.selected;
                }
                var primaryIdx = columnsHelper.getPrimaryIdx(options.columns);
                var labelIdx = columnsHelper.getLabelIdx(options.columns) || primaryIdx;
                return _(data).find(function(line) {
                    return line[primaryIdx].text == id;
                })[labelIdx].originalText;
            },
            clearSelection: function() {
                memory.selected.length = 0;
            },
            reload: function(force) {
                if(_(force).isUndefined()) {
                    force = true;
                }
                tableOptions.reload = true;
                tableOptions.forceReload = force;
            },
            destroy: function() {
                reloadWatch();
                toggleWatch();
                pageWatch();
                paginationWatch();
                filterWatch();
                setTableModel(null);
                if(retrieveTimeout != null) {
                    $timeout.cancel(retrieveTimeout);
                    retrieveTimeout = null;
                }
                if(autoReloadTimeout != null) {
                    $timeout.cancel(autoReloadTimeout);
                    autoReloadTimeout = null;
                }
                requestTimeout.reject();
                destroyed = true;
            },
            makePdf: function(title) {
                var pagination = $.extend(true, {}, tableOptions.pagination);
                var ordering = pagination.ordering;
                updateBackendOrdering(ordering);
                var filter = pagination.filter;

                return args.retrieve({
                    filter: filter,
                    ordering: ordering
                }).then(function(response) {
                    var table = {
                        headerRows: 1,
                        widths: [],
                        body: []
                    };

                    var pdf = {
                        pageOrientation: 'landscape',
                        content: [{
                            text: title,
                            fontSize: 22,
                            bold: true
                        }, {
                            table: table
                        }]
                    };

                    var columns = options.columns;
                    if(!_(response.data.columns).isUndefined()) {
                        columns = response.data.columns;
                    }

                    var header = [];
                    table.body.push(header);
                    _(response.data.data).each(function(row) {
                        table.body.push([]);
                    });

                    _(columns).each(function(column, idx) {
                        var hidden = column.hidden;
                        if(!_(column.pdf).isUndefined() && !_(column.pdf.hidden).isUndefined()) {
                            hidden = column.pdf.hidden;
                        }
                        if(hidden) {
                            return
                        }
                        table.widths.push('auto');
                        header.push({
                            text: column.text || '',
                            bold: true,
                            fillColor: '#f0f0f0'
                        });

                        _(response.data.data).each(function(row, rowIdx) {
                            table.body[rowIdx + 1].push({
                                text: linkedText.toText(row[idx].text) || '',
                                fillColor: rowIdx % 2 ? '#f6f6f6' : '#ffffff'
                            });
                        });
                    });

                    return $q.when(pdfMake.createPdf(pdf));
                });
            }
        };
        return api;
    }
});