var m = angular.module('profiles');

m.factory('commonWidgetsConverter', function($q) {
    return function (input, args) {
        var deferred = $q.defer();

        var rows = input.data.rows = input.data.rows || [input.data.widgets];

        if(!_(args.commonWidgets).isUndefined()) {
            var commonWidgets = args.commonWidgets.data;
            _(rows).each(function (row) {
                _(row).each(function (widget, idx) {
                    if (widget.type == 'reference') {
                        row[idx] = commonWidgets[widget.reference];
                    }
                });
            });
        }

        deferred.resolve($.extend({}, input, {
            data: {
                rows: rows
            }
        }));

        return deferred.promise;
    }
});