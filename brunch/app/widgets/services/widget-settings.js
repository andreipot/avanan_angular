var m = angular.module('widgets');

m.factory('widgetSettings', function($cookies) {
    var settings = ($cookies.widgetSettings && JSON.parse($cookies.widgetSettings)) || {};
    return {
        params: function(widgetUuid, newParams) {
            if(!_(newParams).isUndefined()) {
                settings[widgetUuid] = newParams;
                $cookies.widgetSettings = JSON.stringify(settings);
            }
            return settings[widgetUuid] || {};
        },
        param: function(widgetUuid, key, value) {
            if(!_(value).isUndefined()) {
                settings[widgetUuid] = settings[widgetUuid] || {};
                settings[widgetUuid][key] = value;
                $cookies.widgetSettings = JSON.stringify(settings);
            }
            return settings[widgetUuid] && settings[widgetUuid][key];
        }
    }
});