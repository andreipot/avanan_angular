var m = angular.module('widgets');

m.factory('columnsHelper', function() {
    return {
        getPrimaryIdx: function(columns) {
            var primaryIdx = 0;
            _(columns).each(function(column, idx) {
                if(column.primary) {
                    primaryIdx = idx;
                }
            });
            return primaryIdx;
        },
        getLabelIdx: function(columns) {
            var labelIdx = 0;
            _(columns).each(function(column, idx) {
                if(column.entityLabel) {
                    labelIdx = idx;
                }
            });
            return labelIdx;
        },
        getIdxById: function(columns, id) {
            var columnIdx = 0;
            _(columns).each(function(column, idx) {
                if(column.id == id) {
                    columnIdx = idx;
                }
            });
            return columnIdx;
        }
    }
});