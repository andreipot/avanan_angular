 var m = angular.module('widgets');

m.factory('widgetPolling', function (http, $timeout, routeHelper, widgetSettings, defaultHttpErrorHandler, commonWidgetsConverter) {
    var scope = {};
    var data = {
        rows: [],
        pagination: {}
    };
    var timeouts = {};
    var autoUpdate = false;

    function updater(id, type) {
        var updaterData = data.rows;
        updaterData[type][id].forceUpdate = forceUpdate(type, id);
        var updateTick = updaterData[type][id].updateTick || 0;
        var updateUrl = updaterData[type][id].updateUrl;
        if (_(updateUrl).isUndefined() || updateTick === 'none') {
            return;
        }

        function update() {
            var params = widgetSettings.params(updaterData[type][id].uuid);
            http.get(updateUrl).then(function (result) {
                if (updaterData !== data.rows) {
                    return;
                }
                var resultData = $.extend(true, {}, updaterData[type][id], result.data);
                if (_(resultData.updateData).isFunction()) {
                    resultData.updateData();
                }
                resultData.forceUpdate = forceUpdate(type, id);
                updaterData[type][id] = resultData;
                updateTick = updaterData[type][id].updateTick;
                updateUrl = updaterData[type][id].updateUrl;
                if (autoUpdate && _(updateUrl).isString() && updateTick !== 'none') {
                    timeouts[id + '-' + type] = $timeout(function () {
                        update();
                    }, updateTick);
                }
            }, function () {
                if (autoUpdate) {
                    timeouts[id + '-' + type] = $timeout(function () {
                        update();
                    }, updateTick);
                }
            });
        }

        function forceUpdate(type, id) {
            return function () {
                $timeout.cancel(timeouts[id + '-' + type]);
                timeouts[id + '-' + type] = $timeout(function () {
                    update();
                });
            }
        }

        if (autoUpdate) {
            timeouts[id + '-' + type] = $timeout(function () {
                update();
            }, updateTick);
        }
    }

    function createUpdaters() {
        for (var i = 0; i < data.rows.length; i++) {
            for (var j = 0; j < data.rows[i].length; j++) {
                updater(j, i);
            }
        }
    }

    var reloadFn = _.noop;

    return {
        scope: function (s) {
            if (!_(s).isUndefined()) {
                scope = s;
            }
            return scope;
        },
        setLocal: function (widgetsData) {
            this.stopUpdating();
            autoUpdate = true;
            $timeout(function () {
                data = widgetsData;
                data.rows = data.rows || [data.widgets];
                createUpdaters();
            });
        },
        updateWidgets: function (resource, commonWidgets) {
            autoUpdate = true;
            function update() {
                var req = http.get(resource);
                if (!_(commonWidgets).isUndefined()) {
                    req = req.preFetch(http.get(commonWidgets), 'commonWidgets');
                }
                req.converter(commonWidgetsConverter, function (preFetches) {
                    return {
                        commonWidgets: preFetches.commonWidgets
                    };
                }).then(function (widgets) {
                    data = widgets.data;
                    createUpdaters();
                }, defaultHttpErrorHandler);
            }

            update();
        },
        stopUpdating: function () {
            scope = {};
            autoUpdate = false;
            _(_(timeouts).values()).each(function (timeout) {
                $timeout.cancel(timeout);
            });
            timeouts = {};
            data = {
                infoBoxes: [],
                panels: [],
                pagination: {}
            };
        },
        getRows: function () {
            return data.rows;
        },
        getPagination: function () {
            return data.pagination;
        },
        reload: function (newReloadFn) {
            if (_(newReloadFn).isFunction()) {
                reloadFn = newReloadFn;
            }
            reloadFn();
        }
    };
});