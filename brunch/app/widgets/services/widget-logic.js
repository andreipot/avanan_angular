var m = angular.module('widgets');

m.factory('widgetLogic', function($timeout, $injector) {
    var logics = {};

    function getLogic(logicName) {
        var logic = logics[logicName];
        if(_(logic).isUndefined()) {
            logic = $injector.get(logicName);
            if(_(logic).isUndefined()) {
                alert('Logic ' + logic.name + ' is not defined');
            }
            logics[logicName] = logic;
        }
        return  logic;
    }

    return function($scope, logic) {
        if(_(logic).isUndefined()) {
            return function() {};
        }
        var reinitLogicTimeout = null;

        var destroyLogic = _.noop;
        function reinitLogic() {
            destroyLogic();
            destroyLogic = getLogic(logic.name)($scope, logic, reinitLogic) || _.noop;
            var updateTick = logic.updateTick;
            if(_(updateTick).isUndefined()) {
                updateTick = 20000;
            }
            if(!_(updateTick).isNull()) {
                reinitLogicTimeout = $timeout(reinitLogic, updateTick);
            }
        }
        reinitLogic();
        $scope.$on('$destroy', function() {
            if(reinitLogicTimeout !== null) {
                $timeout.cancel(reinitLogicTimeout);
                reinitLogicTimeout = null;
            }
            destroyLogic();
        });
    }
});