var m = angular.module('widgets');

m.factory('eventsMapLogicCore', function () {
    return function ($scope, logic, coreOptions) {
        $scope.after = $scope.after || '2000-01-01';
        $scope.data = $scope.data || [];

        coreOptions.retrieve($scope.after)
            .pagination(0, 100)
            .order(coreOptions.timeColumnName, 'desc')
            .filter('')
            .then(function (response) {
                if (response.data.data.length > 0) {
                    $scope.after = response.data.data[0].metadata[coreOptions.timeColumnName].value;
                }

                _(response.data.data.reverse()).each(function (d) {
                    $scope.data.splice(0, 0, d);
                });

                $scope.model.data = $scope.data;
                if (_($scope.model.updateData).isFunction()) {
                    $scope.model.updateData();
                }
            });

        return _.noop;
    }
});