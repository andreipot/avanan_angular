var m = angular.module('widgets');

m.factory('simpleLogicCore', function() {
    return function ($scope, logic, coreOptions) {
        coreOptions.retrieve().then(function(response) {
            if(!_(response.data.data).isEqual(angular.copy($scope.model.data))) {
                $scope.model.data = response.data.data;
            }
        });

        return _.noop;
    }
});