var m = angular.module('widgets');

m.factory('paginatedListLogicCore', function(tablePaginationHelper) {
    return function ($scope, logic, coreOptions) {
        var options = {
            columns: $scope.model.columns,
            options: $.extend({
                autoUpdate: logic.updateTick
            }, $scope.model.options)
        };

        logic.updateTick = null;

        $scope.tableHelper = tablePaginationHelper($scope, $scope.model.options, options, function(args) {
            return coreOptions.retrieve();
        }, function(tableModel) {
            if(!_(tableModel).isNull()) {
                $scope.model.pagination = tableModel.pagination;
                $scope.model.data = tableModel.data;
                $scope.model.selected = tableModel.selected;
                $scope.model.rowClasses = tableModel.rowClasses;
            }
        });

        return function() {
            $scope.tableHelper.destroy();
        };
    }
});