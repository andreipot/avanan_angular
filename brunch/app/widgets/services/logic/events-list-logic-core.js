var m = angular.module('widgets');

m.factory('eventsListLogicCore', function(eventsDao, columnsHelper, $timeout, widgetSettings, tablePaginationHelper, $q, widgetPolling) {
    return function ($scope, logic, coreOptions) {
        var data = [];
        var clearData = false;
        var after = '2000-01-01';

        if(_(coreOptions.retrieveTypes).isFunction()) {
            coreOptions.retrieveTypes().then(function (response) {
                $scope.model.wrapper.filters = _(response.data).map(function (eventType) {
                    return {
                        type: 'flag',
                        local: true,
                        state: true,
                        text: eventType,
                        value: eventType
                    }
                });
            });
        }

        $scope.tableOptions = {
            pagesAround: 2,
            pageSize: 10,
            pagination: {
                page: 1,
                ordering: {},
                filter: ''
            }
        };

        var options = {
            columns: $scope.model.columns,
            options: {
                autoUpdate: logic.updateTick
            }
        };
        logic.updateTick = null;

        $scope.tableHelper = tablePaginationHelper($scope, $scope.model.options, options, function(args) {
            var params = _(widgetSettings.params($scope.model.uuid)).chain().map(function(param, value) {
                return param != true ? value : null;
            }).filter(function(param) {
                return param != null;
            }).value().join(',');
            return coreOptions
                .retrieve(after)
                .params({
                    event_types: params
                })
                .filter(getFilter())
                .order(coreOptions.timeColumnName, 'desc')
                .pagination(0, 100)
                .converter(function(response) {
                    if(clearData) {
                        clearData = false;
                        data = [];
                    }

                    if (response.data.data.length > 0) {
                        var timeColumnIdx = columnsHelper.getIdxById($scope.model.columns, coreOptions.timeColumnName);
                        after = response.data.data[0][timeColumnIdx].originalText;
                    }

                    _(response.data.data.reverse()).each(function(d) {
                        data.splice(0, 0, d);
                    });

                    if(data.length > 100) {
                        data = _(data).first(100);
                    }

                    return $q.when({
                        data: {
                            data: data,
                            pagination: {
                                total: data.length
                            }
                        }
                    });
                });
        }, function(tableModel) {
            if(!_(tableModel).isNull()) {
                $scope.model.pagination = tableModel.pagination;
                $scope.model.data = tableModel.data;
                $scope.model.selected = tableModel.selected;
                $scope.model.rowClasses = tableModel.rowClasses;
            }
        });

        function getFilter() {
            if(_($scope.model.wrapper.filter).isObject())
                return $scope.model.wrapper.filter.value || '';
            return '';
        }

        function reload(newValue, oldValue) {
            if (newValue !== oldValue) {
                after = '2000-01-01';
                clearData = true;
                $scope.tableHelper.reload();
            }
        }

        var filterWatch = $scope.$watch(getFilter, reload);
        var paramsWatch = $scope.$watch(function() {
            return widgetSettings.params($scope.model.uuid);
        }, reload, true);

        return function() {
            $scope.tableHelper.destroy();
            filterWatch();
            paramsWatch();
        };
    }
});