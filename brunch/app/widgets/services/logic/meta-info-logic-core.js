var m = angular.module('widgets');

m.factory('metaInfoCoreLogic', function(widgetSettings) {
    return function ($scope, logic, coreOptions) {
        function buildData() {
            var advanced = true;
            if(coreOptions.advanced) {
                advanced = widgetSettings.params($scope.model.uuid).advanced;
            }
            return _($scope.cachedData).reduce(function(memo, value, key) {
                if(!_(value).isNull() && (advanced || _(coreOptions.whiteList).contains(key)) && !_(coreOptions.blackList).contains(key)) {
                    memo.push({
                        key: key,
                        value: _(value).isObject() ? JSON.stringify(value) : value
                    });
                }
                return memo;
            }, []);
        }

        coreOptions.retrieve().then(function(response) {
            $scope.cachedData = response.data;
            $scope.model.data = buildData();
        });

        var paramsWatch = _.noop;
        if(coreOptions.advanced) {
            paramsWatch = $scope.$watch(function () {
                return widgetSettings.params($scope.model.uuid);
            }, function () {
                $scope.model.data = buildData();
            }, true);
        }

        return function() {
            paramsWatch();
        };
    }
});