var m = angular.module('widgets');

m.directive('table', function(path, paginationHelper, columnsHelper, widgetLogic, $timeout) {
    return {
        restrict: 'A',
        replace: true,
        templateUrl: path('widgets').directive('data-table').template(),
        scope: {
            model: '=table',
            options: '=?'
        },
        link: function($scope, element, attrs) {
            widgetLogic($scope, $scope.model.logic);

            var loadRowsTimeout = $timeout(loadRows);
            function loadRows() {
                _($scope.model.data).chain().filter(function(line) {
                    return !line.loaded;
                }).first(20).each(function(line) {
                    line.loaded = true;
                });
                loadRowsTimeout = $timeout(loadRows, 50);
            }

            $scope.$watch('model', function() {
                $scope.model.data = $scope.model.data || [];
                $scope.model.globalSelection = true;
                _($scope.model.data).each(function(line) {
                    if(!$scope.lineSelected(line)()) {
                        $scope.model.globalSelection = false;
                    }
                    line.loaded = true;
                });
                if($scope.model.data.length == 0) {
                    $scope.model.globalSelection = false;
                }
                $scope.model.updateData = updateData;
            });

            $scope.updateSelection = function() {
                _($scope.model.data).each(function(line) {
                    $scope.lineSelected(line)($scope.model.globalSelection);
                });
            };

            function updateData() {
                var self = this;
                if(_(self['dataUpdate']).isArray()) {
                    _(self['dataUpdate']).each(function(e) {
                        self.data.splice(0, 0, e);
                    });
                    this.options = this.options || {};
                    var maxData = this.options.maxData;
                    if(!_(maxData).isUndefined() && maxData < self.data.length) {
                        self.data.splice(maxData, self.data.length - maxData);
                    }
        }
        delete self['dataUpdate'];
}

$scope.$watch('model.pagination', function() {
                if(_($scope.model.pagination).isUndefined()) {
                    $scope.pages = null;
                } else {
                    $scope.pages = paginationHelper.generatePages({
                        total: $scope.model.pagination.total,
                        current: $scope.model.pagination.current,
                        haveNext: $scope.model.pagination.haveNext,
                        pagesAround: $scope.options.pagesAround
                    });
                }
            });

            $scope.paginationClicked = function(event, page) {
                if(page.enabled) {
                    $scope.options.pagination.page = page.value;
                }
                event.preventDefault();
            };

            $scope.changeOrdering = function(columnId) {
                if(!$scope.model.columns[columnId].sortable) {
                    return;
                }
                var ordering = $scope.options.pagination.ordering;
                if(ordering.column != columnId) {
                    ordering.column = columnId;
                    ordering.columnName = $scope.model.columns[columnId].id;
                    ordering.order = 'desc';
                } else {
                    if(ordering.order == 'desc') {
                        ordering.order = 'asc';
                    } else {
                        delete ordering.column;
                        delete ordering.columnName;
                        delete ordering.order;
                    }
                }
            };

            $scope.orderingClass = function(columnId) {
                var ordering = $scope.options.pagination.ordering || {};
                return {
                    'fa-sort-down': ordering.column == columnId && ordering.order == 'desc',
                    'fa-sort-up': ordering.column == columnId && ordering.order == 'asc',
                    'fa-sort': ordering.column != columnId
                }
            };

            var actionsCache = {};
            $scope.getActions = function(display) {
                if(!_(actionsCache[display]).isUndefined()) {
                    return actionsCache[display];
                }
                if(_($scope.options).isUndefined() || _($scope.options.actions).isUndefined()) {
                    return undefined;
                }
                var actions = _($scope.options.actions).filter(function(action) {
                    var actionDisplay = action.display || 'dropdown-button';
                    return actionDisplay == display;
                });
                if(actions.length == 0) {
                    return undefined;
                }
                actionsCache[display] = actions;
                return actions;
            };

            $scope.executeAction = function(event, action) {
                event.preventDefault();
                if(!$scope.actionActive(action)) {
                    return;
                }
                action.execute();
            };

            $scope.actionActive = function(action) {
                return !(_(action.active).isFunction() && !action.active());
            };

            $scope.getText = function(line, index) {
                var href = $scope.model.columns[index].href;
                if(_(href).isUndefined()) {
                    return line[index].text;
                }

                var result = {
                    text: line[index].text,
                    type: href.type,
                    qs: {}
                };

                _(href.params).each(function (param, key) {
                    var value = param;
                    _($scope.model.columns).each(function (column, idx) {
                        if (column.id === param) {
                            value = line[idx].text;
                        }
                    });
                    result[key] = value;
                });
                _(href.qs).each(function(param, key) {
                    var value = param;
                    _($scope.model.columns).each(function (column, idx) {
                        if (column.id === param) {
                            value = line[idx].text;
                        }
                    });
                    result.qs[key] = value;
                });
                return '#' + JSON.stringify(result);
            };

            $scope.getLineSelection = function(line) {
                var primaryIdx = columnsHelper.getPrimaryIdx($scope.model.columns);
                var selectionIdx = undefined;
                _($scope.model.selected).each(function(s, idx) {
                    if(s[primaryIdx].text == line[primaryIdx].text) {
                        selectionIdx = idx;
                    }
                });
                return selectionIdx;
            };

            $scope.lineSelected = function(line) {
                return function(val) {
                    if(_(val).isUndefined()) {
                        return $scope.isSelected(line);
                    } else {
                        var selectIdx = $scope.getLineSelection(line);
                        if(_(selectIdx).isUndefined() && val) {
                            if($scope.model.options.singleSelection) {
                                $scope.model.selected.length = 0;
                            }
                            $scope.model.selected.push(line);
                        } else if(!_(selectIdx).isUndefined() && !val){
                            $scope.model.selected.splice(selectIdx, 1);
                        }
                    }
                }
            };

            $scope.lineClick = function(line) {
                if($scope.model.options.lineSelection) {
                    $scope.lineSelected(line)(!$scope.isSelected(line));
                }
            };

            $scope.getLineClass = function(row, line) {
                var rowClasses = $scope.model.rowClasses || {};
                var result = rowClasses[row] || {};
                result['selected-line'] = $scope.isSelected(line);
                return result;
            };

            $scope.isSelected = function(line) {
                return !_($scope.getLineSelection(line)).isUndefined();
            };

            $scope.getColumnLabel = function(column) {
                if(column.text === false) {
                    return '';
                }
                return column.text || column.id;
            };

            $scope.$on('$destroy', function() {
                $timeout.cancel(loadRowsTimeout);
            });
        }
    }
});