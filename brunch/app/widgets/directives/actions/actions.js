var m = angular.module('widgets');

m.directive('actions', function(path, modals, widgetLogic) {
    return {
        restrict: 'A',
        replace: true,
        templateUrl: path('widgets').directive('actions').template(),
        scope: {
            model: '=actions'
        },
        link: function($scope, element, attr) {
            widgetLogic($scope, $scope.model.logic);
            $scope.processClick = function(action) {
                function process() {

                }

                if(!_(action.confirm).isUndefined()) {
                    modals.confirm(action.confirm.text).then(process);
                } else {
                    process();
                }
            };
        }
    }
});