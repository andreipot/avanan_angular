var m = angular.module('widgets');

m.directive('tableRow', function(path) {
    return {
        restrict: 'A',
        replace: true,
        templateUrl: path('widgets').directive('data-table-row').template(),
        scope: {
            model: '=tableRow',
            row: '=',
            line: '=',
            lineClick: '&',
            getText: '&',
            lineSelected: '&'
        },
        link: function($scope, element, attrs) {
            $scope.getExpandableClass = function(row) {
                var result = {};
                if(row.expanded == null) {
                    result['fa-spinner'] = true;
                } else if(row.expanded == false) {
                    result['fa-plus-square'] = true;
                } else {
                    result['fa-minus-square'] = true;
                }
                return result;
            };
        }
    };
});