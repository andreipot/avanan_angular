var m = angular.module('widgets');

m.directive('flot', function($timeout, widgetLogic) {
    return {
        restrict: 'A',
        scope: {
            model: '=flot'
        },
        link: function ($scope, element, attrs) {
            widgetLogic($scope, $scope.model.logic);
            $scope.plot = null;
            var updateTimeout = null;
            function updateFlot() {
                if(updateTimeout != null) {
                    return;
                }
                updateTimeout = $timeout(function() {
                    updateTimeout = null;
                    $scope.model.updateData = updateData;
                    element.empty();
                    if($scope.model.data && $scope.model.data.length) {
                        var options = $.extend({}, $scope.model.flotOptions, {
                            interaction: {
                                redrawOverlayInterval: 10
                            }
                        });
                        if(options.hideBeforeCreate) {
                            element.hide();
                        }
                        $scope.plot = $.plot(element, $scope.model.data, options);
                        element.show();
                    }
                });
            }
            $scope.$watch('model.data', function(newVal, oldVal) {
                if(!_(newVal).isEqual(oldVal)) {
                    updateFlot();
                }
            }, true);
            $scope.$watch('model.flotOptions', function(newVal, oldVal) {
                if(!_(newVal).isEqual(oldVal)) {
                    updateFlot();
                }
            }, true);
            updateFlot();

            function updateData() {
                var self = this;
                if(_(self['dataUpdate']).isArray()) {
                    _(self['dataUpdate']).each(function (newData, dataId) {
                        self.data[dataId].data.splice(0, newData.length);
                        _(newData).each(function (e) {
                            self.data[dataId].data.push(e);
                        });
                    });
                }
                delete self['dataUpdate'];
            }

            $scope.$on('$destroy', function() {
                if($scope.plot !== null) {
                    $scope.plot.destroy();
                }
            });
        }
    }
});