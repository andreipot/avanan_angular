var m = angular.module('widgets');

m.directive('metaInfo', function(path, widgetLogic) {
    return {
        restrict: 'A',
        replace: true,
        templateUrl: path('widgets').directive('meta-info').template(),
        scope: {
            model: '=metaInfo'
        },
        link: function($scope, element, attrs) {
            widgetLogic($scope, $scope.model.logic);
        }
    }
});