var m = angular.module('widgets');

m.directive('widgetsPagination', function(path, widgetsPaginationHelper, paginationHelper) {
    return {
        restrict: 'A',
        replace: true,
        templateUrl: path('widgets').directive('widgets-pagination').template(),
        scope: {
            /**
             *  {
             *      total: 10, //not required if haveNext exist
             *      haveNext: true, //not required if total exist
             *      current: 4
             *  }
             */
            model: '=widgetsPagination',
            /**
             *  {
             *      urlPattern: '/api/pagination/:page',
             *      pagePlaceholder: ':page',
             *      pagesAround: 2
             *  }
             */
            options: '='
        },
        link: function($scope, element, attrs) {
            function generateHref(page) {
                page = Math.min(Math.max(1, page), $scope.model.total);
                return $scope.options.urlPattern.replace($scope.options.pagePlaceholder, page);
            }

            $scope.$watch('model', function() {
                if(_($scope.options).isUndefined()) {
                    $scope.pages = [];
                } else {
                    var total = $scope.model.total || widgetsPaginationHelper.totalPages();
                    widgetsPaginationHelper.totalPages(total);
                    $scope.pages = paginationHelper.generatePages({
                        total: total,
                        current: widgetsPaginationHelper.currentPage(),
                        haveNext: $scope.model.haveNext,
                        pagesAround: $scope.options.pagesAround,
                        valueGenerator: generateHref
                    });
                }
            });
        }
    }
});