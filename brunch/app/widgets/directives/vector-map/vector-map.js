var m = angular.module('widgets');

m.directive('vectorMap', function($timeout, widgetLogic) {
    var defaultMapOptions = {
        "map": "world_mill_en",
        "scaleColors": [
            "#97d3c5",
            "#8484f6"
        ],
        "normalizeFunction": "polynomial",
        "hoverOpacity": 0.7,
        "hoverColor": 0,
        "focusOn": {
            "x": 0.5,
            "y": 0.5,
            "scale": 1
        },
        "zoomMin": 0.85,
        "zoomStep": 1.6,
        "zoomOnScroll": false,
        "zoomButtons": true,
        "markerStyle": {
            "initial": {
                "fill": "#f39c12",
                "stroke": "#FFF"
            },
            "hover": {
                "fill": "#FFF",
                "stroke": "#2980b9"
            }
        },
        "backgroundColor": "#fff",
        "regionStyle": {
            "initial": {
                "fill": "#33414e",
                "fill-opacity": 1,
                "stroke": "#33414e",
                "stroke-width": 0,
                "stroke-opacity": 0
            },
            "hover": {
                "fill-opacity": 0.8
            },
            "selected": {
                "fill": "yellow"
            }
        }
    };

    return {
        restrict: 'A',
        scope: {
            model: '=vectorMap'
        },
        link: function ($scope, element, attrs) {
            widgetLogic($scope, $scope.model.logic);
            var map = null;
            var lastMarkerId = 0;
            $scope.$watch('model', function() {
                $scope.model.data = $scope.model.data || [];
                if(map === null && $scope.model) {
                    map = {};
                    $timeout(function() {
                        $scope.model.updateData = updateData;
                        var options = $.extend({
                            onMarkerTipShow: function(event, label, index){
                                var name = $scope.model.data[index].name;
                                var metadata = $scope.model.data[index].metadata;
                                if(_(metadata).isObject()) {
                                    var html = '<div class="map-label">';
                                    if (_(name).isString()) {
                                        html += '<span>' + name + '</span>';
                                    }
                                    html += '<table><tbody>';
                                    _(metadata).each(function (data) {
                                        html += '<tr>';
                                        html += '<td>' + data.label + ':</td>';
                                        html += '<td>' + data.value + '</td>';
                                        html += '</tr>';
                                    });
                                    html += '</tbody></table></div>';
                                    label.html(html);
                                } else {
                                    label.html(metadata || '');
                                }
                            },
                            onMarkerClick: function(events, index) {
                                ($scope.model.data[index].onClick || _.noop)(events);
                            }
                        }, defaultMapOptions, $scope.model.mapOptions);
                        options.markers = $scope.model.data || [];
                        lastMarkerId = options.markers.length;
                        element.empty();
                        element.vectorMap(options);
                        element.show();
                        map = element.vectorMap('get', 'mapObject');
                    });
                }
            });

            function updateData() {
                var self = this;

                if(_(self['dataUpdate']).isArray()) {
                    this.options = this.options || {};
                    var maxData = this.options.maxData;

                    _(self['dataUpdate']).each(function(e) {
                        lastMarkerId++;
                        if(!_(maxData).isUndefined() && lastMarkerId >= maxData) {
                            lastMarkerId = 0;
                        }
                        map.addMarker(lastMarkerId, e);
                        self.data[lastMarkerId] = e;
                    });
                } else if(_(self['data']).isArray()) {
                    map.removeAllMarkers();
                    map.addMarkers(self['data']);
                    lastMarkerId = 0;
                }
                delete self['dataUpdate'];
            }

            $scope.$on('$destroy', function() {
                if(map !== null) {
                    map.remove();
                }
            });
        }
    }
});