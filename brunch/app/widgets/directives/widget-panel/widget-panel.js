var m = angular.module('widgets');

m.directive('widgetPanel', function(path, widgetSettings) {
    return {
        restrict: 'A',
        replace: false,
        transclude: true,
        templateUrl: path('widgets').directive('widget-panel').template(),
        scope: {
            model: '=widgetPanel'
        },
        link: function($scope, element, attrs) {
            var unWatchModel = $scope.$watch('model', function(model) {
                if(!_(model).isUndefined()) {
                    init();
                    unWatchModel();
                }
            });

            $scope.$watch('model.wrapper.filters', function() {
                _($scope.model.wrapper.filters).each(function (filter) {
                    var savedState = widgetSettings.param($scope.model.uuid, filter.value);
                    if (!_(savedState).isUndefined()) {
                        filter.state = savedState;
                    } else {
                        widgetSettings.param($scope.model.uuid, filter.value, filter.state);
                    }
                });
            });

            function init() {
                if (_($scope.model.wrapper).isUndefined()) {
                    $scope.model.wrapper = $scope.model;
                }
                $scope.model.wrapper.class = $scope.model.wrapper.class || 'panel-default';

                $scope.minimized = false;
                $scope.showSettings = function () {
                    $($scope.model.settings).modal("show");
                };

                $scope.minimizeToggle = function (time) {
                    if (!$scope.model.wrapper.minimize) {
                        return;
                    }
                    if (_(time).isUndefined()) {
                        time = "slow";
                    }
                    $scope.minimized = !$scope.minimized;
                    var body = element.find(".panel-body");
                    body.addClass('minimizing');
                    body.slideToggle(time, function() {
                        body.removeClass('minimizing');
                    });
                };
                if ($scope.model.wrapper.minimized) {
                    $scope.minimizeToggle(0);
                }

                $scope.close = function () {
                    element.fadeOut();
                };

                $scope.filterChanged = function (filter) {
                    widgetSettings.param($scope.model.uuid, filter.value, filter.state);
                    if (!filter.local) {
                        $scope.model.forceUpdate();
                    }
                };

                $scope.icons = $scope.model.icons || {
                    minimize: 'fa-chevron-up',
                    expand: 'fa-chevron-down'
                };
            }
        }
    }
});