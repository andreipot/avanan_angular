var m = angular.module('widgets');

m.directive('fileSystemTreeNode', function(path, recursionHelper) {
    return {
        restriction: 'A',
        replace: false,
        templateUrl: path('widgets').directive('file-system-tree-node').template(),
        scope: {
            model: '=fileSystemTreeNode',
            node: '=',
            options: '='
        },
        compile: function(element) {
            return recursionHelper.compile(element, this.link);
        },
        link: function($scope, element, attr) {
            $scope.collapsed = true;
            $scope.loading = false;
            $scope.nodes = [];

            $scope.getButtonClass = function() {

                var result = {
                    expandable: $scope.node[$scope.options.expandableField]
                };
                if($scope.loading) {
                    result['fa-spinner'] = true;
                } else if($scope.collapsed) {
                    result['fa-plus-square'] = true;
                } else {
                    result['fa-minus-square'] = true;
                }
                return result;
            };

            $scope.toggle = function() {
                if($scope.loading || !$scope.node[$scope.options.expandableField]) {
                    return;
                }
                if(!$scope.collapsed) {
                    $scope.collapsed = true;
                    $scope.nodes = [];
                } else {
                    $scope.loading = true;
                    $scope.options.retrieveContent($scope.node[$scope.options.primaryField]).then(function(response) {
                        $scope.nodes = response.data.rows;
                        $scope.loading = false;
                        $scope.collapsed = false;
                    }, function() {
                        $scope.loading = false;
                    });
                }
            };

            $scope.selected = false;
            $scope.$watch('model', function() {
                if(_($scope.model.selected).isUndefined()) {
                    $scope.selected = false;
                } else {
                    $scope.selected = $scope.node[$scope.options.primaryField] == $scope.model.selected[$scope.options.primaryField];
                }
            }, true);

            $scope.select = function() {
                $scope.model.selected = $.extend(true, {}, $scope.node);
            };
        }
    };
});