var m = angular.module('widgets');

m.directive('widgetSelector', function(path, recursionHelper) {
    return {
        restrict: 'A',
        replace: true,
        templateUrl: path('widgets').directive('widget-selector').template(),
        scope: {
            model: '=widgetSelector'
        },
        compile: function(element, link) {
            return recursionHelper.compile(element);
        }
    }
});