var m = angular.module('widgets');

m.directive('donut', function($timeout) {
    return {
        restrict: 'A',
        scope: {
            /**
             *  {
             *      data: [{label: 'label', value: 50, color: '#FAFAFA'}, ...]
             *      formatter: function (y) { return y + "%" }
             *  }
             */
            model: '=donut'
        },
        link: function($scope, element, attrs) {
            var donut = null;
            $scope.$watch('model', function() {
                $timeout(function() {
                    element.empty();
                    donut = Morris.Donut({
                        element: element,
                        data: _($scope.model.data).map(function (e) {
                            return {label: e.label, value: e.value};
                        }),
                        colors: _($scope.model.data).map(function (e) {
                            return e.color;
                        }),
                        formatter: $scope.model.formatter || function (y) {
                            return y + "%"
                        },
                        resize: false
                    });
                });
            });

            var resizeHandler = null;
            $scope.$watch(function() {
                return element.parent().width() + element.parent().height();
            }, function() {
                if(donut) {
                    if(resizeHandler !== null) {
                        $timeout.cancel(resizeHandler);
                    }
                    resizeHandler = $timeout(function() {
                        donut.resizeHandler();
                        resizeHandler = null;
                    }, 100);
                }
            })
        }
    }
});