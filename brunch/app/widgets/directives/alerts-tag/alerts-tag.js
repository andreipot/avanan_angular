var m = angular.module('widgets');

m.directive('alertsTag', function(path) {
    return {
        restrict: 'A',
        replace: true,
        templateUrl: path('widgets').directive('alerts-tag').template(),
        scope: {
            model: "=alertsTag",
            tagName: "=tagName"
        },
        link: function ($scope, element, attr) {
            $scope.mouseOver = function() {
                $scope.style = $scope.model.hover || $scope.model.style || $scope.model;
            };

            $scope.mouseLeave = function() {
                $scope.style = $scope.model.style || $scope.model;
            };

            $scope.mouseLeave();
        }
    }
});