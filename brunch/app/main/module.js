var m = angular.module('app', ['ngRoute', 'ngCookies', 'utils', 'ui.slimscroll', 'auth', 'dashboard', 'profiles', 'app-store', 'policy', 'monospaced.mousewheel', 'dao', 'user-management', 'ngSanitize', 'viewer', 'security-stack', 'angular-data.DSCacheFactory', 'report', 'settings']);

m.config(function ($routeProvider, $locationProvider, $httpProvider, $compileProvider) {
    function templatePath(moduleName, controllerName) {
        return '/assets/' + moduleName + '/controllers/' + controllerName + '/' + controllerName + '.html';
    }

    var authRequired = {
        currentUser: ['$q', 'avananUserDao', 'sideMenuManager', function($q, avananUserDao, sideMenuManager) {
            var deferred = $q.defer();
            avananUserDao.current().then(function(response) {
                sideMenuManager.currentUser(response.data);
                deferred.resolve(response);
            }, deferred.reject);
            return deferred.promise;
        }],
        featureResolver: ['feature', function(feature) {
            return feature.init();
        }],
        modulesManagerResolver: ['modulesManager', function(modulesManager) {
            return modulesManager.init();
        }]
    };

    var noAuthRequired = {
        userNotExist: ['$q', 'avananUserDao', function($q, avananUserDao) {
            var deferred = $q.defer();
            avananUserDao.current().then(function() {
                deferred.reject();
            }, function() {
                deferred.resolve();
            });
            return deferred.promise;
        }]
    };

    function title(text) {
        return htmlTitle('<strong>' + text + '</strong>');
    }

    function htmlTitle(text) {
        return '#' + JSON.stringify({
                display: 'html',
                text: text
            });
    }

    $routeProvider
        .when('/dashboard', {
            templateUrl: templatePath('dashboard', 'dashboard'),
            controller: 'DashboardController',
            title: title('Dashboard'),
            name: 'dashboard',
            resolve: authRequired
        })
        .when('/dashboard/:module/', {
            templateUrl: templatePath('dashboard', 'dashboard'),
            controller: 'DashboardController',
            title: title('Dashboard'),
            name: 'dashboard',
            resolve: authRequired
        })
        .when('/application/:module/:id*', {
            templateUrl: templatePath('dashboard', 'dashboard'),
            controller: 'ApplicationProfileController',
            title: title('Application profile'),
            name: 'application',
            resolve: authRequired
        })
        .when('/user/:module/:id*', {
            templateUrl: templatePath('dashboard', 'dashboard'),
            controller: 'UserProfileController',
            title: title('User profile'),
            name: 'user',
            resolve: authRequired
        })
        .when('/folder/:module/:id*', {
            templateUrl: templatePath('dashboard', 'dashboard'),
            controller: 'FolderProfileController',
            title: title('Folder profile'),
            name: 'folder',
            resolve: authRequired
        })
        .when('/file/:module/:id*', {
            templateUrl: templatePath('dashboard', 'dashboard'),
            controller: 'FileProfileController',
            title: title('File profile'),
            name: 'file',
            resolve: authRequired
        })
        .when('/organization/:module/:id*', {
            templateUrl: templatePath('dashboard', 'dashboard'),
            controller: 'OrganizationProfileController',
            title: title('Organization profile'),
            name: 'organization',
            resolve: authRequired
        })
        .when('/group/:module/:id*', {
            templateUrl: templatePath('dashboard', 'dashboard'),
            controller: 'GroupProfileController',
            title: title('Group profile'),
            name: 'group',
            resolve: authRequired
        })
        .when('/site/:module/:id*', {
            templateUrl: templatePath('dashboard', 'dashboard'),
            controller: 'SiteProfileController',
            title: title('Site profile'),
            name: 'site',
            resolve: authRequired
        })
        .when('/ip/:module/:id*', {
            templateUrl: templatePath('dashboard', 'dashboard'),
            controller: 'IpProfileController',
            title: title('IP profile'),
            name: 'ip',
            resolve: authRequired
        })
        .when('/viewer/:module/:id', {
            templateUrl: templatePath('viewer', 'viewer'),
            controller: 'ViewerController',
            title: title('Viewer'),
            name: 'viewer',
            resolve: authRequired
        })

        .when('/auth/:page', {
            templateUrl: templatePath('auth', 'auth'),
            controller: 'AuthController',
            title: title('Login'),
            name: 'auth',
            fullscreen: true,
            resolve: noAuthRequired
        })
        .when('/login', {
            redirectTo: '/auth/login'
        })
        .when('/password-reset', {
            redirectTo: '/auth/password-reset'
        })
        .when('/app-store/:filter', {
            templateUrl: templatePath('dashboard', 'dashboard'),
            controller: 'AppStoreController',
            title: 'Applications Store',
            fullTitle: htmlTitle('<strong>Extend the avanan platform</strong> with these 3rd party services'),
            name: 'app-store',
            accessor: 'appStore',
            resolve: authRequired
        })
        .when('/app-store', {
            templateUrl: templatePath('dashboard', 'dashboard'),
            controller: 'AppStoreController',
            title: 'Applications Store',
            fullTitle: htmlTitle('<strong>Extend the avanan platform</strong> with these 3rd party services'),
            name: 'app-store-all',
            accessor: 'appStore',
            resolve: authRequired
        })
        .when('/cloud-apps', {
            templateUrl: templatePath('dashboard', 'dashboard'),
            controller: 'AppStoreController',
            title: title('Cloud apps'),
            name: 'cloud-apps',
            accessor: 'cloudApps',
            resolve: authRequired
        })
        .when('/user-management/create', {
            templateUrl: templatePath('user-management', 'user-edit'),
            controller: 'UserEditController',
            title: title('New user'),
            name: 'user-management-create',
            resolve: authRequired
        })
        .when('/user-management/edit/:id', {
            templateUrl: templatePath('user-management', 'user-edit'),
            controller: 'UserEditController',
            title: title('Edit user'),
            name: 'user-management-edit',
            resolve: authRequired
        })
        .when('/user-management', {
            templateUrl: templatePath('user-management', 'user-list'),
            controller: 'UserListController',
            title: title('Users management'),
            name: 'user-management',
            resolve: authRequired
        })

        .when('/report/create', {
            templateUrl: templatePath('report', 'report-create'),
            controller: 'ReportCreateController',
            title: title('Report create'),
            name: 'report-create',
            resolve: authRequired
        })
        .when('/report/edit/:id', {
            templateUrl: templatePath('report', 'report'),
            controller: 'ReportController',
            title: title('Report'),
            name: 'report-edit',
            resolve: authRequired
        })
        .when('/report', {
            templateUrl: templatePath('report', 'report-list'),
            controller: 'ReportListController',
            title: title('Reports'),
            name: 'report-list',
            resolve: authRequired
        })
        .when('/policy/create', {
            templateUrl: templatePath('policy', 'policy-create'),
            controller: 'PolicyCreateController',
            title: title('Policy create'),
            name: 'policy-create',
            resolve: authRequired
        })
        .when('/policy/edit/:id', {
            templateUrl: templatePath('policy', 'policy'),
            controller: 'PolicyController',
            title: title('Policy'),
            name: 'policy-edit',
            resolve: authRequired
        })
        .when('/policy', {
            templateUrl: templatePath('policy', 'policy-list'),
            controller: 'PolicyListController',
            title: title('Policies'),
            name: 'policy-list',
            resolve: authRequired
        })
        .when('/policy/root-cause-view/:internalName', {
            templateUrl: templatePath('policy', 'policy-root-cause-view'),
            controller: 'PolicyRootCauseViewController',
            title: title('Policy'),
            name: 'policy-root-cause-view',
            resolve: authRequired
        })
        .when('/policy/allows/:id', {
            templateUrl: templatePath('policy', 'policy-allows'),
            controller: 'PolicyAllowsController',
            title: title('Policy allows'),
            name: 'policy-allows',
            resolve: authRequired
        })
        .when('/security-stack', {
            templateUrl: templatePath('dashboard', 'dashboard'),
            controller: 'SecurityStackController',
            title: title('Security stack'),
            name: 'security-stack',
            resolve: authRequired
        })
        .when('/settings', {
            templateUrl: templatePath('settings', 'settings'),
            controller: 'SettingsController',
            title: title('Settings'),
            name: 'settings',
            resolve: authRequired
        })
        .when('/404', {
            templateUrl: templatePath('main', 'not-found'),
            controller: 'NotFoundController',
            title: title('404'),
            name: '404',
            resolve: authRequired
        })
        .when('/500', {
            templateUrl: templatePath('main', 'internal-error'),
            controller: 'InternalErrorController',
            title: title('500'),
            name: '500',
            resolve: authRequired
        })
        .otherwise({
            redirectTo: '/dashboard'
        });

    $locationProvider.html5Mode(false);
    $locationProvider.hashPrefix('!');

    //initialize get if not there
    if (!$httpProvider.defaults.headers.get) {
        $httpProvider.defaults.headers.get = {};
    }
    //disable IE ajax request caching
    $httpProvider.defaults.headers.get['If-Modified-Since'] = '0';

    //TODO should be true on dev and false on production
    $compileProvider.debugInfoEnabled(false);
});

m.run(function ($http, DSCacheFactory, $location, $route, $rootScope) {
    var original = $location.path;
    $location.path = function (path, reload) {
        if(_(reload).isUndefined()) {
            reload = true;
        }
        if (reload === false) {
            var lastRoute = $route.current;
            var un = $rootScope.$on('$locationChangeSuccess', function () {
                $route.current = lastRoute;
                un();
            });
        }
        return original.apply($location, [path]);
    };

    DSCacheFactory('defaultCache', {
        maxAge: 1000, // Items added to this cache expire after 15 minutes.
        deleteOnExpire: 'aggressive' // Items will be deleted from this cache right when they expire.
    });

    $http.defaults.cache = DSCacheFactory.get('defaultCache');
});