var m = angular.module('app');

m.controller('MainController', function($scope, widgetPolling, modulesManager, routeHelper, $route, sideMenuManager, $timeout, avananUserDao, modals) {
    $scope.$on('$routeChangeSuccess', function(event, current) {
        sideMenuManager.fullscreen($route.current.fullscreen || false);

        $('html, body').animate({ scrollTop: 0 }, 'fast');
        widgetPolling.stopUpdating();
        if(!_(current.$$route).isUndefined() && current.$$route.name != 'auth') {
            $('body').css('background', 'none');
            if (_(current.pathParams.module).isString()) {
                modulesManager.currentModule(current.pathParams.module);
            }
            modulesManager.reloadModules();
        } else {
            $('body').css('background', "url('/assets/auth/controllers/login/images/background.png')")
        }
    });

    $scope.$on('$routeChangeError', function(event, current, previous) {
        if(current.$$route.name == 'auth') {
            routeHelper.redirectTo('dashboard');
        } else {
            routeHelper.redirectTo('auth', {
                page: 'login'
            });
        }
    });

    var maxErrorCount = 10;
    var errorCount = maxErrorCount;
    window.successLogin = function() {
        function checkCurrent() {
            avananUserDao.current().then(function () {
                routeHelper.redirectTo('dashboard');
            }, function () {
                if(errorCount < maxErrorCount) {
                    errorCount++;
                    $timeout(checkCurrent, 500);
                }
            });
        }
        if(errorCount < maxErrorCount) {
            errorCount = 0;
        } else {
            checkCurrent();
        }
    };

    window.failedLogin = function() {
        modals.alert('You are not authorized to access this service', {
            title: ''
        });
    };

    $(window).resize(function() {
        if (Modernizr.mq('(min-width: 768px)') && Modernizr.mq('(max-width: 868px)')) {
            $('#wrapper').addClass('sidebar-mini').addClass('window-resize');
            $('.main-menu').find('.openable').removeClass('open');
            $('.main-menu').find('.submenu').removeAttr('style');
        }
        else if (Modernizr.mq('(min-width: 869px)')) {
            if($('#wrapper').hasClass('window-resize'))	{
                $('#wrapper').removeClass('sidebar-mini window-resize');
                $('.main-menu').find('.openable').removeClass('open');
                $('.main-menu').find('.submenu').removeAttr('style');
            }
        }
        else {
            $('#wrapper').removeClass('sidebar-mini window-resize');
            $('.main-menu').find('.openable').removeClass('open');
            $('.main-menu').find('.submenu').removeAttr('style');
        }
    });
});


function otherScripts()	{
    $('.login-link').click(function(e) {
        e.preventDefault();
        href = $(this).attr('href');

        $('.login-wrapper').addClass('fadeOutUp');

        setTimeout(function() {
            window.location = href;
        }, 900);

        return false;
    });

    //Inbox sidebar (inbox.html)
    $('#inboxMenuToggle').click(function()	{
        $('#inboxMenu').toggleClass('menu-display');
    });

    //Collapse panel
    $('.collapse-toggle').click(function()	{

        $(this).parent().toggleClass('active');

        var parentElm = $(this).parent().parent().parent().parent();

        var targetElm = parentElm.find('.panel-body');

        targetElm.toggleClass('collapse');
    });

    //Number Animation
    var currentVisitor = $('#currentVisitor').text();

    $({numberValue: 0}).animate({numberValue: currentVisitor}, {
        duration: 2500,
        easing: 'linear',
        step: function() {
            $('#currentVisitor').text(Math.ceil(this.numberValue));
        }
    });

    var currentBalance = $('#currentBalance').text();

    $({numberValue: 0}).animate({numberValue: currentBalance}, {
        duration: 2500,
        easing: 'linear',
        step: function() {
            $('#currentBalance').text(Math.ceil(this.numberValue));
        }
    });

    //Refresh Widget
    $('.refresh-widget').click(function() {
        var _overlayDiv = $(this).parent().parent().parent().parent().find('.loading-overlay');
        _overlayDiv.addClass('active');

        setTimeout(function() {
            _overlayDiv.removeClass('active');
        }, 2000);

        return false;
    });

    //Hover effect on touch device
    $('.image-wrapper').bind('touchstart', function(e) {
        $('.image-wrapper').removeClass('active');
        $(this).addClass('active');
    });

    //Dropdown menu with hover
    $('.hover-dropdown').hover(
        function(){ $(this).addClass('open') },
        function(){ $(this).removeClass('open') }
    )

    //upload file
    $('.upload-demo').change(function()	{
        var filename = $(this).val().split('\\').pop();
        $(this).parent().find('span').attr('data-title',filename);
        $(this).parent().find('label').attr('data-title','Change file');
        $(this).parent().find('label').addClass('selected');
    });

    $('.remove-file').click(function()	{
        $(this).parent().find('span').attr('data-title','No file...');
        $(this).parent().find('label').attr('data-title','Select file');
        $(this).parent().find('label').removeClass('selected');

        return false;
    });

    //to do list
    $('.task-finish').click(function()	{
        if($(this).is(':checked'))	{
            $(this).parent().parent().addClass('selected');
        }
        else	{
            $(this).parent().parent().removeClass('selected');
        }
    });

    //Delete to do list
    $('.task-del').click(function()	{
        var activeList = $(this).parent().parent();

        activeList.addClass('removed');

        setTimeout(function() {
            activeList.remove();
        }, 1000);

        return false;
    });

    // Popover
    $("[data-toggle=popover]").popover();

    // Tooltip
    $("[data-toggle=tooltip]").tooltip();

}


