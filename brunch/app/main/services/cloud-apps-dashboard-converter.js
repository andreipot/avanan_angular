var m = angular.module('app');

m.factory('cloudAppsDashboardConverter', function ($q, path, routeHelper) {
    return function (input, args) {
        var deferred = $q.defer();

        var appStore = input.data;
        var saasGroup = _(_(appStore.groups).filter(function (group) {
            return group.family == 'saas';
        })).first();

        var data = {
            saas: _(saasGroup.modules).map(function (module) {
                return {
                    state: module.state,
                    img: path('security-stack').img('applications/' + module.stack_icon),
                    title: module.label,
                    link: routeHelper.getPath('dashboard', {
                        module: module.name
                    }),
                    name: module.name
                };
            }),
            modules: _(appStore.groups).chain().map(function(group) {
                return _(group.modules).map(function(module) {
                    return [module.name, module.state];
                });
            }).flatten(true).object().value()
        };

        deferred.resolve($.extend({}, input, {
            data: data
        }));
        return deferred.promise;
    };
});