var m = angular.module('app');

m.service('modulesManager', function($timeout, $cookies, $injector, $q) {
    var currentModule = $cookies.currentModule || 'google_drive';
    $cookies.currentModule = currentModule;

    var cloudApps = [];
    var modules = {};

    function reloadModules() {
        var modulesDao = $injector.get('modulesDao');
        var routeHelper = $injector.get('routeHelper');
        return modulesDao.cloudAppsDashboard().then(function (response) {
            if(!_(angular.copy(cloudApps)).isEqual(response.data.saas)) {
                cloudApps = response.data.saas;
            }
            modules = response.data.modules;

            var app = modulesManager.cloudApp(modulesManager.currentModule());
            if(_(app).isUndefined() || app.state != 'active') {
                var first = _(cloudApps).find(function(app) {
                    return app.state == 'active';
                });
                modulesManager.currentModule(first.name);
                routeHelper.redirectTo('dashboard', {
                    module: modulesManager.currentModule()
                });
            }
            return $q.when({});
        });
    }

    var initialized = false;

    var modulesManager = {
        init: function() {
            if(initialized) {
                return $q.when();
            }
            initialized = true;
            return reloadModules();
        },
        currentModule: function(newModule) {
            if(_(newModule).isString()) {
                currentModule = $cookies.currentModule = newModule;
            }
            return currentModule;
        },
        reloadModules: reloadModules,
        cloudApps: function() {
            return _(cloudApps).filter(function(app) {
                return app.state == 'active';
            });
        },
        cloudApp: function(name) {
            return _(cloudApps).find(function(app) {
                return app.name == name;
            });
        },
        isActive: function(name) {
            return modules[name] == 'active';
        }
    };
    return modulesManager;
});