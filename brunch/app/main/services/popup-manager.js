var m = angular.module('app');

m.service('popupManager', function() {
    var popupName = 'none';
    return {
        current: function(name) {
            if(_(name).isString()) {
                popupName = name;
            }
            return popupName;
        },
        hideAll: function() {
            popupName = 'none';
        }
    }
});