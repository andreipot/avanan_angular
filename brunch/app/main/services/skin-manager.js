var m = angular.module('app');

m.service('skinManager', function($cookies) {
    return {
        current: function(newSkin) {
            if(_(newSkin).isString()) {
                $cookies['skin_color'] = newSkin;
            }
            return /*$cookies['skin_color'] || */'skin-light';
        }
    }
});