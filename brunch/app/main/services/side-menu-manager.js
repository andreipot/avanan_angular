var m = angular.module('app');

m.service('sideMenuManager', function($q, $timeout, path) {
    var data = {
        menus: [],
        updateTick: 1000,
        updateUrl: 'side-menu'
    };
    var dataUpdateHandle = null;
    function updateMenus() {
        path('main').directive('main-menu').json('main-menu').retrieve().then(function(result) {
            data = $.extend(data, result.data);
        }, function() {
            dataUpdateHandle = $timeout(updateMenus, 1000);
        });
    }
    dataUpdateHandle = $timeout(updateMenus);

    var hidden = false;
    var mini = false;
    var display = false;
    var fixed = true;
    var fullscreen = true;
    var currentUser = {};

    return {
        toggleHide: function() {
            hidden = !hidden;
        },
        hidden: function(newState) {
            if(_(newState).isBoolean()) {
                hidden = newState;
            }
            return hidden;
        },
        toggleMini: function() {
            mini = !mini;
        },
        mini: function(newState) {
            if(_(newState).isBoolean()) {
                mini = newState;
            }
            return mini;
        },
        toggleDisplay: function() {
            display = !display;
        },
        display: function(newState) {
            if(_(newState).isBoolean()) {
                display = newState;
            }
            return display;
        },
        toggleFixed: function() {
            fixed = !fixed;
        },
        fixed: function(newState) {
            if(_(newState).isBoolean()) {
                fixed = newState;
            }
            return fixed;
        },
        getMenus: function() {
            return data.menus;
        },
        updateMenus: function() {
            data.updateTick = 1000;
            $timeout.cancel(dataUpdateHandle);
            dataUpdateHandle = $timeout(updateMenus);
        },
        fullscreen: function(newState) {
            if(_(newState).isBoolean()) {
                fullscreen = newState;
            }
            return fullscreen;
        },
        currentUser: function(newUser) {
            if(_(newUser).isObject()) {
                currentUser = newUser;
            }
            return currentUser;
        }
    }
});