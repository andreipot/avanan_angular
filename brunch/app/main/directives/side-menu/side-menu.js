var m = angular.module('app');

m.directive('sideMenu', function(path, popupManager, sideMenuManager) {
    var root = path('main').directive('side-menu');
    return {
        restrict: 'A',
        replace: false,
        templateUrl: root.template(),
        scope: {},
        link: function($scope, element, attrs) {
            $scope.sideMenuManager = sideMenuManager;
            $scope.root = root;

            $scope.$watch('sideMenuManager.fixed()', function(fixed) {
                if(fixed) {
                    $('aside').addClass('fixed');
                } else {
                    $('aside').removeClass('fixed');
                }
            });

            $scope.showLogoutConfirm = function() {
                popupManager.current('logoutConfirm');
            };

            $('aside li').hover(
                function(){ $(this).addClass('open') },
                function(){ $(this).removeClass('open') }
            );

            $('.openable > a').click(function()	{
                if(!$('#wrapper').hasClass('sidebar-mini'))	{
                    if( $(this).parent().children('.submenu').is(':hidden') ) {
                        $(this).parent().siblings().removeClass('open').children('.submenu').slideUp();
                        $(this).parent().addClass('open').children('.submenu').slideDown();
                    }
                    else	{
                        $(this).parent().removeClass('open').children('.submenu').slideUp();
                    }
                }

                return false;
            });
        }
    }
});