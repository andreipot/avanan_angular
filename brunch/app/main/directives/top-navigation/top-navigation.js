var m = angular.module('app');

m.directive('topNavigation', function(path, popupManager, sideMenuManager, modulesDao, modulesManager, $routeParams, $route, routeHelper) {
    return {
        restrict: 'A',
        replace: false,
        templateUrl: path('main').directive('top-navigation').template(),
        scope: {},
        link: function($scope, element, attrs) {
            $scope.route = $route;
            $scope.sideMenuManager = sideMenuManager;
            $scope.modulesManager = modulesManager;
            $scope.showLogoutConfirm = function(event) {
                event.preventDefault();
                popupManager.current('logoutConfirm');
            };

            $scope.switchCloudApp = function(app) {
                modulesManager.currentModule(app.name);
            };

            $scope.isAppActive = function(app) {
                return app.name == modulesManager.currentModule() || _($routeParams.module).isUndefined();
            };

            $scope.showSettings = function(event) {
                event.preventDefault();
                routeHelper.redirectTo('settings');
            };
        }
    }
});