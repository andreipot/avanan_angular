var m = angular.module('app');

m.directive('sideMenuDisplayToggle', function(sideMenuManager) {
    return {
        restrict: 'A',
        scope: {},
        link: function($scope, element, attrs) {
            element.click(function() {
                sideMenuManager.toggleDisplay();
                $scope.$apply();
            });
        }
    }
});