var m = angular.module('app');

m.directive('sideMenuMiniToggle', function(sideMenuManager) {
    return {
        restrict: 'A',
        scope: {},
        link: function($scope, element, attrs) {
            element.click(function() {
                sideMenuManager.toggleMini();
                $scope.$apply();
            });
        }
    }
});