var m = angular.module('app');

m.directive('sideMenuHideToggle', function(sideMenuManager) {
    return {
        restrict: 'A',
        scope: {},
        link: function($scope, element, attrs) {
            element.click(function() {
                sideMenuManager.toggleHide();
                $scope.$apply();
            });
        }
    }
});