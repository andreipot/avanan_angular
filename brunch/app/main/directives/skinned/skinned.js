var m = angular.module('app');

m.directive('skinned', function(skinManager) {
    return {
        restrict: 'A',
        scope: {},
        link: function($scope, element, attrs) {
            $scope.skinManager = skinManager;
            $scope.$watch('skinManager.current()', function(newSkin, oldSkin) {
                if(_(oldSkin).isString()) {
                    element.removeClass(oldSkin);
                }
                element.addClass(newSkin);
            });
        }
    }
});