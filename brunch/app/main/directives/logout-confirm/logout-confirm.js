var m = angular.module('app');

m.directive('logoutConfirm', function(path, popupManager, $timeout, routeHelper, avananUserDao) {
    return {
        restrict: 'A',
        replace: true,
        templateUrl: path('main').directive('logout-confirm').template(),
        scope: {},
        link: function($scope, element, attrs) {
            $scope.popupManager = popupManager;
            element.popup({
                pagecontainer: '.container',
                transition: 'all 0.3s',
                onclose: function() {
                    $timeout(function() {
                        popupManager.hideAll();
                    });
                }
            });

            $scope.logout = function(event) {
                event.preventDefault();
                avananUserDao.logout().then(function() {
                    routeHelper.redirectTo('auth', {
                        page: 'login'
                    });
                    element.popup('hide');
                });
            };

            $scope.close = function() {
                element.popup('hide');
            };

            $scope.$watch('popupManager.current()', function(current) {
                if(current === 'logoutConfirm') {
                    element.popup('show');
                }
            });
        }
    }
});