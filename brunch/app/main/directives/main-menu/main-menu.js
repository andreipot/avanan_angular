var m = angular.module('app');

m.directive('mainMenu', function(path, sideMenuManager, routeHelper, feature) {
    return {
        restrict: 'A',
        replace: true,
        templateUrl: path('main').directive('main-menu').template(),
        scope: {},
        link: function ($scope, element, attrs) {
            $scope.sideMenuManager = sideMenuManager;
            $scope.routeHelper = routeHelper;

            function closeMenus() {
                element.find('.openable').removeClass('open');
                element.find('.submenu').removeAttr('style');
            }
            $scope.$watch('sideMenuManager.hidden()', closeMenus);
            $scope.$watch('sideMenuManager.mini()', closeMenus);
            $scope.$watch('sideMenuManager.display()', closeMenus);

            $scope.$watch('sideMenuManager.getMenus()', function(menus) {
                $scope.menus = _(menus).filter(function(menu) {
                    return _(menu.feature).isUndefined() || feature[menu.feature];
                });
                _($scope.menus).each(function(menu) {
                    menu.isActive = function() {
                        return routeHelper.isActive(menu.name);
                    };
                    menu.path = function() {
                        return '/#!' + routeHelper.getPath(menu.name, menu.params);
                    };
                });
            });
        }
    }
});