var m = angular.module('app');

m.directive('wrapper', function(path, sideMenuManager, $route, widgetPolling) {
    return {
        restrict: 'A',
        replace: true,
        templateUrl: path('main').directive('wrapper').template(),
        scope: {},
        link: function($scope, element, attrs) {
            $scope.widgetPolling = widgetPolling;
            $scope.sideMenuManager = sideMenuManager;
            $scope.route = $route;
            $scope.showBottomPagination = false;
            $scope.$watch('route.current', function(current) {
                $scope.paginationOptions = current.paginationOptions;
            });

            $scope.$watch(function() {
                return $('#ng-view').height() + $(window).height();
            }, function() {
                $scope.showBottomPagination = $('#ng-view').height() > $(window).height();
            });

            $scope.$watch('sideMenuManager.hidden()', function(state) {
                if(state) {
                    element.addClass('sidebar-hide');
                } else {
                    element.removeClass('sidebar-hide');
                }
            });
            $scope.$watch('sideMenuManager.mini()', function(state) {
                element.off("resize");
                if(state) {
                    element.addClass('sidebar-mini');
                } else {
                    element.removeClass('sidebar-mini');
                }
            });
            $scope.$watch('sideMenuManager.display()', function(state) {
                if(state) {
                    element.addClass('sidebar-display');
                } else {
                    element.removeClass('sidebar-display');
                }
            });

            if(!element.hasClass('sidebar-mini')) {
                if (Modernizr.mq('(min-width: 768px)') && Modernizr.mq('(max-width: 868px)')) {
                    $('#wrapper').addClass('sidebar-mini');
                }
            }
        }
    }
});