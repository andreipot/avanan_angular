var m = angular.module('app');

m.directive('scrollToTop', function() {
    return {
        restrict: 'A',
        replace: true,
        template: '<a href="" id="scroll-to-top" class="hidden-print" ng-click="scroll()"><i class="fa fa-chevron-up"></i></a>',
        scope: {},
        link: function($scope, element, attrs) {
            $scope.scroll = function() {
                $("html, body").animate({ scrollTop: 0 }, 600);
            }
        }
    }
});