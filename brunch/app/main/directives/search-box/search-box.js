var m = angular.module('app');

m.directive('searchBox', function(path, $timeout, globalSearchDao, ref) {
    return {
        restrict: 'A',
        replace: true,
        templateUrl: path('main').directive('search-box').template(),
        scope: {},
        link: function ($scope, element, attrs) {
            var columns = {
                "user": [{
                    "id": "entity_id",
                    "hidden": true
                }, {
                    "id": "image",
                    "converter": "userImageConverter",
                    "dataStyle": {
                        "width": "30px"
                    }
                }, {
                    "id": "name",
                    "href": {
                        "type": "user",
                        "params": {
                            "id": "entity_id"
                        }
                    }
                }],
                "group": [{
                    "id": "entity_id",
                    "hidden": true
                }, {
                    "id": "image",
                    "converter": "groupImageConverter",
                    "dataStyle": {
                        "width": "30px"
                    }
                }, {
                    "id": "name",
                    "href": {
                        "type": "group",
                        "params": {
                            "id": "entity_id"
                        }
                    }
                }],
                "file": [{
                    "id": "entity_id",
                    "hidden": true
                }, {
                    "id": "mime",
                    "converter": "mimeIconConverter",
                    "dataStyle": {
                        "width": "30px"
                    }
                }, {
                    "id": "name",
                    "href": {
                        "type": "file",
                        "params": {
                            "id": "entity_id"
                        }
                    }
                }],
                "folder": [{
                    "id": "entity_id",
                    "hidden": true
                }, {
                    "id": "mime",
                    "converter": "mimeIconConverter",
                    "dataStyle": {
                        "width": "30px"
                    }
                }, {
                    "id": "name",
                    "href": {
                        "type": "folder",
                        "params": {
                            "id": "entity_id"
                        }
                    }
                }],
                "site": [{
                    "id": "entity_id",
                    "hidden": true
                }, {
                    "id": "mime",
                    "converter": "mimeIconConverter",
                    "dataStyle": {
                        "width": "30px"
                    }
                }, {
                    "id": "name",
                    "href": {
                        "type": "site",
                        "params": {
                            "id": "entity_id"
                        }
                    }
                }],
                "app": [{
                    "id": "entity_id",
                    "hidden": true
                }, {
                    "id": "image",
                    "converter": "applicationImageConverter",
                    "dataStyle": {
                        "width": "30px"
                    }
                }, {
                    "id": "name",
                    "href": {
                        "type": "application",
                        "params": {
                            "id": "entity_id"
                        }
                    }
                }]
            };

            $scope.showResults = ref(false);

            $scope.tableOptions = {};
            $scope.tableModels = {};

            $scope.haveResults = function() {
                return !_($scope.tableModels).isEqual({});
            };

            var searchTimeout = null;
            function search(tableModels) {
                searchTimeout = null;

                globalSearchDao.search(columns, $scope.keyword).then(function(response) {
                    $scope.searching = false;
                    _(response).each(function(promise, key) {
                        promise.then(function(tableModel) {
                            tableModels[key] = $.extend(tableModel.data, {
                                columns: columns[key],
                                options: {}
                            });
                        });
                    });
                });
            }

            $scope.$watch('keyword', function(keyword) {
                if(!_(searchTimeout).isNull()) {
                    $timeout.cancel(searchTimeout);
                    searchTimeout = null;
                }
                if(_(keyword).isString() && keyword.length) {
                    $scope.searching = true;
                    searchTimeout = $timeout(function() {
                        $scope.tableModels = {};
                        search($scope.tableModels);
                    }, 300);
                } else {
                    $scope.tableModels = {};
                }
            });

            $scope.$on('$routeChangeSuccess', function(event, next, current) {
                $scope.keyword = '';
            });
        }
    }
});