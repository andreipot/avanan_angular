var m = angular.module('app');

m.directive('skinSelector', function(path, skinManager, sideMenuManager) {
    return {
        restrict: 'A',
        replace: false,
        templateUrl: path('main').directive('skin-selector').template(),
        scope: {},
        link: function($scope, element, attrs) {
            $scope.panelOpen = false;
            $scope.skins = [{
                name: 'default',
                color: '#323447'
            }, {
                name: 'skin-1',
                color: '#efefef'
            }, {
                name: 'skin-2',
                color: '#a93922'
            }, {
                name: 'skin-3',
                color: '#3e6b96'
            }, {
                name: 'skin-4',
                color: '#635247'
            }, {
                name: 'skin-5',
                color: '#3a3a3a'
            }, {
                name: 'skin-6',
                color: '#495B6C'
            }];

            $scope.changeSkin = function(skinName) {
                skinManager.current(skinName);
            };

            $scope.fixedSideMenu = sideMenuManager.fixed();
            $scope.changeFixed = function(fixedSideMenu) {
                sideMenuManager.fixed(fixedSideMenu);
            };

            $scope.togglePanel = function() {
                $scope.panelOpen = !$scope.panelOpen;
            }
        }
    }
});