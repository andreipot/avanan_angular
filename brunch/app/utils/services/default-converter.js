var m = angular.module('utils');

m.factory('defaultConverter', function($q) {
    return function(input) {
        var deferred = $q.defer();
        deferred.resolve(input);
        return deferred.promise;
    }
});