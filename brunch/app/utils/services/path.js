var m = angular.module('utils');
m.factory('path', function(http, uuid) {
    function buildResult(path) {
        return {
            path: path,
            retrieve: function() {
                return http.get(path).params({
                        rid: uuid.random()
                    });
            }
        }
    }
    return function(moduleName) {
        var rootPath = '/assets/' + moduleName;
        return {
            directive: function(directiveName) {
                var directivePath = rootPath + '/directives/' + directiveName;
                return {
                    template: function(templateName) {
                        templateName = templateName || directiveName;
                        return directivePath + '/' + templateName + '.html';
                    },
                    img: function(imgName) {
                        return directivePath + '/images/' + imgName;
                    },
                    json: function(jsonName) {
                        return buildResult(directivePath + '/static/' + jsonName + '.json');
                    },
                    staticFile: function(staticFileName) {
                        return buildResult(directivePath + '/static/' + staticFileName);
                    }
                }
            },
            ctrl: function(controllerName) {
                var controllerPath = rootPath + '/controllers/' + controllerName;
                return {
                    template: function(templateName) {
                        templateName = templateName || controllerName;
                        return controllerPath + '/' + templateName + '.html';
                    },
                    img: function(imgName) {
                        return controllerPath + '/images/' + imgName;
                    },
                    json: function(jsonName) {
                        return buildResult(controllerPath + '/static/' + jsonName + '.json');
                    },
                    staticFile: function(staticFileName) {
                        return buildResult(controllerPath + '/static/' + staticFileName);
                    }
                }
            },
            img: function(imgName) {
                return rootPath + '/images/' + imgName;
            },
            json: function(jsonName) {
                return buildResult(rootPath + '/static/' + jsonName + '.json');
            },
            staticFile: function(staticFileName) {
                return buildResult(rootPath + '/static/' + staticFileName);
            }
        }
    }
});