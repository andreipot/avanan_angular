var m = angular.module('utils');

m.factory('lazyFuture', function($q) {
    return function(future) {
        return {
            then: function(callback) {
                $q.when(future()).then(callback);
            }
        }
    }
});