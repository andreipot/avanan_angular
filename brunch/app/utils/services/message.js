var m = angular.module('utils');
m.factory('message', function() {
    return {
        warning: function(message) {
            alert(message);
        },
        error: function(message) {
            alert(message);
        },
        info: function(message) {
            alert(message);
        }
    }
});