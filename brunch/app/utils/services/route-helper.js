var m = angular.module('utils');

m.factory('routeHelper', function($route, modulesManager, $location) {
    var routes = {};
    _($route.routes).each(function(route) {
        if(_(route.name).isString()) {
            routes[route.name] = route;
        }
    });
    var lastParams = {};
    return {
        getPath: function(name, params, qs) {
            params = $.extend({
                module: modulesManager.currentModule()
            }, params);
            qs = qs || {};
            if(_(name).isArray()) {
                name = name[0];
            }
            if(_(routes[name]).isUndefined()) {
                alert('Route with name ' + name + ' not defined');
            }
            var path = routes[name].originalPath;
            _(params).each(function(val, key) {
                path = path.replace(':' + key + '*', val);
                path = path.replace(':' + key + '?', val);
                path = path.replace(':' + key, val);
            });
            var query = _(qs).map(function(val, key) {
                return key + '=' + val;
            }).join(',');
            if(query.length) {
                path = path + '?' + query;
            }
            return path;
        },
        isActive: function(name) {
            if(_(name).isArray()) {
                return _(name).find(function(n) {
                    return $route.current.name === n;
                }) ? true : false;
            }
            return $route.current.name === name;
        },
        redirectTo: function(name, params, options) {
            options = options || {};
            if(options.replace) {
                $location.replace();
            }
            $location.path(this.getPath(name, params), options.reload);
            lastParams = params;
        },
        getParams: function() {
            return lastParams || {};
        }
    }
});