var m = angular.module('utils');

m.factory('equalCache', function() {
    return function(process) {
        var cached = null;
        return function() {
            var result = process(arguments);
            if(_(result).isEqual(angular.copy(cached))) {
                return cached;
            }
            return cached = result;
        }
    }
});