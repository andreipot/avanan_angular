var m = angular.module('utils');
m.factory('ref', [function() {
    return function(init) {
        var value = init;
        return function(newValue) {
            if(!_(newValue).isUndefined()) {
                value = newValue;
            }
            return value;
        }
    }
}]);