var m = angular.module('user-management');

m.controller('UserListController', function($scope, $q, tablePaginationHelper, routeHelper, path, avananUserDao) {
    function isActionActive() {
        var selected = $scope.tableHelper.getSelectedIds();
        return selected.length === 1;
    }

    $scope.userListPanel = {
        title: "Users",
        'class': 'light white-body white-header'
    };

    $scope.tableOptions = {
        pagesAround: 2,
        pageSize: 10,
        pagination: {
            page: 1,
            ordering: {},
            filter: ''
        },
        actions: [{
            label: 'Delete',
            active: function() {
                return $scope.tableHelper.getTotal() > 1 && isActionActive();
            },
            execute: function() {
                var selected = $scope.tableHelper.getSelectedIds();
                avananUserDao.remove(selected[0]).then(function() {
                    $scope.tableHelper.reload();
                });
            }
        }, {
            label: 'Create New User',
            display: 'button',
            execute: function() {
                routeHelper.redirectTo('user-management-create');
            }
        }]
    };

    path('user-management').ctrl('user-list').json('list-conf').retrieve().then(function(response) {
        $scope.options = response.data;

        $scope.tableHelper = tablePaginationHelper($scope, $scope.tableOptions, $scope.options, function(args) {
            return avananUserDao.list(args.offset / args.limit + 1, args.limit, $scope.options.columns);
        }, function(tableModel) {
            $scope.tableModel = tableModel;
        });
    });
});