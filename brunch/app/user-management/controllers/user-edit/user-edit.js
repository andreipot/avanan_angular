var m = angular.module('user-management');

m.controller('UserEditController', function($scope, avananUserDao, routeHelper, modals, $routeParams) {
    var userId = $routeParams.id;
    $scope.isNew = _(userId).isUndefined();

    if($scope.isNew) {
        $scope.model = {
            first_name: '',
            last_name: '',
            email: '',
            description: ''
        };
    } else {
        avananUserDao.retrieve(userId).then(function(user) {
            $scope.model = user.data;
        });
    }

    $scope.userCreatePanel = {
        title: "User details",
        "class": "avanan white-body"
    };

    $scope.save = function() {
        if(!/.+@.+\..+/.test($scope.model.email)) {
            modals.alert('Incorrect email');
            return;
        }
        var promise;
        if($scope.isNew) {
            promise = avananUserDao.create($scope.model);
        } else {
            promise = avananUserDao.update(userId, $scope.model);
        }

        promise.then(function () {
            routeHelper.redirectTo("user-management");
        }, function (error) {
            modals.alert(error.data.message);
        });
    }
});