var m = angular.module('settings');

m.controller('SettingsController', function($scope, avananUserDao, modals) {
    $scope.changePasswordModel = {
        oldPassword: '',
        newPassword: '',
        repeatPassword: ''
    };

    $scope.error = {};
    $scope.changePassword = function() {
        $scope.error = {};
        if($scope.changePasswordModel.oldPassword.length == 0) {
            $scope.error.oldPassword = 'Password should not be empty';
            return;
        }
        if($scope.changePasswordModel.newPassword.length == 0) {
            $scope.error.newPassword = 'Password should not be empty';
            return;
        }
        if($scope.changePasswordModel.newPassword != $scope.changePasswordModel.repeatPassword) {
            $scope.error.repeatPassword = 'Passwords are not same';
            return;
        }
        avananUserDao.changePassword($scope.changePasswordModel.oldPassword, $scope.changePasswordModel.newPassword).then(function() {
            modals.alert('Password changes successfully');
        }, function(error) {
            $scope.error.oldPassword = error.data.message;
            $scope.error.newPassword = error.data.message;
        })
    };
});