var m = angular.module('dashboard');

m.controller('DashboardController', function($scope, widgetPolling, sizeClasses, path, $routeParams, routeHelper, modulesManager) {
    modulesManager.reloadModules().then(function() {
        var module = $routeParams.module;
        if(_(module).isUndefined() || module != modulesManager.currentModule()) {
            routeHelper.redirectTo('dashboard', {
                module: modulesManager.currentModule()
            });
            return;
        }
        widgetPolling.updateWidgets(path('dashboard').ctrl('dashboard').json(module + '/dashboard').path);
        $scope.widgetPolling = widgetPolling;
        $scope.getSizeClasses = sizeClasses;
        $scope.wrapperClass = 'dashboard';
    });
});