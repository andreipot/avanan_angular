var m = angular.module('dashboard');

m.factory('avInfoBoxLogic', function (viewerTypeDao, widgetSettings, $timeout, $q, path) {
    var timebackWidgetUuid = "dashboard-info-boxes-timeback";
    return function ($scope, logic, reinitLogic) {
        if (!_($scope.model.lines).isArray()) {
            $scope.model.lines = [{
                text: '0',
                'class': {
                    'count-odometer': true,
                    'zero-opacity': true
                }
            }, {
                text: '0',
                'class': {
                    'count': true,
                    'right': true,
                    'zero-opacity': true
                }
            }, {
                text: '#' + JSON.stringify({
                    display: 'img',
                    path: path('app-store').img('check.png'),
                    tooltip: 'No threats found'
                }),
                'class': {
                    'av-check': true,
                    'display-none': true
                }
            }, {
                text: logic.first.label,
                'class': {
                    'message': true7
                }
            }, {
                text: logic.second.label,
                'class': {
                    'message': true,
                    'right': true
                }
            }];
        }
        var after = widgetSettings.param(timebackWidgetUuid, 'data') || 0;
        $q.all([
            viewerTypeDao.count(logic.first.id, {
                after: after
            }),
            viewerTypeDao.count(logic.second.id, {
                after: after
            })
        ]).then(function (responses) {
            $scope.model.lines = $.extend(true, $scope.model.lines || [], [{
                text: '#' + JSON.stringify({
                    display: 'odometer',
                    text: responses[0].data
                })
            }, {
                text: '#' + JSON.stringify({
                    text: responses[1].data,
                    type: 'viewer',
                    id: logic.second.id,
                    qs: {
                        after: after
                    }
                })
            }, {}, {}, {}]);

            $timeout(function() {
                _($scope.model.lines).each(function(line) {
                    line.class['zero-opacity'] = false;
                });

                var showCheck = responses[1].data == 0;
                $scope.model.lines[1].class['display-none'] = showCheck;
                $scope.model.lines[2].class['display-none'] = !showCheck;
            });
        });

        var dataWatch = $scope.$watch(function () {
            return widgetSettings.param(timebackWidgetUuid, 'data');
        }, function (newValue, prevValue) {
            if (newValue != prevValue) {
                reinitLogic();
            }
        });

        return function () {
            dataWatch();
        }
    }
});