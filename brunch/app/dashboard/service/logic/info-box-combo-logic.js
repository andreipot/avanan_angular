var m = angular.module('dashboard');

m.factory('infoBoxComboLogic', function(widgetSettings) {
    return function($scope, logic) {
        var dataWatch = $scope.$watch('model.data', function(newVal, oldVal) {
            if(newVal != oldVal || widgetSettings.param($scope.model.uuid, 'data') != newVal) {
                widgetSettings.param($scope.model.uuid, 'data', $scope.model.data);
            }
        });

        return function() {
            dataWatch();
        }
    }
});