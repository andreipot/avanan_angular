var m = angular.module('dashboard');

m.factory('policiesPlotLogic', function(widgetSettings, policyDao, modulesManager) {
    return function($scope, logic) {
        $scope.discoveryData = $scope.discoveryData || [];

        function filterData() {
            var params = widgetSettings.params($scope.model.uuid);
            var filtered = _($scope.discoveryData).filter(function(series, idx) {
                return _(params[idx]).isUndefined() ? true : params[idx];
            });
            return _(filtered).map(function(series, idx) {
                return {
                    label: series.label,
                    data: series.data
                };
            });
        }

        policyDao.discovery(logic.days, modulesManager.currentModule()).then(function(response) {
            $scope.discoveryData = response.data;
            $scope.model.data = filterData();
            var params = widgetSettings.params($scope.model.uuid);
            $scope.model.wrapper.filters = [];
            _($scope.discoveryData).each(function (policy, idx) {
                $scope.model.wrapper.filters.push({
                    "type": "flag",
                    "state": _(params[idx]).isUndefined() ? true : params[idx],
                    "text": policy.label,
                    "value": idx,
                    "local": true
                });
            });
        });

        var paramsWatch = $scope.$watch(function() {
            return widgetSettings.params($scope.model.uuid);
        }, function(params) {
            $scope.model.data = filterData();
        }, true);

        return function() {
            paramsWatch();
        };
    }
});