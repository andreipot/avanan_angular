var m = angular.module('dashboard');

m.factory('infoBoxLogic', function (viewerTypeDao, widgetSettings, $timeout) {
    var timebackWidgetUuid = "dashboard-info-boxes-timeback";
    return function ($scope, logic, reinitLogic) {
        if (!_($scope.model.lines).isArray()) {
            $scope.model.lines = [{
                text: '0',
                'class': 'count zero-opacity'
            }, {
                text: logic.title,
                'class': 'message'
            }];
        }
        var after = widgetSettings.param(timebackWidgetUuid, 'data') || 0;
        viewerTypeDao.count(logic.id, {
            after: after
        }).then(function (countResponse) {
            $scope.model.lines = $.extend(true, $scope.model.lines || [], [{
                text: '#' + JSON.stringify({
                    text: countResponse.data,
                    type: 'viewer',
                    id: logic.id,
                    qs: {
                        after: after
                    }
                })
            }, {
                text: logic.title
            }]);

            if(_($scope.model.lines).first().class.indexOf('zero-opacity') != -1) {
                $timeout(function () {
                    _($scope.model.lines).first().class = 'count';
                });
            }
        });

        var dataWatch = $scope.$watch(function () {
            return widgetSettings.param(timebackWidgetUuid, 'data');
        }, function (newValue, prevValue) {
            if (newValue != prevValue) {
                reinitLogic();
            }
        });

        return function () {
            dataWatch();
        }
    }
});