var m = angular.module('dashboard');

m.factory('worldAccessMapLogic', function(ipDao, widgetPolling, eventsMapLogicCore) {
    return function($scope, logic) {
        ipDao.worldAccess().then(function(response) {
            $scope.model.data = response.data;
            if (_($scope.model.updateData).isFunction()) {
                $scope.model.updateData();
            }
        });
    };
});