var m = angular.module('dashboard');

m.factory('doubleInfoBoxLogic', function (viewerTypeDao, widgetSettings, $timeout, $q) {
    var timebackWidgetUuid = "dashboard-info-boxes-timeback";
    return function ($scope, logic, reinitLogic) {
        if (!_($scope.model.lines).isArray()) {
            $scope.model.lines = [{
                text: '0',
                'class': 'count zero-opacity'
            }, {
                text: '0',
                'class': 'count right zero-opacity'
            }, {
                text: logic.first.label,
                'class': 'message'
            }, {
                text: logic.second.label,
                'class': 'message right'
            }];
        }
        var after = widgetSettings.param(timebackWidgetUuid, 'data') || 0;
        $q.all([
            viewerTypeDao.count(logic.first.id, {
                after: after
            }),
            viewerTypeDao.count(logic.second.id, {
                after: after
            })
        ]).then(function (responses) {
            $scope.model.lines = $.extend(true, $scope.model.lines || [], [{
                text: '#' + JSON.stringify({
                    text: responses[0].data,
                    type: 'viewer',
                    id: logic.first.id,
                    qs: {
                        after: after
                    }
                })
            }, {
                text: '#' + JSON.stringify({
                    text: responses[1].data,
                    type: 'viewer',
                    id: logic.second.id,
                    qs: {
                        after: after
                    }
                })
            }, {}, {}]);

            $timeout(function() {
                _($scope.model.lines).each(function(line) {
                    line.class = line.class.split(' zero-opacity').join('');
                });
            });
        });

        var dataWatch = $scope.$watch(function () {
            return widgetSettings.param(timebackWidgetUuid, 'data');
        }, function (newValue, prevValue) {
            if (newValue != prevValue) {
                reinitLogic();
            }
        });

        return function () {
            dataWatch();
        }
    }
});