var m = angular.module('dashboard');

m.factory('eventsListLogic', function(eventsDao, eventsListLogicCore, widgetSettings) {
    return function ($scope, logic) {
        return eventsListLogicCore($scope, logic, {
            retrieveTypes: function() {
                return eventsDao.retrieveTypes({
                    drive: true,
                    login: true
                });
            },
            retrieve: function(after) {
                return eventsDao.retrieve($scope.model.columns, after);
            },
            timeColumnName: logic.timeColumnName
        });
    }
});