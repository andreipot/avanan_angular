var m = angular.module('dashboard');

m.factory('eventsMapLogic', function(eventsDao, widgetPolling, eventsMapLogicCore) {
    return function($scope, logic) {
        return eventsMapLogicCore($scope, logic, {
            retrieve: function(after) {
                return eventsDao.retrieveMap($scope.model.fields, after);
            },
            timeColumnName: logic.timeColumnName
        });
    }
});