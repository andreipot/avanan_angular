var m = angular.module('dashboard');

m.factory('policyDiscoveryConverter', function($q) {
    return function(input, args) {
        var deferred = $q.defer();
        var result = {};

        function fillGaps(discovers) {
            var result = [];
            var current = moment().hours(12).minutes(0).seconds(0).milliseconds(0).add(-args.after + 1, 'days');
            for(var i = 0; i < args.after; i++) {
                var date = current.format('YYYY-MM-DD');
                result.push([parseInt(current.format('x')), discovers[date] || 0]);
                current.add(1, 'days');
            }
            return result;
        }

        var rows = input.data.rows || [];

        var discovers = {};
        _(rows).each(function(row) {
            discovers[row.time] = row.count;
        });

        result.last_match_time = _(rows).reduce(function(memo, row) {
            if(!_(row.last_day_match).isUndefined() && (_(memo).isNull() || moment(row.last_day_match).isAfter(memo))) {
                return row.last_day_match;
            }
            return memo;
        }, null);

        result.data = fillGaps(discovers);

        deferred.resolve($.extend({}, input, {
            data: result
        }));

        return deferred.promise;
    }
});