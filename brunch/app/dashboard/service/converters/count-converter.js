var m = angular.module('dashboard');

m.factory('countConverter', function($q) {
    return function(input, args) {
        var deferred = $q.defer();
        var count = input.data.rows[0].count;

        var result = {
            "lines": [
                {
                    "text": count,
                    "class": "count"
                },
                {
                    "text": args.title,
                    "class": "message"
                }
            ],
            "updateTick": "10000"
        };

        deferred.resolve($.extend({}, input, {
            data: result
        }));

        return deferred.promise;
    }
});