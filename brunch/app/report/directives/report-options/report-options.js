var m = angular.module('report');

m.directive('reportOptions', function(path, tablePaginationHelper, reportDao, columnsHelper, $q, folderDao, eventsDao, $injector) {
    return {
        restrict: 'A',
        replace: false,
        templateUrl: path('report').directive('report-options').template(),
        scope: {
            model: '=reportOptions'
        },
        link: function($scope) {
            $scope.internalModel = {
                editableTemplate: false
            };

            $scope.promiseResult = {};

            if(_($scope.model.template).isUndefined()) {
                $scope.internalModel.editableTemplate = true;
                $scope.tableOptions = {
                    pagesAround: 2,
                    pageSize: 20,
                    pagination: {
                        page: 1,
                        ordering: {},
                        filter: '',
                        disableBottom: true
                    }
                };

                path('report').directive('report-options').json('template-list-conf').retrieve().then(function(response) {
                    $scope.options = response.data;
                    $scope.tableHelper = tablePaginationHelper($scope, $scope.tableOptions, $scope.options, function (args) {
                        return reportDao.templates(response.data.columns);
                    }, function (tableModel) {
                        $scope.tableModel = tableModel;
                    });

                    $scope.$watchCollection('tableHelper.getSelected()', function(selected) {
                        if (_(selected).isUndefined() || selected.length == 0) {
                            return;
                        }

                        var templateIdx = columnsHelper.getIdxById(response.data.columns, null);
                        $scope.model.template = $.extend(true, {}, _(selected).first()[templateIdx]).originalText;
                        $scope.model.params = _($scope.model.template.params).chain().map(function(param) {
                            return [param.id, param.value];
                        }).object().value();

                        generateQuestions();

                        $scope.tableHelper.clearSelection();
                    });
                });
            } else {
                generateQuestions();
            }

            function generateQuestions() {
                $scope.internalModel.questions = _($scope.model.template.params).chain().filter(function(param) {
                    return !_(param.label).isUndefined();
                }).map(function(param) {
                    if(!_(param.promises).isUndefined()){
                        var promiseHandler = $injector.get(param.promises);
                        if(!_(promiseHandler).isUndefined()){
                        $q.when(promiseHandler()).then(function(response){
                                $scope.promiseResult = $.extend($scope.promiseResult,{ eventTypesVariants: response.data });
                            });
                        }
                    }
                    return {
                        id: param.id,
                        label: param.label,
                        type: {
                            'TEXT': 'string',
                            'EVENTTYPELIST':'event-type-list',
                            'DATETIME':'datetime',
                            'INT': 'integer',
                            'BOOLEAN': 'boolean',
                            'DIRECTORY': 'tree',
                            'FILE': 'tree'
                        }[param.type],
                        options: {
                            retrieveContent: function(id) {
                                if(_(id).isUndefined()) {
                                    return $q.when({
                                        data:{
                                            rows: [{
                                                label: '#' + JSON.stringify({
                                                    display: 'img',
                                                    path: path('common').img('mime/folder.svg'),
                                                    'class': 'icon-16px'
                                                }) + '/',
                                                entity_id: '',
                                                expandable: true,
                                                primaryKey: 'entity_id'
                                            }]
                                        }
                                    });
                                }
                                return folderDao.content(id, param.type == 'FILE');
                            },
                            primaryField: 'entity_id',
                            labelField: 'label',
                            expandableField: 'expandable'
                        },
                        popup: {
                            title: param.label
                        }
                    };
                }).value();
            }

            $scope.reset = function() {
                if($scope.internalModel.editableTemplate) {
                    delete $scope.model.template;
                    delete $scope.model.params;
                }
            };
        }
    }
});