var m = angular.module('report');

m.controller('ReportController', function($q, $scope, reportDao, objectTypeDao, $routeParams, modals, tablePaginationHelper, tableRemediationActionsBuilder) {
    var id = $routeParams.id;

    $q.all([
        reportDao.retrieve(id),
        objectTypeDao.retrieve()
    ]).then(function(responses) {
        $scope.model = responses[0].data;
        $scope.sources = responses[1].data.sources;

        $scope.reportOptionsPanel = {
            title: "Report options",
            'class': 'light white-body white-header no-margin'
        };

        $scope.showOptions = false;
        $scope.resultsPanel = {
            title: 'Results',
            'class': 'light white-body white-header no-margin',
            leftButton: {
                label: function() {
                    if($scope.showOptions) {
                        return 'Hide options'
                    }
                    return 'Show options'
                },
                'class': 'btn-blue',
                onclick: function() {
                    $scope.showOptions = !$scope.showOptions;
                    $scope.splitterOptions.disabled = !$scope.showOptions;
                }
            }
        };

        $scope.splitterOptions = {
            orientation: 'vertical',
            position: '50%',
            limit: 150,
            disabled: !$scope.showOptions
        };

        $scope.update = function() {
            if(_($scope.model.data.name).isUndefined()) {
                modals.alert('You must specify report name before you can update');
                return;
            }

            reportDao.update(id, $scope.model).then(function() {
                $scope.tableHelper.clearSelection();
                $scope.tableHelper.reload();
            });
        };

        $scope.tableOptions = {
            pagesAround: 2,
            pageSize: 20,
            pagination: {
                page: 1,
                ordering: {},
                filter: '',
                disableTop: true
            },
                actions: [{
                label: 'Export as pdf',
                display: 'button',
                execute: function() {
                    var win = window.open('', '_blank');
                    $scope.tableHelper.makePdf($scope.model.data.name).then(function(pdf) {
                        pdf.getDataUrl(function(result) {
                            win.location.href = result;
                        });
                    }, function() {
                        win.close();
                    });
                }
            }]
        };

        tableRemediationActionsBuilder($scope.tableOptions, responses[1].data.actionTypes, $scope.sources, $scope.model.data.template.saas, function() {
            return $scope.tableHelper.getSelectedIds();
        });

        $scope.options = $scope.model.data.template;
        $scope.tableHelper = tablePaginationHelper($scope, $scope.tableOptions, $scope.options, function(args) {
            var params = {};
            _($scope.model.data.params).each(function(value, key) {
                if(_(value).isObject()) {
                    if(_(value).isArray()) {
                        params[key] =value.join(",");
                    }else {
                        params[key] = value[value.primaryKey];
                    }
                }else {
                    params[key] = value;
                }
            });
            return reportDao.report($scope.options.columns, $scope.options.url, params)
                .pagination(args.offset, args.limit)
                .filter(args.filter)
                .order(args.ordering.columnName, args.ordering.order, args.ordering.nulls);
        }, function(tableModel) {
            $scope.tableModel = tableModel;
        });
    });
});