var m = angular.module('report');

m.controller('ReportCreateController', function($scope, reportDao, routeHelper, modals) {
    $scope.model = {
        data: {}
    };

    $scope.reportOptionsPanel = {
        title: "Report options",
        'class': 'light white-body white-header no-margin'
    };

    $scope.isSaveVisible = function() {
        return !_($scope.model.data.template).isUndefined();
    };

    $scope.save = function() {
        if(_($scope.model.data.name).isUndefined()) {
            modals.alert('You must specify report name before you can continue');
            return;
        }

        reportDao.save($scope.model).then(function(response) {
            var id = response.data.id;
            routeHelper.redirectTo('report-edit', {
                id: id
            });
        });
    };
});