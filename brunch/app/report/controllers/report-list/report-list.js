var m = angular.module('report');

m.controller('ReportListController', function($scope, tablePaginationHelper, reportDao, routeHelper, $q, path) {
    $scope.reportListPanel = {
        title: "Reports",
        'class': 'light white-body white-header'
    };

    $scope.tableOptions = {
        pagesAround: 2,
        pageSize: 20,
        pagination: {
            page: 1,
            ordering: {},
            filter: '',
            disableTop: true
        },
        actions: [
            {
                label: 'Delete',
                active: function() {
                    return $scope.tableHelper.getSelected().length > 0;
                },
                execute: function() {
                    $q.all(_($scope.tableHelper.getSelectedIds()).map(function(report) {
                        return reportDao.remove(report)
                    })).then(function() {
                        $scope.tableHelper.clearSelection();
                        $scope.tableHelper.reload();
                    });
                }
            }, {
                label: 'Add new report',
                display: 'button',
                execute: function() {
                    routeHelper.redirectTo('report-create');
                }
            }
        ]
    };

    path('report').ctrl('report-list').json('list-conf').retrieve().then(function(response) {
        $scope.options = response.data;
        $scope.tableHelper = tablePaginationHelper($scope, $scope.tableOptions, $scope.options, function(args) {
            return reportDao.list($scope.options.columns);
        }, function(tableModel) {
            $scope.tableModel = tableModel;
        });
    });
});