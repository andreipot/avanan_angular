var m = angular.module('viewer');

m.factory('addMaxApplicationsRiskConverter', function($q) {
    return function(input) {
        var maxRisk = 0;
        _(input.data.rows).each(function(row) {
            var totalRisk = _(row.scopes_risk).reduce(function(memo, val) {
                return memo + val;
            }, 0);
            maxRisk = Math.max(maxRisk, totalRisk);
        });
        _(input.data.rows).each(function(row) {
            row.max_risk = maxRisk;
        });
        return $q.when(input);
    };
});