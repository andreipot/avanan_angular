var m = angular.module('viewer');

m.controller('ViewerController', function($scope, $routeParams, viewerTypeDao, objectTypeDao, tablePaginationHelper, $q, addMaxApplicationsRiskConverter, routeHelper) {
    var type = $routeParams.id;

    viewerTypeDao.types().then(function(response) {
        var metaInfo = response.data[type];
        var dataType = metaInfo.type || 'viewer';
        if(dataType == 'viewer') {
            initViewer();
        } else if(dataType == 'policy') {
            routeHelper.redirectTo(metaInfo.policyViewer, {
                internalName: metaInfo.internalName
            });
        }
    });

    function initViewer() {
        $q.all([
            objectTypeDao.retrieve(),
            viewerTypeDao.listColumns(type)
        ]).then(function (responses) {
            $scope.actionTypes = responses[0].data.actionTypes;
            $scope.options = responses[1].data;

            $scope.viewerPanel = {
                title: 'Loading...',
                'class': 'avanan white-body'
            };

            $scope.tableOptions = {
                pagesAround: 2,
                pageSize: 20,
                pagination: {
                    page: 1,
                    ordering: {},
                    filter: ''
                }
            };

            if (_($scope.options.entityType).isString()) {
                $scope.tableOptions.actions = [];
                _($scope.actionTypes[$scope.options.entityType]).each(function (action) {
                    $scope.tableOptions.actions.push({
                        label: action.label,
                        execute: function () {
                        }
                    });
                });
            }

            $scope.tableHelper = tablePaginationHelper($scope, $scope.tableOptions, $scope.options, function (args) {
                return viewerTypeDao.list($scope.options.columns, type, _($routeParams).omit('id'), {
                    addMaxApplicationsRiskConverter: addMaxApplicationsRiskConverter
                }).pagination(args.offset, args.limit)
                    .filter(args.filter)
                    .order(args.ordering.columnName, args.ordering.order);
            }, function (tableModel) {
                $scope.tableModel = tableModel;
                $scope.viewerPanel.title = $scope.tableHelper.getTotal() + ' ' + $scope.options.options.title;
            });
        });
    }
});