var m = angular.module('dao');

m.factory('eventsDao', function(http, defaultListConverter, defaultMapConverter, $q, sqlQueries, arrayValueConverter,eventTypeListSelectConverter) {
    var eventTags = {};
    function retrieveEvents(after, userId, ipId) {
        return http.get(sqlQueries('user_events')).params({
            after: after,
            user_id: userId,
            ip_id: _(ipId).isUndefined() ? undefined : ipId + '/%%'
        }).cache('eventsDao.retrieveEvents-' + after + '-' + (userId || ''), 5000);
    }
    return {
        retrieve: function(columns, after, userId, ipId) {
            return retrieveEvents(after, userId, ipId)
                .converter(defaultListConverter, function() {
                    return {
                        columns: columns,
                        converters: {
                            eventType: function(text) {
                                return text;
                            }
                        }
                    }
                });
        },
        retrieveMap: function(fields, after, userId) {
            return retrieveEvents(after, userId).converter(defaultMapConverter, {
                fields: fields
            });
        },
        retrieveTypes: function(params) {
            return http.get(sqlQueries('event_types')).params(params).converter(arrayValueConverter, {
                name: 'name'
            });
        },
        retrieveTypesList: function(params) {
            return http.get(sqlQueries('event_types')).params(params).converter(eventTypeListSelectConverter, {
                name: 'name'
            });
        },
        retrieveTags: function() {
            return $q.when({
                data: eventTags
            })
        }
    };
});