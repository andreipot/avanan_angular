var m = angular.module('dao');

m.factory('worldAccessMapConverter', function ($q, routeHelper) {
    return function (input, args) {
        var deferred = $q.defer();

        var rows = input.data.rows || [];

        var geos = {};

        _(rows).each(function(row) {
            var key = row.geo_lat + ';' + row.geo_lon;
            geos[key] = geos[key] || _(row).chain().omit('entity_id').defaults({
                totalUsers: 0,
                rows: []
            }).value();
            geos[key].rows.push(row);
            geos[key].totalUsers += row.users_count;
        });

        function usersCount(count) {
            if(count == 1) {
                return count + ' User';
            }
            return count + ' Users';
        }

        var startColor = parseInt('f35428', 16);
        var endColor = parseInt('f39c12', 16);
        var result = _(geos).chain().sortBy(function(geo) {
            return -geo.totalUsers;
        }).map(function(geo) {
            geo.rows = _(geo.rows).sortBy(function(row) {
                return -row.users_count;
            });
            return {
                latLng: [geo.geo_lat, geo.geo_lon],
                metadata: '<div class="map-label">' +
                '<table>' +
                _(geo.rows).map(function(row) {
                    return '<tr><td>' + row.entity_id + ':</td><td>' + usersCount(row.users_count) + '</td></tr>';
                }).join('') + _([
                        ['Country', geo.geo_country],
                        ['Region', geo.geo_region],
                        ['City', geo.geo_city]
                    ]).chain().filter(function(pair) {
                        return _(pair[1]).isString();
                    }).map(function(pair) {
                        return '<tr><td>' + pair[0] + ':</td><td>' + pair[1] + '</td></tr>'
                    }).value().join('') +
                    '</table>' +
                '</div>',
                style: {
                    r: Math.min(2 + geo.totalUsers, 12),
                    fill: '#' + (startColor + (endColor - startColor) * (Math.min(geo.totalUsers - 1, 10)) / 10).toString(16)
                },
                onClick: function() {
                    routeHelper.redirectTo('ip', {
                        id: _(geo.rows).first().entity_id
                    });
                }
            };
        }).value();

        deferred.resolve($.extend({}, input, {
            data: result
        }));

        return deferred.promise;
    }
});