var m = angular.module('dao');

m.factory('ipDao', function(http, defaultListConverter, sqlQueries, singleRowConverter, usersWithIpPerDayConverter, worldAccessMapConverter) {
    return {
        retrieve: function (ipId) {
            return http.get(sqlQueries('entity/geo')).params({
                entity_id: ipId
            }).converter(singleRowConverter);
        },
        usersWithIp: function(columns, ipId) {
            return http.get(sqlQueries('user_with_ip')).params({
                ip_id: ipId
            }).converter(defaultListConverter, {
                columns: columns
            });
        },
        usersWithIpPerDay: function(ipId, after) {
            return http.get(sqlQueries('user_with_ip_per_day')).params({
                ip_id: ipId,
                after: after
            }).converter(usersWithIpPerDayConverter, {
                after: after
            });
        },
        worldAccess: function() {
            return http.get(sqlQueries('ip_events')).converter(worldAccessMapConverter);
        }
    }
});