var m = angular.module('dao');

m.factory('dao', function($timeout, $injector) {
    var daoCache = {};

    var daoList = [
        'application',
        'avananUser',
        'common',
        'events',
        'file',
        'folder',
        'globalSearch',
        'ip',
        'modules',
        'objectType',
        'policy',
        'policyReport',
        'user',
        'viewerType'
    ];

    var result = {};
    _(daoList).each(function(dao) {
        result[dao] = function() {
            if(_(daoCache[dao]).isUndefined()) {
                daoCache[dao] = $injector.get(dao + 'Dao');
            }
            return daoCache[dao];
        }
    });
    return result;
});