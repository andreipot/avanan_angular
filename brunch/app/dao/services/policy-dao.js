var m = angular.module('dao');

m.factory('policyDao', function ($q, $http, http, defaultListConverter, policyDiscoveryConverter, path, rowsToObjectConverter, policyDiscoveriesConverter, sqlQueries, policyFindingsConverter, dao, modulesManager, singleRowConverter, rowsNameConverter, policyConverter, policySaveConverter, predefinedPolicyConverter) {
    var url = '/api/v1/policies/policy_catalog';
    var policyDao = {
        policyCatalog: function (saas, hidePaused, hideHidden) {
            return http.get(sqlQueries('policy_catalog')).params({
                saas: saas,
                hide_paused: hidePaused,
                hide_hidden: hideHidden
            });
        },
        list: function (columns, sources, hidePaused, hideHidden) {
            return policyDao.policyCatalog(undefined, hidePaused, hideHidden)
                .preFetch(policyDao.discovery(7, undefined, hidePaused), 'discovery')
                .preFetch(policyDao.findingsStatus(), 'findingsStatus')
                .dynamicConverter(function (preFetches) {
                    return function (input) {
                        var discovery = preFetches.discovery.data;
                        var rows = input.data.rows || [];

                        _(rows).each(function (row) {
                            var findingStatus = preFetches.findingsStatus.data[row.policy_id] || {};
                            row.entities_matches = findingStatus['Match'] || 0;
                            row.entities_unmatches = findingStatus['Unmatch'] || 0;
                            row.entities_pending = findingStatus['Pending'] || 0;
                            row.entities_total = row.entities_matches + row.entities_unmatches + row.entities_pending;
                            row.last_match_time = (_(discovery).find(function (p) {
                                return p.id == row.policy_id;
                            }) || {}).last_match_time;
                        });

                        return defaultListConverter(input, {
                            columns: columns,
                            converters: {
                                policyStatus: function (text) {
                                    return {
                                        "running": "Running",
                                        "pause": "Pause",
                                        "mark_for_deletion": "Marked for deletion"
                                    }[text];
                                },
                                policyName: function (text, columnId, columnsMap) {
                                    return '#' + JSON.stringify({
                                            display: 'link',
                                            'class': 'policy-name',
                                            type: 'policy-edit',
                                            id: columnsMap['policy_id'],
                                            text: text
                                        }) + '#' + JSON.stringify({
                                            display: 'text',
                                            'class': 'policy-description',
                                            text: columnsMap['policy_data.description']
                                        });
                                },
                                entityType: function (text) {
                                    if (_(sources[text]).isUndefined()) {
                                        return 'Unknown';
                                    }
                                    return sources[text].label;
                                },
                                entityTypeImage: function (text) {
                                    if (!_(text).isObject() || !_(text.saas).isString()) {
                                        return '';
                                    }
                                    return '#' + JSON.stringify({
                                            display: 'img',
                                            'class': 'entity-type-image',
                                            path: modulesManager.cloudApp(text.saas).img
                                        });
                                },
                                lastMatchConverter: function (text) {
                                    if (moment().add(-1, 'days').isBefore(text)) {
                                        return '#' + JSON.stringify({
                                                display: 'text',
                                                'class': 'new-event',
                                                text: "New Event"
                                            }) + text;
                                    } else {
                                        return text;
                                    }
                                },
                                trendConverter: function (text, columnId, columnsMap, args) {
                                    var discoveries = _(discovery).find(function (p) {
                                        return p.id == text;
                                    });
                                    if (_(discoveries).isUndefined()) {
                                        return '';
                                    }
                                    return '#' + JSON.stringify({
                                            display: 'flot',
                                            'class': 'findings-flot',
                                            flot: {
                                                data: [discoveries.data],
                                                flotOptions: args.flotOptions
                                            }
                                        });
                                }
                            }
                        });
                    }
                });
        },
        retrieve: function (id) {
            return policyDao.policyCatalog().params({
                policy_id: id
            }).converter(singleRowConverter)
                .converter(policyConverter);
        },
        retrieveByInternalName: function(internalName) {
            return policyDao.policyCatalog().params({
                internal_name: internalName
            }).converter(singleRowConverter)
                .converter(policyConverter);
        },
        create: function (data) {
            return http.post(url).body(data).bodyConverter(policySaveConverter).run();
        },
        update: function (id, data) {
            return http.put(url + '/' + id).body(data).bodyConverter(policySaveConverter).run();
        },
        recheck: function(id) {
            return http.post(url + '/' + id + '/recheck').run();
        },
        updateExclude: function (id, added, removed) {
            var promises = [];
            if (_(added).isArray() && added.length > 0) {
                promises.push($http.put(url + '/' + id + '/exclude', {
                    entity_id: added,
                    action: 'Add'
                }));
            }
            if (_(removed).isArray() && removed.length > 0) {
                promises.push($http.put(url + '/' + id + '/exclude', {
                    entity_id: removed,
                    action: 'Remove'
                }));
            }
            return $q.all(promises);
        },
        discovery: function (after, saas, hidePaused) {
            after--;
            return http.get(sqlQueries('policy_discovers')).params({
                after: after,
                saas: saas
            }).preFetch(policyDao.policyCatalog(saas, hidePaused), 'policyCatalog')
                .preFetch(policyDao.findingsStatus(), 'findingsStatus')
                .converter(policyDiscoveriesConverter, function (preFetches) {
                    return {
                        after: after,
                        policyCatalog: preFetches.policyCatalog,
                        findingsStatus: preFetches.findingsStatus
                    };
                });
        },
        findingsStatus: function (policyId) {
            var url = '/api/v1/policies/dynamic_policy_status_current_report';
            if(!_(policyId).isUndefined()) {
                url += '/' + policyId;
            }
            return http.get(url).converter(policyFindingsConverter, {
                policyId: policyId
            });
        },
        getTemplatesList: function (columns, sources) {
            return http.get('/api/v1/policies/policy_templates')
                .converter(rowsNameConverter, {
                    name: 'templates'
                })
                .converter(defaultListConverter, {
                    columns: columns,
                    converters: {
                        entityType: function (text) {
                            if (_(sources[text]).isUndefined()) {
                                return 'Unknown';
                            }
                            return sources[text].label;
                        }
                    }
                });
        },
        predefinedPolicy: function(saas, security) {
            return http.get('/api/v1/policy_matrix/' + saas + '/' + security).converter(predefinedPolicyConverter);
        },
        modulesMatrix: function() {
            return http.get('/api/v1/modules_matrix');
        }
    };
    return policyDao;
});