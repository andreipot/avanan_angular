var m = angular.module('dao');

m.factory('modulesDao', function($q, http, modulesConverter, appStoreConverter, cloudAppsConverter, securityMatrixConverter, scriptConverter, policyDao, cloudAppsDashboardConverter) {
    var modulesUrl = '/api/v1/modules';
    var appStoreOrder = {
        categorySort: 'appStoreOrder',
        moduleSort: 'app_store_order'
    };
    var securityStackOrder = {
        categorySort: 'securityStackOrder',
        moduleSort: 'security_stack_order'
    };
    return {
        appStore: function(filter) {
            return http.get(modulesUrl)
                .converter(modulesConverter, appStoreOrder)
                .converter(appStoreConverter, {
                filter: filter
            });
        },
        cloudApps: function() {
            return http.get(modulesUrl)
                .converter(modulesConverter, appStoreOrder)
                .converter(cloudAppsConverter);
        },
        securityMatrix: function() {
            return http.get(modulesUrl)
                .preFetch(policyDao.policyCatalog(), 'policyCatalog')
                .preFetch(policyDao.modulesMatrix(), 'modulesMatrix')
                .converter(modulesConverter, securityStackOrder)
                .converter(securityMatrixConverter, function(preFetches) {
                    return preFetches;
                });
        },
        cloudAppsDashboard: function() {
            return http.get(modulesUrl)
                .converter(modulesConverter, appStoreOrder)
                .converter(cloudAppsDashboardConverter);
        },
        saveConfig: function(moduleName, data) {
            return http.put('/api/v1/modules/' + moduleName + '/config').body(data).run();
        },
        operation: function(moduleName, operation) {
            return http.put('/api/v1/modules/' + moduleName + '/operation/' + operation).run();
        }
    };
});