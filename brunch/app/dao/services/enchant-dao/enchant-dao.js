var m = angular.module('dao');

m.factory('enchantDao', function(http, sqlQueries, defaultListConverter, mimeIcons, primaryKeyReorderConverter) {
    return {
        dataEntitiesByIds: function(columns, ids, actions, idsCounts) {
            return http.get(sqlQueries('viewer/data_entities')).params({
                entity_ids: ids.join(',')
            }).preFetch(mimeIcons(), 'mimeIcons')
                .converter(defaultListConverter, function(preFetches) {
                    return {
                        actions: actions,
                        mimeIcons: preFetches.mimeIcons,
                        columns: columns,
                        converters: {
                            subItemsConverter: function(text, columnId, columnsMap) {
                                if(columnsMap.entity_type == 'folder') {
                                    return idsCounts[columnsMap['entity_id']];
                                }
                                return '';
                            }
                        }
                    };
                }).converter(primaryKeyReorderConverter, {
                    ids: ids,
                    columns: columns,
                    primaryKeyColumn: 'entity_id'
                });
        },
        dataEntitiesByRootCause: function(columns, rootCause, actions) {
            return http.get(sqlQueries('viewer/data_entities_under_root_cause')).params({
                root_cause: rootCause
            }).preFetch(mimeIcons(), 'mimeIcons')
                .converter(defaultListConverter, function(preFetches) {
                    return {
                        actions: actions,
                        mimeIcons: preFetches.mimeIcons,
                        columns: columns,
                        converters: {
                            subItemsConverter: function(text, columnId, columnsMap) {
                                return '';
                            }
                        }
                    };
                });
        }
    };
});