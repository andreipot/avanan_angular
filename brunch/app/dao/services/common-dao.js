var m = angular.module('dao');

m.factory('commonDao', function(http, $q) {
    return {
        entityIdentifier: function(entities, entityType) {
            if(!entities.length) {
                return $q.when([]);
            }
            return http.put('/api/v1/entity_identifier').body(_(entities).map(function(entity) {
                return {
                    entity_id: entity,
                    entity_type: entityType
                };
            })).converter(function(response) {
                return $q.when(_(response.data).map(function(entity) {
                    return {
                        id: entity.entity_id,
                        label: entity.main_identifier_value
                    };
                }));
            });
        }
    }
});