var m = angular.module('dao');

m.factory('manualActionSaveConverter', function($q, actionTagsProcessor) {
    return function (input, args) {
        var deferred = $q.defer();

        actionTagsProcessor.generateTags(input.data, input.data.attributes);

        deferred.resolve(input);
        return deferred.promise;
    };
});