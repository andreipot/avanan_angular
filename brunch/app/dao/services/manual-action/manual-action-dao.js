var m = angular.module('dao');

m.factory('manualActionDao', function(http, manualActionSaveConverter) {
    return {
        create: function(entityType, ids, action) {
            if(!_(ids).isArray()) {
                ids = [ids];
            }
            return http.post('/api/v1/manual_actions').body({
                entity_ids: ids,
                entity_type: entityType,
                data: action,
                date_trigger: moment().format()
            }).bodyConverter(manualActionSaveConverter).run();
        }
    };
});