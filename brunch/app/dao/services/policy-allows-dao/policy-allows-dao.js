var m = angular.module('dao');

m.factory('policyAllowsDao', function(http, defaultListConverter) {
    var policyAllowsDao = {
        retrieve: function(columns, policyId) {
            return http.get('/api/v1/policies/allow').params({
                policy_id: policyId
            }).preFetch(policyAllowsDao.types('public_shared_files_allow'), 'allowsTypes')
                .converter(defaultListConverter, function(preFetches) {
                    return {
                        columns: columns,
                        converters: {
                            allowTypeConverter: function(text) {
                                return preFetches.allowsTypes.data[text].label;
                            }
                        }
                    };
                });
        },
        create: function(policyId, exceptionType, entityType, entityIds) {
            return http.post('/api/v1/policies/allow').body({
                policy_id: policyId,
                allow_name: exceptionType,
                entity_type: entityType,
                entity_ids: entityIds
            }).run();
        },
        remove: function(allowId) {
            return http.delete('/api/v1/policies/allow/' + allowId).run();
        },
        types: function(section) {
            return http.get('/api/v1/policies/allows').params({
                section: section
            });
        }
    };

    return policyAllowsDao;
});

