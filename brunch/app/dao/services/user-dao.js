var m = angular.module('dao');

m.factory('userDao', function(http, futureCache, userFileShareConverter, defaultListConverter, mimeIcons, sqlQueries, singleRowConverter) {
    return {
        retrieve: function(userId) {
            return http.get(sqlQueries('entity/user')).params({
                entity_id: userId
            }).converter(singleRowConverter);
        },
        fileShare: function(after, userId) {
            return http.get(sqlQueries('events_per_day')).params({
                after: after,
                user_id: userId,
                event_name: 'change_user_access',
                event_type: 'acl_change'
            }).converter(userFileShareConverter, {
                after: after
            });
        },
        policyFindings: function(columns, after, userId) {
            return http.get(sqlQueries('policy_findings')).params({
                after: after,
                entity_id: userId
            }).converter(defaultListConverter, {
                columns: columns
            });
        },
        applicationList: function(columns, userId) {
            return http.get(sqlQueries('user_applications')).params({
                user_id: userId
            }).converter(defaultListConverter, {
                columns: columns
            });
        },
        filesOwnedList: function(columns, userId) {
            return http.get(sqlQueries('user_files')).params({
                user_id: userId
            }).preFetch(mimeIcons(), 'mimeIcons')
                .converter(defaultListConverter, function(preFetches) {
                    return  {
                        mimeIcons: preFetches.mimeIcons,
                        columns: columns
                    };
                });
        },
        permissions: function(columns, userId) {
            return http.get(sqlQueries('user_permissions')).params({
                user_id: userId
            }).preFetch(mimeIcons(), 'mimeIcons')
                .converter(defaultListConverter, function(preFetches) {
                    return  {
                        mimeIcons: preFetches.mimeIcons,
                        columns: columns
                    };
                });
        },
        connections: function(columns, userId) {
            return http.get(sqlQueries('user_connected')).params({
                user_id: userId
            }).converter(defaultListConverter, {
                columns: columns
            });
        }
    };
});