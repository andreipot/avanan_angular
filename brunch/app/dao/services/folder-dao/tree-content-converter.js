var m = angular.module('dao');

m.factory('treeContentConverter', function($q) {
    return function (input, args) {
        var deferred = $q.defer();

        _(input.data.rows).each(function(row) {
            return $.extend(row, {
                label: '#' + JSON.stringify({
                    display: 'img',
                    path: args.mimeIcons(row.mimeType),
                    'class': 'icon-16px'
                }) + row.title,
                entity_id: row.entity_id,
                primaryKey: 'entity_id'
            });
        });

        deferred.resolve(input);
        return deferred.promise;
    };
});