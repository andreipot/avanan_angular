var m = angular.module('dao');

m.factory('folderDao', function (http, defaultListConverter, defaultMapConverter, $q, singleValueConverter, fileDao, sqlQueries, singleRowConverter, treeContentConverter, mimeIcons) {
    var folderDao = {
        retrieve: function (folderId) {
            return http.get(sqlQueries('entity/folder')).params({
                entity_id: folderId
            }).converter(singleRowConverter);
        },
        size: function (folderId) {
            return http.get(sqlQueries('folder_size')).params({
                folder_id: folderId
            }).converter(singleValueConverter, {
                name: 'sum'
            });
        },
        permissions: function (columns, folderId) {
            return http.get(sqlQueries('folder_permissions')).params({
                file_id: folderId
            }).order('natural_order', 'asc')
                .preFetch(folderDao.retrieve(folderId), 'folder')
                .converter(defaultListConverter, function(preFetches) {
                    return {
                        columns: columns,
                        entity: preFetches.folder
                    };
                });
        },
        usersRatio: function (folderId) {
            return fileDao.usersRatio(folderId);
        },
        content: function(folderId, withFiles) {
            return http.get(sqlQueries('folder_content')).params({
                entity_id: folderId || '',
                with_files: withFiles
            }).preFetch(mimeIcons(), 'mimeIcons')
                .converter(treeContentConverter, function(preFetches) {
                    return preFetches;
                });
        }
    };
    return folderDao;
});