var m = angular.module('dao');

m.factory('opswatScanDetailsConverter', function($q) {
    return function (input, args) {
        var deferred = $q.defer();

        var scanDetails = input.data.scan_details;

        var rows = [];
        if(_(scanDetails).isObject()) {
            _(scanDetails).each(function (value, key) {
                rows.push($.extend({
                    name: key
                }, value));
            });
        }

        deferred.resolve($.extend({}, input, {
            data: {
                rows: rows
            }
        }));

        return deferred.promise;
    }
});