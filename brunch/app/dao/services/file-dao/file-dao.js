var m = angular.module('dao');

m.factory('fileDao', function(http, defaultListConverter, defaultMapConverter, $q, countRowsConverter, sqlQueries, singleRowConverter, comodoScanDetailsConverter, opswatScanDetailsConverter, wildfireScanDetailsConverter, objectTypeDao, notEmptyConverter) {
    function retrieveEvents(after, fileId) {
        return http.get(sqlQueries('file_events')).params({
            file_id: fileId,
            after: after
        }).cache('futureCache.retrieveEvents-' + after + '-' + fileId, 5000);
    }
    var fileDao = {
        events: function(columns, after, fileId) {
            return retrieveEvents(after, fileId).converter(defaultListConverter, {
                columns: columns
            });
        },
        eventsMap: function(fields, after, fileId) {
            return retrieveEvents(after, fileId).converter(defaultMapConverter, {
                fields: fields
            });
        },
        retrieve: function(fileId) {
            return http.get(sqlQueries('entity/file')).params({
                entity_id: fileId
            }).converter(singleRowConverter)
                .cache('fileDao.retrieve-' + fileId, 5000);
        },
        permissions: function(columns, fileId) {
            return http.get(sqlQueries('file_permissions')).params({
                file_id: fileId
            }).order('natural_order', 'asc')
                .preFetch(fileDao.retrieve(fileId), 'file')
                .converter(defaultListConverter, function(preFetches) {
                    return {
                        columns: columns,
                        entity: preFetches.file
                    };
                });
        },
        usersRatio: function(fileId) {
            return http.get(sqlQueries('file_permissions')).params({
                file_id: fileId
            }).converter(countRowsConverter, {
                field: 'is_domain'
            });
        },
        types: function(userId) {
            return http.get(sqlQueries('file_shared_types_count')).params({
                user_id: userId
            });
        },
        externalUserTypes: function(userId) {
            return http.get(sqlQueries('user_file_shared_types_count')).params({
                user_id: userId
            });
        },
        opswatScanDetails: function(columns, fileId) {
            return http.get(sqlQueries('scan_details/opswat')).params({
                file_id: fileId
            }).converter(notEmptyConverter)
                .converter(singleRowConverter)
                .converter(opswatScanDetailsConverter)
                .converter(defaultListConverter, {
                columns: columns
            });
        },
        comodoScanDetails: function(columns, fileId) {
            return http.get(sqlQueries('scan_details/comodo')).params({
                file_id: fileId
            }).converter(notEmptyConverter)
                .converter(singleRowConverter)
                .converter(comodoScanDetailsConverter)
                .converter(defaultListConverter, {
                columns: columns
            });
        },
        wildfireScanDetails: function(fileId) {
            return http.get(sqlQueries('scan_details/wildfire')).params({
                file_id: fileId
            }).preFetch(objectTypeDao.retrieve(), 'properties')
                .converter(notEmptyConverter)
                .converter(singleRowConverter)
                .converter(wildfireScanDetailsConverter, function(preFetches) {
                    return {
                        properties: preFetches.properties
                    };
                });
        }
    };

    return fileDao;
});