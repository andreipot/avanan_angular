var m = angular.module('dao');

m.factory('wildfireScanDetailsConverter', function ($q) {
    function openWildfireReport() {
        window.open('/api/v1/wildfire/pdf_report/' + this.sha256, '_blank');
    }

    return function (input, args) {
        var deferred = $q.defer();

        var scanDetails = input.data;

        var scanResultsVariants = args.properties.data.allSources['wildfire_scan.result'].variants;

        var result = {
            data: [{
                key: 'File format',
                value: scanDetails.file_type_description
            }, {
                key: 'Scan status',
                value: scanDetails.status
            }, {
                key: 'Scan result',
                value: (_(scanResultsVariants).find(function(variant) {
                    return variant.id == scanDetails.result;
                }) || {
                    label: scanDetails.result
                }).label
            }, {
                button: {
                    label: 'Download report',
                    'class': 'btn btn-blue',
                    sha256: scanDetails.sha256,
                    onClick: openWildfireReport
                }
            }]
        };

        deferred.resolve($.extend({}, input, {
            data: result
        }));

        return deferred.promise;
    }
});