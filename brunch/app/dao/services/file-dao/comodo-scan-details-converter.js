var m = angular.module('dao');

m.factory('comodoScanDetailsConverter', function($q) {
    return function (input, args) {
        var deferred = $q.defer();

        var scanDetails = input.data.scan_details;

        var rows = [];
        if(_(scanDetails).isObject()) {
            _(scanDetails.matching_detail).each(function (m) {
                _(m.matching_details).each(function (d, idx) {
                    rows.push({
                        matcher: m.matcher,
                        index: idx,
                        pattern: d.pattern,
                        count: d.count
                    });
                });
            });
        }

        deferred.resolve($.extend({}, input, {
            data: {
                rows: rows
            }
        }));

        return deferred.promise;
    }
});