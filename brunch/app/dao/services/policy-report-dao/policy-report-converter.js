var m = angular.module('dao');

m.factory('policyReportConverter', function ($q, defaultListConverter, $filter, path) {
    var root = path('policy');

    return function(input, args) {
        var policy = input.data.policy;
        var table = input.data.table;
        var rows = input.data.rows;

        (args.stateCallback || _.noop)((args.findings.Match || 0) + (args.findings.Unmatch || 0), input.data.total_entities);

        var colIdToId = {};
        var colValueMap = {};

        var columns = _(table.schema).chain().sortBy(function(col, colId) {
            col.uniqueId = colId;
            colIdToId[colId] = col.uniqueId;
            return col.idx || 0;
        }).map(function(col) {
            var valueMap = {};
            if(col.show_in_policy_table) {
                var propId;
                if (col.field_type == 'entity_id') {
                    propId = col.entity_type + '.' + col.prop_name;
                } else if(col.field_type == 'value') {
                    propId = col.name
                }
                if(_(propId).isString()) {
                    var propIdEnd = _(propId.split('.')).last(2).join('.');
                    var prop = args.properties[propIdEnd] || {};
                    _(prop.variants).each(function(opt) {
                        if(_(opt.icon).isUndefined()) {
                            valueMap[opt.id] = opt.label;
                        } else if(_(opt.icon).isNull()) {
                            valueMap[opt.id] = '';
                        } else {
                            valueMap[opt.id] = '#' + JSON.stringify({
                                display: 'img',
                                path: root.img([
                                    col.module_name,
                                    col.entity_type,
                                    col.prop_name,
                                    opt.icon
                                ].join('/')),
                                tooltip: {
                                    content: opt.label,
                                    position: {
                                        my: 'bottom right',
                                        at: 'top center'
                                    }
                                }
                            });
                        }
                    });
                    col.mainIdentifier = prop.mainIdentifier;
                }
            }
            colValueMap[col.uniqueId] = valueMap;

            function hrefConverter(type, module, column) {
                return function(text, columnId, columnsMap) {
                    if(_(columnsMap[column]).isString()) {
                        return '#' + JSON.stringify({
                                display: 'link',
                                type: type,
                                module: module,
                                id: _(columnsMap[column].split('/')).last(),
                                text: text
                            });
                    }
                    return text;
                }
            }

            var converter;
            if(col.reference_type == 'google_user') {
                converter = hrefConverter('user', 'google_drive', col.reference_column_key);
            } else if(col.reference_type == 'google_file') {
                converter = hrefConverter('file', 'google_drive', col.reference_column_key);
            } else if(col.reference_type == 'box_user') {
                converter = hrefConverter('user', 'box', col.reference_column_key);
            } else if(col.reference_type == 'box_file') {
                converter = hrefConverter('file', 'box', col.reference_column_key);
            }

            var result = {
                propName: col.prop_name,
                id: col.uniqueId,
                primary: col.is_primary_key,
                hidden: !col.show_in_policy_table,
                entityLabel: col.mainIdentifier,
                sortable: {
                    original: true
                },
                text: col.label,
                def: '',
                converter: converter
            };

            if(col.value_type == 'DATE') {
                result.format = 'time';
            }

            return result;
        }).value();

        columns.push({
            id: 'actions',
            text: 'Actions',
            'class': 'actions-column',
            converter: 'actionsIconsConverter',
            converterArgs: {
                entity_type: policy.entity_type
            }
        });

        colValueMap.actions = {};
        colIdToId.actions = 'actions';

        var rowsAsObjects = _(rows).chain().map(function(row) {
            var result = {};
            _(row).each(function(val, colId) {
                if(_(colValueMap[colId][val]).isUndefined()) {
                    result[colIdToId[colId]] = val;
                } else {
                    result[colIdToId[colId]] = colValueMap[colId][val];
                }
            });
            return result;
        }).filter(function(row) {
            return row.policy_status == args.status;
        }).value();

        return defaultListConverter($.extend({}, input, {
            data: {
                total_rows: input.data.total_rows,
                page: input.data.page,
                rows: rowsAsObjects
            }
        }), {
            actions: args.actions,
            columns: columns,
            options: {
                autoUpdate: 5000,
                checkboxes: true
            },
            toggle: [{
                text: 'Matched: ' + $filter('number')(args.findings.Match || 0),
                value: 'Match'
            }, {
                text: 'Pending: ' + $filter('number')(args.findings.Pending || 0),
                value: 'Pending'
            }, {
                text: 'Unmatch: ' + $filter('number')(args.findings.Unmatch || 0),
                value: 'Unmatch'
            }],
            converters: args.converters
        }).then(function(response) {
            response.data.columns = columns;
            return $q.when(response);
        });
    };
});