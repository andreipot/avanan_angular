var m = angular.module('dao');

m.factory('policyReportDao', function (http, policyReportConverter, policyDao, sqlQueries) {
    return {
        retrieve: function (policyId, properties, actions, page, perPage, filter, orderBy, status, stateCallback) {
            return http.get('/api/v1/policies/dynamic_policy_report/' + policyId).params({
                page: page,
                per_page: perPage,
                filter: filter,
                order_by: orderBy,
                column_name: 'policy_status',
                column_value: status
            }).preFetch(policyDao.findingsStatus(policyId), 'findingsStatus')
                .converter(policyReportConverter, function(preFetches) {
                    return {
                        status: status,
                        current: page,
                        findings: preFetches.findingsStatus.data[policyId] || {},
                        properties: properties,
                        actions: actions,
                        stateCallback: stateCallback
                    };
                });
        },
        filePolicyRootCauses: function(policyId) {
            return http.get(sqlQueries('viewer/file_policy_root_causes')).params({
                policy_id: policyId
            });
        }
    }
});