var m = angular.module('dao');

m.factory('siteDao', function(http, sqlQueries, singleRowConverter) {
    return {
        retrieve: function(siteId) {
            return http.get(sqlQueries('entity/site')).params({
                entity_id: siteId
            }).converter(singleRowConverter);
        }
    };
});