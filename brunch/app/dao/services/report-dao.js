var m = angular.module('dao');

m.factory('reportDao', function(http, $q, defaultListConverter, path, sqlQueries, mimeIcons) {
    return {
        list: function(columns) {
            return http.get('/api/v1/report/definitions').converter(defaultListConverter, {
                columns: columns
            });
        },
        retrieve: function(reportId) {
            return http.get('/api/v1/report/definitions/' + reportId);
        },
        update: function(reportId, data) {
            return http.put('/api/v1/report/definitions/' + reportId).body(data).run();
        },
        save: function(data) {
            return http.post('/api/v1/report/definitions').body(data).run();
        },
        remove: function(reportId) {
            return http.delete('/api/v1/report/definitions/' + reportId).run();
        },
        templates: function(columns) {
            return http.get('/api/v1/report').converter(defaultListConverter, {
                columns: columns
            });
        },
        report: function(columns, reportPath, params) {
            return http.get(sqlQueries('/' + reportPath))
                .params(params)
                .preFetch(mimeIcons(), 'mimeIcons')
                .converter(defaultListConverter, function(preFetches) {
                    return {
                        mimeIcons: preFetches.mimeIcons,
                        columns: columns,
                        converters: {
                            pathConverter: function (text, columnId, columnsMap, args) {
                                return '#' + JSON.stringify({
                                        display: 'linked-text',
                                        text: _(text).map(function (title, idx) {
                                            return '#' + JSON.stringify({
                                                    display: 'link',
                                                    type: (idx == text.length - 1) ? columnsMap[args.typeColumnName] : 'folder',
                                                    id: columnsMap[args.idsColumnName][idx],
                                                    text: title
                                                });
                                        }).join('/'),
                                        'class': 'no-ws-small'
                                    });
                            }
                        }
                    }
            });
        }
    };
});