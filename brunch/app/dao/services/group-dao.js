var m = angular.module('dao');

m.factory('groupDao', function(http, sqlQueries, singleRowConverter) {
    return {
        retrieve: function(groupId) {
            return http.get(sqlQueries('entity/group')).params({
                entity_id: groupId
            }).converter(singleRowConverter);
        }
    };
});