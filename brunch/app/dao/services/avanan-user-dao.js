var m = angular.module('dao');

m.factory('avananUserDao', function($q, http, defaultListConverter) {
    return {
        list: function(page, pageSize, columns) {
            return http.get('/api/v1/webuser')
                .converter(defaultListConverter, {
                columns: columns
            });
        },
        retrieve: function(id) {
            return http.get('/api/v1/webuser/' + id);
        },
        create: function(data) {
            return http.post('/api/v1/webuser').body(data).run();
        },
        update: function(id, data) {
            return http.put('/api/v1/webuser/' + id).body(data).run();
        },
        remove: function(id) {
            return http.delete('/api/v1/webuser/' + id).run();
        },
        auth: function(email, password) {
            return http.post('/api/v1/webuser/login').body({
                email: email,
                current_password: password
            });
        },
        logout: function() {
            return http.put('/api/v1/current_user/logout').run();
        },
        current: function() {
            return http.get('/api/v1/current_user');
        },
        login: function(email, password) {
            return http.post('/api/v1/current_user/login').body({
                email: email,
                current_password: password
            });
        },
        forgot: function(email) {
            return http.put('/api/v1/current_user/forgot_password').body({
                email: email
            });
        },
        reset: function(token, newPassword) {
            return http.put('/api/v1/current_user/reset_password').body({
                token: token,
                new_password: newPassword
            });
        },
        changePassword: function(oldPassword, newPassword) {
            return http.put('/api/v1/current_user/change_password').body({
                current_password: oldPassword,
                new_password: newPassword
            });
        }
    };
});