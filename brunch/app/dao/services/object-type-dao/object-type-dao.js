var m = angular.module('dao');

m.factory('objectTypeDao', function($q, http, policyPropertiesConverter) {
    return {
        retrieve: function () {
            return http.get('/api/v1/policies/policy_ui_conditions/')
                .converter(policyPropertiesConverter);
        }
    };
});