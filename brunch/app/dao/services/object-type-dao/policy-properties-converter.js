var m = angular.module('dao');

m.factory('policyPropertiesConverter', function ($q, propertiesTreeCommon, $routeParams) {
    var propertyTypes = {
        "number": [{
            "id": "=",
            "label": "=",
            "type": "single"
        }, {
            "id": ">",
            "label": ">",
            "type": "single"
        }, {
            "id": "<",
            "label": "<",
            "type": "single"
        }, {
            "id": "between",
            "label": "between",
            "type": "double",
            "firstLabel": "",
            "secondLabel": "and",
            "options": [
                {
                    "id": "leftInclusive",
                    "label": "Left inclusive",
                    "type": "flag",
                    "defaultState": false
                },
                {
                    "id": "rightInclusive",
                    "label": "Right inclusive",
                    "type": "flag",
                    "defaultState": false
                }
            ]
        }],
        "string": [{
            "id": "is",
            "label": "is",
            "type": "single",
            "options": [
                {
                    "id": "ignoreCase",
                    "label": "Ignore case",
                    "type": "flag",
                    "defaultState": false
                }
            ]
        }, {
            "id": "contains",
            "label": "contains",
            "type": "single",
            "options": [
                {
                    "id": "ignoreCase",
                    "label": "Ignore case",
                    "type": "flag",
                    "defaultState": false
                }
            ]
        }, {
            "id": "startsWith",
            "label": "starts with",
            "type": "single",
            "options": [
                {
                    "id": "ignoreCase",
                    "label": "Ignore case",
                    "type": "flag",
                    "defaultState": false
                }
            ]
        }, {
            "id": "regexp",
            "label": "regexp",
            "type": "single"
        }],
        "ipAddr": [{
            "id": "is",
            "label": "is",
            "type": "single",
            "options": [
                {
                    "id": "ignoreCase",
                    "label": "Ignore case",
                    "type": "flag",
                    "defaultState": false
                }
            ]
        }, {
            "id": "contains",
            "label": "contains",
            "type": "single",
            "options": [
                {
                    "id": "ignoreCase",
                    "label": "Ignore case",
                    "type": "flag",
                    "defaultState": false
                }
            ]
        }, {
            "id": "startsWith",
            "label": "starts with",
            "type": "single",
            "options": [
                {
                    "id": "ignoreCase",
                    "label": "Ignore case",
                    "type": "flag",
                    "defaultState": false
                }
            ]
        }, {
            "id": "regexp",
            "label": "regexp",
            "type": "single"
        }, {
            "id": "ip_between",
            "label": "between",
            "type": "double",
            "firstLabel": "",
            "secondLabel": "and",
            "options": [
                {
                    "id": "leftInclusive",
                    "label": "Left inclusive",
                    "type": "flag",
                    "defaultState": false
                },
                {
                    "id": "rightInclusive",
                    "label": "Right inclusive",
                    "type": "flag",
                    "defaultState": false
                },
                {
                    "id": "ignoreCase",
                    "label": "Ignore case",
                    "type": "flag",
                    "defaultState": false
                }
            ]
        }],
        "date": [{
            "id": "date_after",
            "label": "After",
            "type": "single-date"
        }, {
            "id": "date_before",
            "label": "Before",
            "type": "single-date"
        }],
        "enum": [{
            "id": "in",
            "label": "is one of",
            "type": "list"
        }],
        "policies": [{
            "id": "policy_match",
            "label": "Match",
            "type": "selector"
        }, {
            "id": "policy_unmatch",
            "label": "Unmatch",
            "type": "selector"
        }],
        "boolean": [{
            "id": "bool",
            "label": "is",
            "hidden": "true",
            "type": "selector",
            "defOptions": [{
                "id": true,
                "label": "is true"
            }, {
                "id": false,
                "label": "is false"
            }]
        }]
    };

    function typeConverter(menu) {
        var type = menu.value_type;
        var multiSelect = menu.multi_select;
        var isPolicy = menu.is_policy;

        if (multiSelect) {
            return 'enum';
        }
        if(isPolicy) {
            menu.options = _(menu.options).filter(function(option) {
                return option.id != $routeParams.id;
            });
            return 'policies'
        }
        if (type == 'INT') {
            return 'number';
        }
        if (type == 'BOOLEAN') {
            return 'boolean';
        }
        if (type == 'DATETIME' || type == 'DATE') {
            return 'date';
        }
        if (type == 'IP_ADDR') {
            return 'ipAddr'
        }
        return 'string';
    }

    function propertyTypeConverter(property) {
        if(property.is_entity) {
            return 'property'
        }
        if(property.type == 'Boolean') {
            return 'boolean';
        }
        if(property.type == 'TEXT') {
            return 'property-string';
        }
        if(property.type == 'RICH_TEXT_EDITOR') {
            return 'property-email-body';
        }
        return 'string';
    }

    return function (input, args) {
        var deferred = $q.defer();

        var saasList = input.data.saas_list;
        var entities = input.data.entities;
        var actions = input.data.actions;

        var data = {
            saasList: saasList,
            sources: {},
            allSources: {},
            propertyTypes: $.extend(true, {}, propertyTypes),
            actionTypes: {},
            dataUrls: {}
        };

        _(entities).each(function (entity, entityName) {
            data.actionTypes[entityName] = _(actions).chain().map(function(action, actionId) {
                if(_(action.saas).isUndefined() || action.saas == entity.saas) {
                    return {
                        id: actionId,
                        label: action.label,
                        display: 'popup',
                        size: 'lg',
                        attributes: _(action.attributes).chain().map(function (property, propertyId) {
                            var result = {
                                id: propertyId,
                                label: property.label,
                                type: propertyTypeConverter(property),
                                order: property.order
                            };
                            if(result.type == 'property') {
                                result.propertyType = property.type;
                            }
                            return result;
                        }).sortBy('order').value()
                    };
                }
                return null;
            }).filter(function(action) {
                return !_(action).isNull();
            }).value();

            if(!_(entity.dataUrl).isUndefined()) {
                data.dataUrls[entityName] = entity.dataUrl;
            }
        });

        var properties = data.allSources;

        _(entities).each(function (entity, entityName) {
            properties[entityName] = {
                id: entityName,
                label: entity.label,
                sub: [],
                selectable: false
            };
            _(entity.properties).each(function (menu) {
                var propertyType = typeConverter(menu);
                var id = entityName + '.' + menu.name;
                properties[id] = {
                    id: id,
                    entityName: entityName,
                    name: menu.name,
                    label: menu.label,
                    description: menu.description,
                    pointer: menu.entity_pointer,
                    isPolicy: menu.is_policy,
                    isSaas: menu.is_saas,
                    isSecureApp: menu.is_secapp,
                    onlyOnRoot: menu.is_policy,
                    selectable: true,
                    type: propertyType,
                    variants: menu.options,
                    mainIdentifier: menu.main_identifier
                };
                var propertyOperators = data.propertyTypes[propertyType];
                properties[id].variants = properties[id].variants || propertyOperators[0].defOptions;
                properties[entityName].sub.push(id);
            });
        });

        _(properties).chain().filter(function(property) {
            return !_(property.sub).isUndefined();
        }).each(function(property) {
            property.sub = _(property.sub).chain().sortBy(function(subId) {
                var prefix = null;
                if(properties[subId].isSaas) {
                    prefix = '6';
                } else if(properties[subId].isSecureApp) {
                    prefix = '7';
                } else if(properties[subId].isPolicy) {
                    prefix = '4';
                } else {
                    properties[subId].isCommon = true;
                    prefix = '5';
                }
                properties[subId].priority = prefix;
                return prefix + '-' + properties[subId].label;
            }).reduce(function(memo, subId) {
                if(memo.length == 0) {
                    memo.push(subId);
                } else {
                    var last = properties[_(memo).last()];
                    var current = properties[subId];

                    if(last.priority != current.priority) {
                        memo.push({
                            separator: true,
                            onlyOnRoot: last.onlyOnRoot
                        });
                    }
                    memo.push(subId);
                }
                return memo;
            }, []).flatten().value();
        });

        _(properties).chain().filter(function (property) {
            return _(property.name).isString();
        }).each(function (property) {
            if (_(property.pointer).isString() && _(properties[property.pointer]).isObject()) {
                property.sub = _(properties[property.pointer].sub).map(function(sub) {
                    if(_(sub).isString()) {
                        var id = property.name + '.' + properties[sub].name;
                        properties[id] = properties[sub];
                        return id;
                    }
                    return sub;
                });
                property.selectable = false;
            }
        });

        function getSubProperties(propertyName, result) {
            result = result || {};
            if (_((properties[propertyName] || {}).sub).isUndefined()) {
                return {};
            }
            _(properties[propertyName].sub).each(function (id) {
                if (_(result[id]).isUndefined()) {
                    result[id] = $.extend(true, {}, properties[id]);
                    getSubProperties(id, result);
                }
            });
            return result;
        }

        _(input.data.entities).each(function (entity, entityName) {
            if(entity['entity_type'] == 'base_entity') {
                var source = data.sources[entityName] = {
                    label: entity.label,
                    saas: entity.saas,
                    properties: getSubProperties(entityName)
                };
                source.properties[0] = properties[entityName];
                propertiesTreeCommon.removeSameEntityTypesSubMenus(source);
            }
        });

        deferred.resolve($.extend({}, input, {
            data: data
        }));
        return deferred.promise;
    }
});