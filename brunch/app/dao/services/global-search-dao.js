var m = angular.module('dao');

m.factory('globalSearchDao', function(http, defaultListConverter, mimeIcons, $q, sqlQueries) {
    return {
        search: function(columns, keyword, categoryLimit) {
            return http.get(sqlQueries('global_search')).params({
                keyword: keyword,
                category_limit: categoryLimit
            }).preFetch(mimeIcons(), 'mimeIcons')
                .dynamicConverter(function(preFetches) {
                    return function (response) {
                        var result = {};
                        _(response.data.rows).each(function (row) {
                            result[row.type] = null;
                        });
                        _(result).each(function (value, key) {
                            var tableResponse = $.extend({}, response, {
                                data: {
                                    rows: _(response.data.rows).filter(function (row) {
                                        return row.type == key;
                                    })
                                }
                            });
                            result[key] = defaultListConverter(tableResponse, {
                                mimeIcons: preFetches.mimeIcons,
                                columns: columns[key]
                            });
                        });
                        return $q.when(result);
                    }
                });
        }
    }
});