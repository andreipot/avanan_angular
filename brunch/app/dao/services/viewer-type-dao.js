var m = angular.module('dao');

m.factory('viewerTypeDao', function ($http, http, path, lazyFuture, futureCache, $q, defaultListConverter, mimeIcons, singleValueConverter, sqlQueries, modulesManager, policyDao) {
    var root = path('viewer').ctrl('viewer');

    function retrieveTypes() {
        return futureCache.cache('viewer-types', 60000, function () {
            return root.json('types').retrieve();
        });
    }

    var viewerTypeDao = {
        types: retrieveTypes,
        count: function (type, params) {
            return retrieveTypes().then(function (response) {
                var metaInfo = response.data[type];
                var dataType = metaInfo.type || 'viewer';
                if(dataType == 'viewer') {
                    return http.get(sqlQueries('viewer/' + metaInfo.url)).params($.extend({
                        queryCount: 1
                    }, params)).converter(singleValueConverter, {
                        name: 'count'
                    });
                } else if(dataType == 'policy') {
                    return policyDao.retrieveByInternalName(metaInfo.internalName).then(function(policy) {
                        return policyDao.findingsStatus(policy.data.policy_id).then(function(findingStatus) {
                            return $q.when({
                                data: findingStatus.data[policy.data.policy_id].Matched
                            });
                        });
                    });
                }
            });
        },
        listColumns: function (type) {
            return root.json(modulesManager.currentModule() + '/' + type).retrieve();
        },
        list: function (columns, type, params, preConverters) {
            return http.get(function (preFetches) {
                return sqlQueries('viewer/' + preFetches.types.data[type].url)
            }).params(params || {})
                .preFetch(retrieveTypes(), 'types')
                .preFetch(mimeIcons(), 'mimeIcons')
                .dynamicConverter(function(preFetches) {
                    return preConverters[preFetches.types.data[type].preConverter];
                })
                .converter(defaultListConverter, function (preFetches) {
                    return {
                        mimeIcons: preFetches.mimeIcons,
                        columns: columns,
                        converters: {
                            installedAppConverter: function (text, columnId, columnsMap) {
                                return lazyFuture(function () {
                                    return viewerTypeDao.count('users-with-app', {
                                        app_id: columnsMap['entity_id']
                                    }).then(function (usersWithAppResponse) {
                                        return $q.when('#' + JSON.stringify({
                                            display: 'link',
                                            type: 'viewer',
                                            id: 'users-with-app',
                                            qs: {
                                                app_id: columnsMap['entity_id']
                                            },
                                            text: usersWithAppResponse.data
                                        }));
                                    });
                                });
                            },
                            scopesListConverter: function (text, columnId, columnsMap) {
                                return '#' + JSON.stringify({
                                        display: 'inline-list',
                                        'class': 'medium-width point-prefix',
                                        data: _(text).chain().map(function (label, idx) {
                                            return label + ' (' + columnsMap['scopes_risk'][idx] + ')';
                                        }).sortBy(function (label, idx) {
                                            return -columnsMap['scopes_risk'][idx] * 10;
                                        }).map(function (label, idx) {
                                            return '#' + JSON.stringify({
                                                    display: 'text',
                                                    text: label
                                                });
                                        }).value(),
                                        countText: text.length + ' scopes'
                                    });
                            },
                            applicationNameConverter: function (text, columnId, columnsMap) {
                                if (columnsMap.type == 'token') {
                                    return text;
                                }
                                return '#' + JSON.stringify({
                                        display: 'link',
                                        type: 'application',
                                        id: columnsMap['entity_id'],
                                        text: text
                                    });
                            },
                            applicationRiskFactorConverter: function (text, columnId, columnsMap) {
                                function sum(memo, val) {
                                    return memo + val;
                                }

                                var lowRisk = _(text).chain().filter(function (risk) {
                                    return risk <= 3;
                                }).reduce(sum, 0).value();

                                var mediumRisk = _(text).chain().filter(function (risk) {
                                    return risk > 3 && risk < 6;
                                }).reduce(sum, 0).value();

                                var highRisk = _(text).chain().filter(function (risk) {
                                    return risk >= 6;
                                }).reduce(sum, 0).value();

                                return '#' + JSON.stringify({
                                        display: 'stacked-progress',
                                        total: columnsMap['max_risk'],
                                        data: [{
                                            value: lowRisk,
                                            'class': 'progress-bar-success'
                                        }, {
                                            value: mediumRisk,
                                            'class': 'progress-bar-warning'
                                        }, {
                                            value: highRisk,
                                            'class': 'progress-bar-danger'
                                        }]
                                    });
                            }
                        }
                    }
                });
        }
    };
    return viewerTypeDao;
});