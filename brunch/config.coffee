exports.config =
  # See http://brunch.readthedocs.org/en/latest/config.html for documentation.
  plugins:
    uglify:
      mangle: true
      compress:
        global_defs:
          DEBUG: false
    sass:
      mode: 'ruby'
    assetsmanager:
      copyTo:
        'javascript/tinymce': ['bower_components/tinymce/*'],
        'css/': [
          'bower_components/select2/select2.png',
          'bower_components/select2/select2-spinner.gif'
        ],
        'fonts/': [
          'bower_components/font-awesome/fonts/*',
          'bower_components/google-open-sans/open-sans',
          'bower_components/google-open-sans/open-sans-condensed'
        ]
    javascript:
      validate: true
    jshint:
      pattern: /^app\/.*\.js$/
      options:
        bitwise: false
        curly: false
      globals:
        jQuery: true
      warnOnly: true
  modules:
    definition: false
    wrapper: (path, data) ->
      if /^app[\\/].*/.test(path)
        """(function(module){'use strict'\n#{data}})();\n\n"""
      else data
  paths:
    public: '../assets'
  files:
    javascripts:
      joinTo:
        'javascript/app.js': /^app[\\/]/
        'javascript/vendor.js': /^bower_components[\\/]/
      order:
        before: [
          /[\\/]module\.js/
        ]
        after: [
          /[\\/]jquery\.jvectormap\.min\.js/,
          /[\\/]jquery-jvectormap-world-mill-en\.js/
        ]

    stylesheets:
      joinTo:
        'css/app.css': /^(app[\\/]|bower_components[\\/])/
      order:
        before: [
          'app/reset.css',
          'bower_components/bootstrap/dist/css/bootstrap.css',
          'app/global.less'
        ]

  conventions:
    ignored: [
      'bower_components/google-open-sans/'
    ]
    assets: (path) ->
      if /[\\/]$/.test path
        return true
      if /^app[\\/].*\.html/.test path
        return true
      if /^app[\\/].*\.json/.test path
        return true
      if /^app[\\/].*\.txt/.test path
        return true
      if /^app[\\/].*\.png/.test path
        return true
      if /^app[\\/].*\.jpg/.test path
        return true
      if /^app[\\/].*\.svg/.test path
        return true
      return false
