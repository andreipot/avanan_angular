# Application structure

root
  +--assets (compiled sources)
    +-fonts (copy here fonts from font-awesome folder)
  +--brunch (source root)
    +-bower.json //bower config
    +-package.json //dependencies for brunch
    +-config.coffee //brunch config
    +-bower_components //created by bower, ignore in git
    +-node_modules //created by npm, ingore in git
    +-app //application root
        +-first-module //Split modules into separate dirs
            +-module.js //define module and configure it
            +-controllers //dir with controllers
                +-first-controller //every contoller have own dir
                    +-first-controller.js //controller code
                    +-first-controller.html //controller template
                    +-first-controller.scss //controller styles
                +-second-controller
                    +-second-controller.js
                    +-second-controller.html
                    +-second-controller.scss
            +-directives //dir with directives
                +-first-directive //every directive have own dir
                    +-first-directive.js //directive code
                    +-first-directive.html //directive template
                    +-first-directive.scss //diretive styles
            +-services //dir with services
                +-first-service.js //service code. It don't need any template/styles.
                +-second-service.js
        +-second-module
        +-third-module
        +-css (styles from bootstrap theme)
        +-endless.js (leftover script from bootstrap theme, need to move in future)
  +--index.html //application html that consumes assets data

# Development environment setup:
0. Setup needed soft like: npm, bower, brunch, ruby (for scss compilation), python:
sudo apt-get install npm
sudo npm install -g bower
sudo npm install -g brunch
sudo apt-get install nodejs-legacy
sudo apt-get install ruby
sudo gem install sass
1. Go to `brunch` folder
2. Execute `npm install` (may be you will need superuser rights)
3. Execute `bower install`
4. Some dependencies need to be built after install:
4.1. jvectomap: go to bower_components/jvectormap/ and execute build.sh (uglifyjs required to be installed.
5. Go to brunch folder again and execute `brunch watch`
6. If you want to get release version execute `brunch watch -P`
7. Your sources will be compiled into assets folder.
8. You can start local http server by `python -m SimpleHTTPServer`

# Automated build using docker
1. Make sure docker is installed (https://docs.docker.com/installation/#installation) and configured to be used without root access (https://docs.docker.com/installation/ubuntulinux/#giving-non-root-access)
2. Run build.sh (no need to install any of the pre-requisites from above)

This will create a file named `avanan-frontend-<version>.tar.gz` that contains all the static files of the avanan frontend application