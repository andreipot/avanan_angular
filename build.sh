#!/bin/bash -e

# TODO
# - Versioning (including GIT tagging)

VERSION=0.1
DOCKER_BUILD_IMAGE_NAME=avanan-frontend-build

echo --- Building Docker build environment
docker build --tag=$DOCKER_BUILD_IMAGE_NAME ./build/docker

echo --- Building static application files
docker run \
	--rm \
	-i \
	-v $PWD:/data \
	--dns 8.8.8.8 --dns 4.4.4.4 \
	$DOCKER_BUILD_IMAGE_NAME \
	./build/docker/build-static.sh

echo --- Building application package
tar -cvf avanan-frontend-$VERSION.tar *.html assets api

# TODO tag git repository

# Interactive
#docker run -i -t --dns 8.8.8.8 --dns 4.4.4.4 -v $PWD/../..:/data avanan-application-build /bin/bash
