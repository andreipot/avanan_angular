#!/bin/bash -e
cd brunch
npm install
bower install --allow-root --config.interactive=false
cd bower_components/jvectormap
./build.sh
cd -
brunch build
